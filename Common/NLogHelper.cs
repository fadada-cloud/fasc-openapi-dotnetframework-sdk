﻿namespace fasc_openapi_donet_sdk.Common
{
    /// <summary>
    /// 系统日志记录公共类
    /// </summary>
    public class NLogHelper
    {
        //private static Logger logger = LogManager.GetCurrentClassLogger();//初始化Nlog
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();//初始化Nlog

        /// <summary>
        /// 调试
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="args"></param>
        public static void Debug(string msg, params object[] args)
        {
            logger.Debug(msg, args);
        }

        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="args"></param>
        public static void Info(string msg, params object[] args)
        {
            logger.Info(msg, args);
        }

        /// <summary>
        /// 异常信息
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="args"></param>
        public static void Error(string msg, params object[] args)
        {
            logger.Error(msg, args);
        }
    }
}

