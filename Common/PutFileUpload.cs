﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Common
{
    public static class PutFileUpload
    {
        /// <summary>
        /// 法大大本地文件直传腾讯云
        /// </summary>
        /// <param name="url"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static HttpWebResponse HttpUploadFile(string url, string path)
        {
            try
            {
                // 设置参数
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                CookieContainer cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = "PUT";
                request.ContentType = "application/octet-stream";
                //请求头部信息 
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (Stream putStream = request.GetRequestStream())
                    {
                        // Use a buffer to upload in chunks, for progress monitoring or cancellation
                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = fs.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            putStream.Write(buffer, 0, bytesRead);
                        }
                    }
                }

                //发送请求并获取相应回应数据
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
