﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Common;
using fasc_openapi_donet_sdk.Model.RequestModel;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace fasc_openapi_donet_sdk.Client
{
    public class OpenApiClient : AccessClient
    {
        public OpenApiClient(string appId, string appSecret, string serverUrl)
        {
            this.AppId = appId;
            this.AppSecret = appSecret;
            this.ServerUrl = serverUrl;
        }
        /// <summary>
        /// 获取AccessToken
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="req"></param>512
        /// <returns></returns>
        public BaseRes<T> GetHttpResponse<T>(BaseReq<T> req,string accessToken) where T : class,new()
        {
            var reqStr = JsonConvert.SerializeObject(req,Formatting.Indented,new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });          
            var urlAttribute = GetUrlAttribute(req);
            var url = ServerUrl + urlAttribute.Url;
            BaseRes<T> rspModel = null;
            try
            {
                var body = new Dictionary<string, string>() { { "bizContent", reqStr } };
                var client = HttpHelper.CreateDefault(url);
                if (accessToken != null)
                {
                    SetReqHeadParams(client, accessToken, body);
                    client.Method = urlAttribute.Method;
                    client.ContentType = "application/x-www-form-urlencoded";       
                    client.SubmitFormData(body, null);
                    var rsp = client.GetResponseIgnoreServerError();
                    string rspStr = null;
 
                    rspStr = rsp.GetResponseString();
                    rspModel = JsonConvert.DeserializeObject<BaseRes<T>>(rspStr);
                    rspModel.requestId = rsp.Headers["X-FASC-Request-Id"];
                }
                else
                {
                    rspModel = new BaseRes<T>()
                    {
                        code = "100002",
                        msg = "请检查accessToken是否存在，若不存在或已过期，请重新获取",
                        data = null
                    };
                }

            }
            catch (Exception ex)
            {
                rspModel = new BaseRes<T>()
                {
                    code = "-1",
                    msg = "clientResquest" + ex.Message,
                };
            }
            return rspModel;
        }

        //获取类名上方配置的url地址
        public  RemoteServiceAttribute GetUrlAttribute<T>(BaseReq<T> req) where T : class, new()
        {
            var urlAttribute = req.GetType().GetCustomAttributes(false).First(r => r is RemoteServiceAttribute) as RemoteServiceAttribute;
            return urlAttribute;
        }

        /// <summary>
        /// 拼接公共请求head参数
        /// </summary>
        /// <param name="client"></param>
        /// <param name="accessToken"></param>
        /// <param name="body"></param>
        public void SetReqHeadParams(HttpWebRequest client,string accessToken,Dictionary<string,string> body)
        {
            IDictionary<string, string> headerParams = new Dictionary<string, string>();
            string nonce = Guid.NewGuid().ToString("N");
            //标准时间戳
            string strTimeStamp = ((DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000).ToString();//毫秒值
            headerParams.Add("X-FASC-App-Id", AppId);
            headerParams.Add("X-FASC-Sign-Type", signType);
            headerParams.Add("X-FASC-Timestamp", strTimeStamp);
            headerParams.Add("X-FASC-Nonce", nonce);
            headerParams.Add("X-FASC-AccessToken", accessToken);
            headerParams.Add("X-FASC-Api-SubVersion", "5.1");
            string signature = Common.SignUtil.GetSignByParams(headerParams.Union(body), strTimeStamp, AppSecret);

            headerParams.Add("X-FASC-Sign", signature);
            foreach (var p in headerParams)
            {
                client.Headers[p.Key] = p.Value;
            }
        }

        /// <summary>
        /// Put上传本地文件到uploadUrl地址
        /// </summary>
        /// <param name="uploadUrl"></param>
        /// <param name="path"></param>
        /// <returns></returns>
       public string PutLocalfileUpload(string uploadUrl,string path)
        {          
            if(uploadUrl != null && path != null)
            {
                using (var resp = PutFileUpload.HttpUploadFile(uploadUrl, path))
                {
                    if (resp.StatusCode.ToString() == HttpStatusCode.OK.ToString())//code 为OK则文件put成功
                    {
                        return JsonConvert.SerializeObject(new BaseRes<Object>()
                        {
                            code = "200",
                            msg = "本地文件上传成功",
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new BaseRes<Object>()
                        {
                            code = "0",
                            msg = "本地文件上传失败,请联系管理员",
                        });
                    }
                }
            }
            else
            {
                return JsonConvert.SerializeObject(new BaseRes<Object>()
                {
                    code = "0",
                    msg =  "文件上传地址以及文件路径不能为空"
                });                   
            }
        }
    }
}
