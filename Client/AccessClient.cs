﻿using fasc_openapi_donet_sdk.Common;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Client
{
    /// <summary>
    /// 客户端初始化并获取服务访问凭证
    /// </summary>
    public  class AccessClient
    {
        public string AppId;
        public string AppSecret;
        public string ServerUrl;

        public const string signType = "HMAC-SHA256";//签名算法

        //Token对象存储
        public static ConcurrentDictionary<string, AccessTokenRes> tokens = new ConcurrentDictionary<string, AccessTokenRes>();

        //获取token并判断失效时间
        public BaseRes<AccessTokenRes> GetToken()
        {
            BaseRes<AccessTokenRes> tokenData = new BaseRes<AccessTokenRes>();
            if (tokens.TryGetValue(AppId, out tokenData.data) == false || tokenData.data == null)
            {
                tokenData = GetTokenFromServer();
                tokens[AppId] = tokenData.data;
            }
            else if ((tokenData.data.expiresTime - DateTime.Now).TotalMinutes < 3)
            {
                tokenData = GetTokenFromServer();
                tokens[AppId] = tokenData.data;
            }
            return tokenData;
        }

        /// <summary>
        /// 获取服务访问凭证token
        /// </summary>
        /// <returns></returns>
        public BaseRes<AccessTokenRes> GetTokenFromServer()
        {
            try
            {
                AccessTokenRes res = new AccessTokenRes();
                var headParams = GetReqHeadParams();

                var client = HttpHelper.CreateDefault(ServerUrl + "/service/get-access-token");

                foreach (var item in headParams)
                {
                    client.Headers[item.Key] = item.Value;
                }
                client.Method = "POST";
                client.ContentType = "application/x-www-form-urlencoded";
                var rspResponse = client.GetResponseIgnoreServerError();

                var rspStr = rspResponse.GetResponseString();
                var rspToken = JsonConvert.DeserializeObject<BaseRes<AccessTokenRes>>(rspStr);
                rspToken.requestId = rspResponse.Headers["X-FASC-Request-Id"];

                if (rspToken.data != null)
                {
                    rspToken.data.expiresTime = DateTime.Now.AddSeconds(Convert.ToDouble(rspToken.data.expiresIn));
                }
                return rspToken;
            }
            catch (Exception ex)
            {
                NLogHelper.Error("获取token异常" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 拼接header请求参数
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, string> GetReqHeadParams()
        {
            IDictionary<string, string> headerParams = new Dictionary<string, string>();
            string nonce = Guid.NewGuid().ToString("N");
            //标准时间戳
            string strTimeStamp = ((DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000).ToString();//毫秒值
            headerParams.Add("X-FASC-App-Id", AppId);
            headerParams.Add("X-FASC-Sign-Type", signType);
            headerParams.Add("X-FASC-Timestamp", strTimeStamp);
            headerParams.Add("X-FASC-Nonce", nonce);
            headerParams.Add("X-FASC-Grant-Type", "client_credential");
            headerParams.Add("X-FASC-Api-SubVersion", "5.1");
            string signature = Common.SignUtil.GetSignByParams(headerParams, strTimeStamp, AppSecret);

            headerParams.Add("X-FASC-Sign", signature);

            return headerParams;
        }
    }
}
