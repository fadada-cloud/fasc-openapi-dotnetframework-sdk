# FASC OpenApi C# .net framework SDK v5.1 说明

# 简介

欢迎使用法大大开发者工具套件（SDK），C# .net framework SDK 是法大大电子合同和电子签云服务开放平台（FASC OPEN API）的配套工具。



# 版本说明

FASC.openAPI 产品目前存在两个子版本号：v5.0、v5.1， 均在持续迭代维护。 

当前页面SDK基于FASC.openAPI v5.1子版本开发，如需使用FASC.openAPI v5.0版本SDK，请访问： 

https://gitee.com/fadada-cloud/fasc-openapi-dotnetframework-sdk/tree/v5.0




# 依赖环境

基于dotnetframework 4.8



# 版本更新日志

- 5.1.0 - 2022-07-28   基于FASC OpenAPI 5.1.0版本开发，初始版本。

  

# 参考

FASC OpenAPI (服务端) 接口文档 v5.1

https://dev.fadada.com/api-doc/MTE9YIK1SP/QMMRYYN5RMPREAZH/5-1 

