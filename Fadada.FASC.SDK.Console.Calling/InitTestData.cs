﻿using fasc_openapi_donet_sdk.Client;

namespace Test_Fasc_api_sdk
{
    /// <summary>
    /// 测试数据
    /// </summary>
    public class InitTestData
    {
        public static string APPID = "00000282";
        public static string APPSECRET = "LFMP41RMYLSQSFJQV99XOT6CUM1VUIU4";
        public static string SERVERURL = "https://sit-api.fadada.com/api/v5";
        public static OpenApiClient client;

        /**
         * 测试数据
         */
        public static string clientCorpId = "企业主体在应用中的唯一标识";
        public static string openCorpId = "00796dd2ffe44896b80ec53bd90a1466";
        public static string clientUserId = "clientUserId8888";
        public static string openUserId = "3028a36280964c6eac5dfddb98d1c8d2";
        public static string docTemplateId = "文档模板id";
        public static string signTemplateId = "1680830329677193201";
        public static string docFileId = "1696648192992187929";
        public static string attachFileId = "1690357770515178035";
        public static string attachId = "签署任务内指定附件序号";
        public static string signTaskId = "签署任务id";
    }
}
