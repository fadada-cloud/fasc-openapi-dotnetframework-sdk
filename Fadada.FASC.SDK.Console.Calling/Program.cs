﻿using fasc_openapi_donet_sdk.Client;
using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.RequestModel;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientBillManage;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientEUIManage;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientUserManage;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientAppField;
using Newtonsoft.Json;
using System;
using Test_Fasc_api_sdk;
using fasc_openapi_donet_sdk.Common;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientApproveManage;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientDraftManage;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientArchivesPerformance;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientOCR;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService;
using fasc_openapi_donet_sdk.Model.RequestModel.ClientCallBack;

namespace Fasc_opensdk_test_demo
{
    class Program : InitTestData
    {
        static void Main(string[] args)
        {
           
            #region 公共信息
            string appid = APPID;
            string reqUrl = SERVERURL;
            string appSecret = APPSECRET;

            OpenApiClient client = new OpenApiClient(appid, appSecret, reqUrl);//OPENAPI公共客户端
                                                                               // EUIClient   euiClient = new EUIClient(appid, appSecret, reqUrl); //EUI客户端,构造企业用户授权链接

            #endregion

            #region 各个模块调用示例

            #region 获取个人凭证Token

            string accessToken = string.Empty;
            var tokenRes = client.GetTokenFromServer();
            if (tokenRes.code == "100000" && tokenRes.data.accessToken != null)
            {
                accessToken = tokenRes.data.accessToken;
            }

            #endregion

            #region 个人用户账号管理

            //恢复个人用户
            UserEnableReq userEnableReq = new UserEnableReq()
            {
                openUserId = openUserId
            };
            //禁用个人用户
            UserForbidenReq userForReq = new UserForbidenReq()
            {
                openUserId = openUserId
            };
            //解绑个人用户
            UserUnbindReq userUnbindReq = new UserUnbindReq()
            {
                openUserId = openUserId
            };
            //获取个人用户授权链接 
            GetUserAuthUrlReq getUserAuthUrlReq = new GetUserAuthUrlReq()
            {
                clientUserId = "",
                requestAuthScope = new string[] { "ident_info", "signtask_init", "signtask_info" },
                userIdentInfo = new UserIdentInfo()
                {
                    userName = "",
                    identNo = "",
                    identType = ""
                },
                identInfoMatch = true,
                redirectUrl = ""
            };

            //获取个人用户信息
            GetUserInfoReq getUserInfoReq = new GetUserInfoReq()
            {
                openUserId = openUserId
                // clientUserId = ""(二选其一)
            };

            //获取个人用户实名信息
            GetUserIdentityReq getUserIdentityReq = new GetUserIdentityReq()
            {
                openUserId = openUserId
            };

            //查询授权用户列表
            GetAppOpenIdListReq getAppOpenIdListReq = new GetAppOpenIdListReq()
            {
                idType = "",
                owner = false,
                listPageNo = -1,
                listPageSize = -1
            };
            PrintResponse("查询授权用户列表", client, getAppOpenIdListReq, accessToken);

            // 恢复个人用户
            GetChangeAccountUrlReq getChangeAccountUrlReq = new GetChangeAccountUrlReq
            { changeType = "", redirectUrl = "" };
            PrintResponse("恢复个人用户", client, getChangeAccountUrlReq, accessToken);

            #endregion

            #region 企业用户账号管理

            //查询企业用户基本信息
            GetCorpReq getCorpReq = new GetCorpReq()
            {
                openCorpId = openCorpId,
                //clientCorpId = clientCorpId(二选其一)
            };
            //获取企业用户身份信息
            GetCorpIdentityInfoReq getCorpIdentityInfoReq = new GetCorpIdentityInfoReq()
            {
                openCorpId = openCorpId
            };
            //禁用企业用户
            CorpForbidenReq corpForbidenReq = new CorpForbidenReq()
            {
                openCorpId = openCorpId
            };
            //恢复企业用户
            CorpEnableReq corpEnableReq = new CorpEnableReq()
            {
                openCorpId = openCorpId
            };

            //解除企业用户授权
            CorpUnbindReq corpUnbindReq = new CorpUnbindReq()
            {
                openCorpId = openCorpId
            };

            //获取企业用户授权链接
            GetCorpAuthUrlReq getCorpAuthUrlReq = new GetCorpAuthUrlReq()
            {
                clientCorpId = "",
                corpIdentInfoMatch = true,
                redirectUrl = "",
                authScopes = new string[] { "ident_info", "seal_info", "signtask_info" }
            };

            //查询企业实名认证状态
            GetCorpIdentifiedStatusReq getCorpIdentifiedStatusReq = new GetCorpIdentifiedStatusReq()
            {
                corpName = "",
                corpIdentNo = "",
            };
            #endregion

            #region 组织架构管理
            //查询企业成员列表
            GetCorpMemberListReq getCorpMemberListReq = new GetCorpMemberListReq()
            {
                ownerId = "",
                listFilter = new CorpListFilter()
                {
                    roleType = ""
                },
                listPageNo = 1,
                listPageSize = 10
            };
            //查询部门列表
            GetCorpDeptListReq GetCorpDeptListReq = new GetCorpDeptListReq()
            {
                openCorpId = ""
            };
            //查询部门详情
            GetCorpDeptDetailReq getCorpDeptDetailReq = new GetCorpDeptDetailReq()
            {
                openCorpId = "",
                deptId = ""
            };
            //创建部门
            GetCorpDeptCreateReq getCorpDeptCreateReq = new GetCorpDeptCreateReq()
            {
                openCorpId = "",
                deptName = ""
            };

            //修改部门基本信息
            GetCorpDeptModifyReq getCorpDeptModifyReq = new GetCorpDeptModifyReq()
            {
                openCorpId = "",
                deptName = ""
            };

            //删除部门
            GetCorpDeptDeleteReq getCorpDeptDeleteReq = new GetCorpDeptDeleteReq()
            {
                openCorpId = "",
                deptId = ""
            };

            //查询成员详情
            GetCorpMemberDetailReq GetCorpMemberDetailReq = new GetCorpMemberDetailReq()
            {
                openCorpId = "",
                memberId = ""
            };

            //创建成员
            CorpMemberCreateReq corpMemberCreateReq = new CorpMemberCreateReq()
            {
                openCorpId = "",
            };

            //获取成员激活链接
            GetCorpMemberActiveUrlReq getCorpMemberActiveUrlReq = new GetCorpMemberActiveUrlReq()
            {
                openCorpId = "",
            };

            //修改成员基本信息
            CorpMemberModifyReq corpMemberModifyReq = new CorpMemberModifyReq()
            {
                openCorpId = "",
            };

            //设置成员所属部门
            CorpMemberSetDeptReq corpMemberSetDeptReq = new CorpMemberSetDeptReq()
            {
                openCorpId = "",
            };

            //设置成员状态
            CorpMemberSetStatusReq corpMemberSetStatusReq = new CorpMemberSetStatusReq()
            {
                openCorpId = "",
            };

            //删除成员
            CorpMemberDeleteReq corpMemberDeleteReq = new CorpMemberDeleteReq()
            {
                openCorpId = "",
            };

            //获取组织管理链接
            GetCorpOrganizationManageUrlReq getCorpOrganizationManageUrlReq = new GetCorpOrganizationManageUrlReq()
            {
                openCorpId = "",
            };

            //获取企业工商信息变更链接
            GetChangeCorpIdentityInfoUrlReq getChangeCorpIdentityInfoUrlReq = new GetChangeCorpIdentityInfoUrlReq()
            { clientCorpId = clientCorpId, openCorpId = openCorpId, clientUserId = clientUserId, redirectUrl = "" };
            PrintResponse("获取企业工商信息变更链接", client, getChangeCorpIdentityInfoUrlReq, accessToken);

            //获取成员企业管理链接
            GetCorpEntityManageUrlReq getCorpEntityManageUrlReq = new GetCorpEntityManageUrlReq()
            { openCorpId = openCorpId, clientUserId = clientUserId, redirectUrl = "" };
            PrintResponse("获取成员企业管理链接", client, getCorpEntityManageUrlReq, accessToken);

            //查询相对方列表
            GetCounterpartListReq getCounterpartListReq = new GetCounterpartListReq()
            { openCorpId = openCorpId };
            PrintResponse("查询相对方列表", client, getCounterpartListReq, accessToken);

            //查询企业主体列表
            GetEntityListReq getEntityListReq = new GetEntityListReq()
            { openCorpId = openCorpId };
            PrintResponse("查询企业主体列表", client, getEntityListReq, accessToken);

            #endregion

            #region 印章管理

            //获取印章列表
            GetSealListReq getSealListReq = new GetSealListReq()
            {
                openCorpId = openCorpId,
                //listFilter = new ListFilter()
                //{
                //    categoryType = new string[] { "official_seal", "contract_seal" }
                //}
            };
            //查询企业用印员列表
            GetSealUserListReq getSealUserListReq = new GetSealUserListReq()
            {
                openCorpId = openCorpId,
            };

            //查询印章详情
            GetSealDetailReq getSealDetailReq = new GetSealDetailReq()
            {
                openCorpId = openCorpId,

            };

            //获取指定印章详情链接
            GetSealManageAppointedSealUrlReq getSealManageAppointedSealUrlReq = new GetSealManageAppointedSealUrlReq()
            {
                openCorpId = openCorpId,

            };

            //获取印章创建链接
            GetSealCreateUrlReq getSealCreateUrlReq = new GetSealCreateUrlReq()
            {
                openCorpId = openCorpId,

            };

            //查询审核中的印章列表
            GetSealVerifyListReq getSealVerifyListReq = new GetSealVerifyListReq()
            {
                openCorpId = openCorpId,
            };

            //修改印章基本信息
            GetSealModifyReq getSealModifyReq = new GetSealModifyReq()
            {
                openCorpId = openCorpId,
            };

            //获取印章授权链接
            GetSealGrantUrlReq getSealGrantUrlReq = new GetSealGrantUrlReq()
            {
                openCorpId = openCorpId,
            };

            //解除印章授权
            SealGrantCancelReq sealGrantCancelReq = new SealGrantCancelReq()
            {
                openCorpId = openCorpId,
            };

            //设置印章状态
            SealSetStatusReq sealSetStatusReq = new SealSetStatusReq()
            {
                openCorpId = openCorpId,

            };

            //删除印章
            SealDeleteReq sealDeleteReq = new SealDeleteReq()
            {
                openCorpId = openCorpId,

            };

            //获取印章管理链接
            SealManageUrlReq sealManageUrlReq = new SealManageUrlReq()
            {
                openCorpId = openCorpId,

            };

            //获取设置企业印章免验证签链接
            GetSealFreeSignUrlReq getSealFreeSignUrlReq = new GetSealFreeSignUrlReq()
            {
                openCorpId = openCorpId,

            };

            //查询个人签名列表
            GetSealPersonalSealListReq getSealPersonalSealListReq = new GetSealPersonalSealListReq()
            {
                openUserId = "",

            };

            //获取设置个人签名免验证签链接
            GetSealPersonalSealFreeSignUrlReq getSealPersonalSealFreeSignUrlReq = new GetSealPersonalSealFreeSignUrlReq()
            {
                openUserId = openCorpId,

            };

            //解除签名免验证签
            CancelPersonalSealFreeSignReq cancelPersonalSealFreeSignReq = new CancelPersonalSealFreeSignReq()
            { openUserId = openUserId, businessId = "", sealId = 111 };
            PrintResponse("解除签名免验证签", client, cancelPersonalSealFreeSignReq, accessToken);

            //解除印章免验证签
            CancelSealFreeSignReq cancelSealFreeSignReq = new CancelSealFreeSignReq()
            { openCorpId = openCorpId, businessId = "", sealId = 111 };
            PrintResponse("解除印章免验证签", client, cancelSealFreeSignReq, accessToken);

            //创建法定代表人图片印章
            CreateLegalRepresentativeSealByImageReq createLegalRepresentativeSealByImageReq = new CreateLegalRepresentativeSealByImageReq()
            { openCorpId = openCorpId, openUserId = openUserId, createSerialNo = "", entityId = "", sealColor = "", sealHeight = 166, sealWidth = 166, sealTag = "", sealImage = "", sealName = "", sealOldStyle = 111 };
            PrintResponse("创建法定代表人图片印章", client, createLegalRepresentativeSealByImageReq, accessToken);

            //创建法定代表人模板印章
            CreateLegalRepresentativeSealByTemplateReq createLegalRepresentativeSealByTemplateReq = new CreateLegalRepresentativeSealByTemplateReq()
            { openCorpId = openCorpId, openUserId = openUserId, entityId = "" };
            PrintResponse("创建法定代表人模板印章", client, createLegalRepresentativeSealByTemplateReq, accessToken);

            //创建图片签名
            CreatePersonalSealByImageReq createPersonalSealByImageReq = new CreatePersonalSealByImageReq()
            { openUserId = openUserId, sealImage = "" };
            PrintResponse("创建图片签名", client, createPersonalSealByImageReq, accessToken);

            //创建模板签名
            CreatePersonalSealByTemplateReq createPersonalSealByTemplateReq = new CreatePersonalSealByTemplateReq()
            { openUserId = openUserId };
            PrintResponse("创建模板签名", client, createPersonalSealByTemplateReq, accessToken);

            //创建图片印章
            CreateSealByImageReq createSealByImageReq = new CreateSealByImageReq()
            { openCorpId = openCorpId };
            PrintResponse("创建图片印章", client, createSealByImageReq, accessToken);

            //创建模板印章
            CreateSealByTemplateReq createSealByTemplateReq = new CreateSealByTemplateReq()
            { openCorpId = openCorpId, };
            PrintResponse("创建模板印章", client, createSealByTemplateReq, accessToken);

            //删除签名
            DeletePersonalSealReq deletePersonalSealReq = new DeletePersonalSealReq()
            { };
            PrintResponse("删除签名", client, deletePersonalSealReq, accessToken);

            //获取签名创建链接
            GetPersonalSealCreateUrlReq getPersonalSealCreateUrlReq = new GetPersonalSealCreateUrlReq()
            { };
            PrintResponse("获取签名创建链接", client, getPersonalSealCreateUrlReq, accessToken);

            //获取签名管理链接
            GetPersonalSealManageUrlReq getPersonalSealManageUrlReq = new GetPersonalSealManageUrlReq()
            { };
            PrintResponse("获取签名管理链接", client, getPersonalSealManageUrlReq, accessToken);

            //获取企业用印员列表
            GetSealTagListReq getSealTagListReq = new GetSealTagListReq()
            { };
            PrintResponse("获取企业用印员列表", client, getSealTagListReq, accessToken);

            //查询成员被授权的印章列表
            GetUserSealListReq getUserSealListReq = new GetUserSealListReq()
            { };
            PrintResponse("查询成员被授权的印章列表", client, getUserSealListReq, accessToken);

            #endregion

            #region 文档管理

            ///通过网络文件地址上传
            UploadFileByUrlReq uploadFileByUrlReq = new UploadFileByUrlReq()
            {
                fileType = "doc",
                fileUrl = "http://static.fadada.com/fortest20170901/协议书范本.pdf"
            };

            ////接口调用
            //var reslt = client.GetHttpResponse(uploadFileByUrlReq, accessToken);
            //Console.WriteLine(JsonConvert.SerializeObject(reslt));
            //Console.ReadKey();

            //获取文件上传地址
            GetLocalUploadFileUrlReq getLocalUploadFileUrlReq = new GetLocalUploadFileUrlReq()
            {
                fileType = "doc"
            };
            ////PUT上传本地文件
            //var result = client.GetHttpResponse(getLocalUploadFileUrlReq, accessToken);//获取本地上传文件地址
            ////var filePath = "C:\\Users\\Fadada\\Desktop\\测试文档001.doc";
            //var filePath = "E:\\测试文档001.docx";
            //var resp = client.PutLocalfileUpload(result.data.uploadUrl, filePath);//开始put本地文件，返回200文件上传成功


            //文件处理
            FddUploadFiles uploadFile1 = new FddUploadFiles()
            {
                fileType = "doc",
                fileName = "测试文档001.doc",
                //fddFileUrl = result.data.fddFileUrl
            };
            //FddUploadFiles uploadFile2 = new FddUploadFiles()
            //{
            //    fileType = "",
            //    fileName = "",
            //    fddFileUrl = ""
            //};
            FileProcessReq fileProcessReq = new FileProcessReq()
            {
                fddFileUrlList = new FddUploadFiles[] { uploadFile1 }
            };
            var result1 = client.GetHttpResponse(fileProcessReq, accessToken);

            //获取文件对比页面链接
            GetOcrEditCompareUrlReq getOcrEditCompareUrlReq = new GetOcrEditCompareUrlReq()
            {
                originFileId = "qqq",
                targetFileId = "aaa"
            };

            //获取历史文件对比页面链接
            GetOcrEditCompareResultUrlReq getOcrEditCompareResultUrlReq = new GetOcrEditCompareResultUrlReq()
            {
                compareId = "www",
            };

            //获取合同智审页面链接
            GetOcrEditExamineUrlReq getOcrEditExamineUrlReq = new GetOcrEditExamineUrlReq()
            {
                fileId = "123",
            };

            //获取历史合同智审页面链接
            GetOcrEditExamineResultUrlReq getOcrEditExamineResultUrlReq = new GetOcrEditExamineResultUrlReq()
            {
                examineId = "667",
            };

            //文档验签
            FileVerifySignReq fileVerifySignReq = new FileVerifySignReq()
            {
                fileId = "eeeeee",
                fileHash = "iuhyt6353retegdte5ee6eteyr765dtdue7ye6eyery7ye"
            };

            //Console.WriteLine(JsonConvert.SerializeObject(resp));
            //Console.ReadKey();

            //查询文档关键字坐标
            GetKeywordPositionReq getKeywordPositionReq = new GetKeywordPositionReq()
            { };
            PrintResponse("查询文档关键字坐标", client, getKeywordPositionReq, accessToken);

            //OFD文件追加
            OfdFileMergeReq ofdFileMergeReq = new OfdFileMergeReq()
            { };
            PrintResponse("OFD文件追加", client, ofdFileMergeReq, accessToken);

            #endregion

            #region 模板管理

            //查询文档模板列表
            GetDocTemplateListReq getDocTemplateListReq = new GetDocTemplateListReq()
            {
                ownerId = new OpenId()
                {
                    idType = "person",
                    openId = openUserId
                },
                //listFilter = new DocTemplateListFilterInfo()
                //{
                //    docTemplateName = ""
                //},
                //listPageNo = 1,
                //listPageSize = 1
            };
            //查询文档模板详情
            DocTemplateDetailReq docTemplateDetailReq = new DocTemplateDetailReq()
            {
                ownerId = new OpenId()
                {
                    idType = "person",
                    openId = openUserId
                },
                docTemplateId = ""
            };

            //查询签署模板列表
            GetSignTemplateListReq getSignTemplateListReq = new GetSignTemplateListReq()
            {
                ownerId = new OpenId
                {
                    idType = "person",
                    openId = openUserId
                },
                listFilter = new SignTemplateListFilterInfo()
                {
                    signTemplateName = ""
                },
                listPageNo = 1,
                listPageSize = 1
            };
            //查询签署模板详情
            GetSignTemplateDetailReq getSignTemplateDetailReq = new GetSignTemplateDetailReq()
            {
                signTemplateId = signTemplateId,
                ownerId = new OpenId
                {
                    idType = "person",
                    openId = openUserId
                }
            };

            //获取模板新增链接
            GetTemplateCreateUrlReq getTemplateCreateUrlReq = new GetTemplateCreateUrlReq()
            {
                openCorpId = openCorpId,
                type = "doc",
                //redirectUrl=""
            };
            //获取模板编辑链接
            GetTemplateEditUrlReq getTemplateEditUrlReq = new GetTemplateEditUrlReq()
            {
                openCorpId = openCorpId,
                templateId = "",
            };
            //获取模板预览链接
            GetTemplatePreviewUrlReq getTemplatePreviewUrlReq = new GetTemplatePreviewUrlReq()
            {
                openCorpId = openCorpId,
                templateId = ""
            };
            //获取模板管理链接
            GetTemplateManageUrlReq getPageManageUrlReq = new GetTemplateManageUrlReq()
            {
                openCorpId = openCorpId
                //redirectUrl = ""
            };

            //查询应用文档模板列表
            GetAppDocTemplateListReq getAppDocTemplateListReq = new GetAppDocTemplateListReq()
            {
            };

            //获取应用文档模板详情
            GetAppDocTemplateDetailReq getAppDocTemplateDetailReq = new GetAppDocTemplateDetailReq()
            {
            };

            //查询应用签署任务模板列表
            GetAppSignTemplateListReq getAppSignTemplateListReq = new GetAppSignTemplateListReq()
            {
            };

            //获取应用签署任务模板详情
            GetAppSignTemplateDetailReq getAppSignTemplateDetailReq = new GetAppSignTemplateDetailReq()
            {
            };

            //获取应用模板新增链接
            GetAppTemplateCreateUrlReq getAppTemplateCreateUrlReq = new GetAppTemplateCreateUrlReq()
            {
            };

            //获取应用模板编辑链接
            GetAppTemplateEditUrlReq getAppTemplateEditUrlReq = new GetAppTemplateEditUrlReq()
            {
            };

            //获取应用模板预览链接
            GetAppTemplatePreviewUrlReq getAppTemplatePreviewUrlReq = new GetAppTemplatePreviewUrlReq()
            {
            };

            //复制新增文档模板
            CopyCreateDocTemplateReq copyCreateDocTemplateReq = new CopyCreateDocTemplateReq()
            { };
            PrintResponse("复制新增文档模板", client, copyCreateDocTemplateReq, accessToken);

            //创建自定义控件
            CreateCorpFieldReq createCorpFieldReq = new CreateCorpFieldReq()
            { };
            PrintResponse("创建自定义控件", client, createCorpFieldReq, accessToken);

            //创建文档模板
            CreateDocTemplateReq createDocTemplateReq = new CreateDocTemplateReq()
            { };
            PrintResponse("创建文档模板", client, createDocTemplateReq, accessToken);

            //删除自定义控件
            DeleteCorpFieldReq deleteCorpFieldReq = new DeleteCorpFieldReq()
            { };
            PrintResponse("删除自定义控件", client, deleteCorpFieldReq, accessToken);

            //删除文档模板
            DeleteDocTemplateReq deleteDocTemplateReq = new DeleteDocTemplateReq()
            { };
            PrintResponse("删除文档模板", client, deleteDocTemplateReq, accessToken);

            //查询应用文档模板列表
            DeleteSignTemplateReq deleteSignTemplateReq = new DeleteSignTemplateReq()
            { };
            PrintResponse("查询应用文档模板列表", client, deleteSignTemplateReq, accessToken);

            //填写文档模板生成文件
            DocTemplateFillValuesReq docTemplateFillValuesReq = new DocTemplateFillValuesReq()
            { };
            PrintResponse("填写文档模板生成文件", client, docTemplateFillValuesReq, accessToken);

            //查询自定义控件列表
            GetCorpFieldListReq getCorpFieldListReq = new GetCorpFieldListReq()
            { };
            PrintResponse("查询自定义控件列表", client, getCorpFieldListReq, accessToken);

            //启用/停用文档模板
            SetDocTemplateStatusReq setDocTemplateStatusReq = new SetDocTemplateStatusReq()
            { };
            PrintResponse("启用/停用文档模板", client, setDocTemplateStatusReq, accessToken);

            //启用/停用签署任务模板
            SetSignTemplateStatusReq setSignTemplateStatusReq = new SetSignTemplateStatusReq()
            { };
            PrintResponse("启用/停用签署任务模板", client, setSignTemplateStatusReq, accessToken);

            //删除应用文档模板
            DeleteAppTemplateReq deleteAppTemplateReq = new DeleteAppTemplateReq()
            { };
            PrintResponse("删除应用文档模板", client, deleteAppTemplateReq, accessToken);

            //删除应用签署任务模板
            DeleteSignAppTemplateReq deleteSignAppTemplateReq = new DeleteSignAppTemplateReq()
            { };
            PrintResponse("删除应用签署任务模板", client, deleteSignAppTemplateReq, accessToken);

            //启用/停用应用签署任务模板
            SetAppSignTemplateStatusReq setAppSignTemplateStatusReq = new SetAppSignTemplateStatusReq()
            { };
            PrintResponse("停用应用签署任务模板", client, setAppSignTemplateStatusReq, accessToken);

            //启用/停用应用文档模板
            SetTemplateStatusReq setTemplateStatusReq = new SetTemplateStatusReq()
            { };
            PrintResponse("启用/停用应用文档模板", client, setTemplateStatusReq, accessToken);

            #endregion

            #region 签署任务
            SignTaskActor signTaskActor1 = new SignTaskActor()
            {
                actor = new Actor()
                {
                    actorId = "个人方",
                    actorName = "个人方",
                    actorType = "person",
                    permissions = new string[]{"sign"},
                },
                fillFields = new FillField[] { },
                signConfigInfo = new SignConfigInfo() { },
                signFields = new SignField[] { }, 
            };


            /*可在文档中添加一些控件。当前仅支持签章控件*/
            Field field1 = new Field()
            {
                fieldId = docFileId,
                fieldName = "文本输入框",
                fieldType = "person_sign",
                position = new FieldPosition()
                {
                    positionMode = "pixel",
                    positionPageNo = 1,
                    positionX = "200",
                    positionY = "300"
                }
            };

            /*创建签署任务*/
            Doc doc1 = new Doc()
            {
                docId = "doc1",
                docName = "创建签署任务文档",
                docTemplateId = docTemplateId,
                // docFields = new Field[] { field1 }
            };
            //创建签署任务(基于即时文档或文档模板)
            CreateSignTaskReq createSignTaskReq = new CreateSignTaskReq()
            {
                signTaskSubject = "开始测试创建签署任务接口",
                initiator = new OpenId()
                {
                    idType = "corp",
                    openId = openCorpId
                },
                autoStart = false,
                autoFillFinalize = false,
                actors = new SignTaskActor[] { signTaskActor1 },
                docs = new Doc[] { doc1 }
            };
            {
                var rsp = client.GetHttpResponse(createSignTaskReq, accessToken);
                Console.WriteLine("创建签署任务");
                Console.WriteLine(JsonConvert.SerializeObject(rsp));
            }

            /*创建签署任务基于签署模板*/

            CreateSignTaskWithTemplateReq createSignTaskWithTemplateReq = new CreateSignTaskWithTemplateReq()
            {
                signTemplateId = signTemplateId,
                signTaskSubject = "开始基于签署模板创建签署任务",
                initiator = new OpenId()
                {
                    idType = "person",
                    openId = openUserId
                },
                autoStart = false,
                autoFillFinalize = false,
                actors = new SignTaskActor[] { signTaskActor1 }
            };

            //添加签署任务文档

            AddDocInfoReq addDocInfoReq = new AddDocInfoReq()
            {
                signTaskId = signTaskId,
                // docs = new Doc[] { doc1 }
            };

            //移除签署任务文档
            DeleteDocInfoReq deleteDocInfoReq = new DeleteDocInfoReq()
            {
                signTaskId = signTaskId,
                docIds = new string[] { "1" }
            };

            //添加签署任务控件

            AddFieldInfo addFieldInfo1 = new AddFieldInfo()
            {
                docId = 3,
                docFields = new Field[] { field1 }
            };
            AddSignTaskFieldReq addSignTaskFieldReq = new AddSignTaskFieldReq()
            {
                signTaskId = signTaskId,
                fields = new AddFieldInfo[] { addFieldInfo1 }
            };

            //移除签署任务控件
            Fields fields1 = new Fields()
            {
                docId = 3,
                fieldIds = new string[] { "001" }
            };
            DeleteSignTaskFieldReq deleteSignTaskFieldReq = new DeleteSignTaskFieldReq()
            {
                signTaskId = signTaskId,
                fields = new Fields[] { fields1 }
            };

            //填写签署任务控件内容
            DocFieldValueInfo docFieldValue1 = new DocFieldValueInfo()
            {
                docId = "0",
                fieldId = docFileId,
                fieldValue = "第2次 接口填充1%"
            };
            DocFieldValueInfo docFieldValue2 = new DocFieldValueInfo()
            {
                docId = "0",
                fieldId = docFileId,
                fieldValue = "第2次 接口填充2 ~!@#$%^&*()"
            };
            DocFieldValueInfo docFieldValue3 = new DocFieldValueInfo()
            {
                docId = "0",
                fieldId = docFileId,
                fieldValue = "第2次 接口填充3 ~!@#$$^&*()"
            };

            FillFieldValuesReq fillFieldValuesReq = new FillFieldValuesReq()
            {
                signTaskId = signTaskId,
                docFieldValues = new DocFieldValueInfo[] { docFieldValue1, docFieldValue2, docFieldValue3 }
            };


            //添加签署任务附件
            Attach attach1 = new Attach()
            {
                attachFileId = attachFileId,
                attachId = attachId,
                attachName = "保全报告"
            };
            AddAttachReq addAttachReq = new AddAttachReq()
            {
                signTaskId = signTaskId,
                attachs = new Attach[] { attach1 }
            };
            //移除签署任务附件  
            DeleteAttachReq deleteAttachReq = new DeleteAttachReq()
            {
                signTaskId = signTaskId,
                attachIds = new string[] { "1" }
            };
            //添加签署任务参与方
            AddActorsReq addActorsReq = new AddActorsReq()
            {
                signTaskId = signTaskId,
                actors = new SignTaskActor[] { signTaskActor1 }
            };

            //移除签署任务参与方
            DeleteActorReq deleteActorReq = new DeleteActorReq()
            {
                signTaskId = signTaskId,
                actorIds = new string[] { "个人" }//参与方在签署任务中被设定的唯一标识                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  

            };
            //提交签署任务
            StartSignTaskReq startSignTaskReq = new StartSignTaskReq()
            {
                signTaskId = signTaskId
            };

            //定稿签署任务文档
            FinalizeSignTaskDocReq finalizeSignTaskDoc = new FinalizeSignTaskDocReq()
            {
                signTaskId = signTaskId
            };

            //阻塞签署任务
            BlockSignTaskReq blockSignTaskReq = new BlockSignTaskReq()
            {
                signTaskId = signTaskId,
                actorId = "1"
            };
            //解阻签署任务
            UnblockSignTaskReq unblockSignTaskReq = new UnblockSignTaskReq()
            {
                signTaskId = signTaskId,
                actorId = "指定的参与方在本签署任务中的标识"
            };

            //撤销签署任务
            CancelSignTaskReq cancelSignTaskReq = new CancelSignTaskReq()
            {
                signTaskId = signTaskId
            };

            //查询企业签署任务文件夹
            GetSignTaskCatalogListReq getSignTaskCatalogListReq = new GetSignTaskCatalogListReq()
            {
            };

            //查询签署任务控件信息
            GetSignTaskFieldListReq getSignTaskFieldListReq = new GetSignTaskFieldListReq()
            {
                signTaskId = signTaskId
            };

            //查询签署任务参与方信息
            GetSignTaskActorListReq getSignTaskActorListReq = new GetSignTaskActorListReq()
            {
                signTaskId = signTaskId
            };
            {
                var rsp = client.GetHttpResponse(getSignTaskActorListReq, accessToken);
                Console.WriteLine("查询签署任务参与方信息");
                Console.WriteLine(JsonConvert.SerializeObject(rsp));
            }
            

            //获取签署任务参与方专属链接
            GetSignTaskActorUrlReq getSignTaskActorUrlReq = new GetSignTaskActorUrlReq()
            {
                signTaskId = signTaskId,
                actorId = "参与方在签署任务中被设定的唯一标识"
            };

            //获取签署任务编辑链接
            GetSignTaskEditUrlReq getSignTaskEditUrlReq = new GetSignTaskEditUrlReq()
            {
                signTaskId = signTaskId,
            };

            //获取签署任务预览链接
            GetSignTaskPreviewUrlReq getSignTaskPreviewUrlReq = new GetSignTaskPreviewUrlReq()
            {
                signTaskId = signTaskId,
            };

            //获取签署任务批量签署链接
            GetSignTaskBatchSignUrlReq getSignTaskBatchSignUrlReq = new GetSignTaskBatchSignUrlReq()
            {
                clientUserId = "",
            };

            //获取指定归属方的签署任务列表
            GetOwnerSignTaskListReq getOwnerSignTaskListReq = new GetOwnerSignTaskListReq()
            {
                ownerId = new OpenId()
                {

                },
                ownerRole = "",
                listFilter = new SignListFilter()
                {
                    signTaskSubject = "",
                    signTaskStatus = new string[] { "fill_progress", "sign_progress", "task_finished" }
                },
                listPageNo = 1,
                listPageSize = 1
            };

            //获取指定归属方的签署任务文档下载地址
            GetOwnerSignTaskUrlReq getOwnerSignTaskUrlReq = new GetOwnerSignTaskUrlReq()
            {
                ownerId = new OpenId()
                {

                },
                signTaskId = signTaskId,
                fileType = "",
                id = ""
            };

            //获取签署任务详情
            GetAppSignTaskDetailReq getSignTaskDetailReq = new GetAppSignTaskDetailReq()
            {
                signTaskId = signTaskId
            };
            {
                var rsp = client.GetHttpResponse(getSignTaskDetailReq, accessToken);
                Console.WriteLine("获取签署任务详情");
                Console.WriteLine(JsonConvert.SerializeObject(rsp));
            }

            //查询签署任务审批信息
            GetSignTaskApprovalInfoReq getSignTaskApprovalInfoReq = new GetSignTaskApprovalInfoReq()
            {
                signTaskId = signTaskId
            };

            //催办签署任务
            SignTaskUrgeReq signTaskUrgeReq = new SignTaskUrgeReq()
            {
                signTaskId = signTaskId
            };

            //获取签署任务公证处保全报告下载地址
            GetSignTaskDownloadEvidenceReportReq getSignTaskDownloadEvidenceReportReq = new GetSignTaskDownloadEvidenceReportReq()
            {
                signTaskId = signTaskId
            };

            //作废签署任务
            CancelSignTaskCreateReq cancelSignTaskCreateReq = new CancelSignTaskCreateReq()
            { };
            PrintResponse("作废签署任务", client, cancelSignTaskCreateReq, accessToken);

            //删除签署任务
            DeleteSignTaskReq deleteSignTaskReq = new DeleteSignTaskReq()
            { };
            PrintResponse("删除签署任务", client, deleteSignTaskReq, accessToken);

            //延期签署任务
            ExtensionReq extensionReq = new ExtensionReq()
            { };
            PrintResponse("延期签署任务", client, extensionReq, accessToken);

            //结束签署任务
            FinishSignTaskReq finishSignTaskReq = new FinishSignTaskReq()
            { };
            PrintResponse("结束签署任务", client, finishSignTaskReq, accessToken);

            //获取参与方签署音视频
            GetActorAudioVideoReq getActorAudioVideoReq = new GetActorAudioVideoReq()
            { };
            PrintResponse("获取参与方签署音视频", client, getActorAudioVideoReq, accessToken);

            //查询签署业务类型列表
            GetSignTaskBusinessTypeListReq getSignTaskBusinessTypeListReq = new GetSignTaskBusinessTypeListReq()
            { };
            PrintResponse("查询签署业务类型列表", client, getSignTaskBusinessTypeListReq, accessToken);

            //获取签署参与方刷脸底图
            GetSignTaskFacePictureReq getSignTaskFacePictureReq = new GetSignTaskFacePictureReq()
            { };
            PrintResponse("获取签署参与方刷脸底图", client, getSignTaskFacePictureReq, accessToken);

            //获取图片版签署文档下载地址
            GetSignTaskPicDocTicketReq getSignTaskPicDocTicketReq = new GetSignTaskPicDocTicketReq()
            { };
            PrintResponse("获取图片版签署文档下载地址", client, getSignTaskPicDocTicketReq, accessToken);

            //获取签署文档切图
            GetSignTaskSlicingDocReq getSignTaskSlicingDocReq = new GetSignTaskSlicingDocReq()
            { };
            PrintResponse("获取签署文档切图", client, getSignTaskSlicingDocReq, accessToken);

            //获取参与方签署链接（API3.0任务专属）
            GetV3ActorSignTaskUrlReq getV3ActorSignTaskUrlReq = new GetV3ActorSignTaskUrlReq()
            { };
            PrintResponse("获取参与方签署链接（API3.0任务专属）", client, getV3ActorSignTaskUrlReq, accessToken);

            //修改签署任务参与方
            ModifyActorReq modifyActorReq = new ModifyActorReq()
            { };
            PrintResponse("修改签署任务参与方", client, modifyActorReq, accessToken);

            //申请证据报告
            SignTaskApplyReportReq signTaskApplyReportReq = new SignTaskApplyReportReq()
            { };
            PrintResponse("申请证据报告", client, signTaskApplyReportReq, accessToken);

            //下载证据报告
            SignTaskDownloadReportReq signTaskDownloadReportReq = new SignTaskDownloadReportReq()
            { };
            PrintResponse("下载证据报告", client, signTaskDownloadReportReq, accessToken);

            //查询参与方证书文件
            SignTaskGetCerInfoReq signTaskGetCerInfoReq = new SignTaskGetCerInfoReq()
            { };
            PrintResponse("查询参与方证书文件", client, signTaskGetCerInfoReq, accessToken);

            //查询签署完成的文件
            SignTaskGetFileReq signTaskGetFileReq = new SignTaskGetFileReq()
            { };
            PrintResponse("查询签署完成的文件", client, signTaskGetFileReq, accessToken);

            //驳回填写签署任务
            SignTaskIgnoreReq signTaskIgnoreReq = new SignTaskIgnoreReq()
            { };
            PrintResponse("驳回填写签署任务", client, signTaskIgnoreReq, accessToken);

            //获取签署任务预填写链接
            SignTaskPreFillUrlReq signTaskPreFillUrlReq = new SignTaskPreFillUrlReq()
            { };
            PrintResponse("获取签署任务预填写链接", client, signTaskPreFillUrlReq, accessToken);

            #endregion

            #region 计费管理

            GetBillUrlReq getBillUrlReq = new GetBillUrlReq()
            {
                openId = new OpenId()
                {
                    idType = "person",
                    openId = openUserId
                },
                urlType = "",
                redirectUrl = ""
            };

            //查询账户可用余量
            GetUsageAvailablenumReq getUsageAvailablenumReq = new GetUsageAvailablenumReq()
            { };
            PrintResponse("查询账户可用余量", client, getUsageAvailablenumReq, accessToken);

            #endregion

            #region EUI页面资源管理

            //获取应用级资源访问链接
            GetAppResourceUrlReq getAppResourceUrlReq = new GetAppResourceUrlReq()
            {
                //ownerId = new OpenId()
                //{

                //},
                resource = new AppResouce()
                {
                    resourceId = "SIGNTASK",
                    action = "EDIT",
                    Params = "signTaskId=1658716488492151628"
                }
            };

            //获取用户级资源访问链接
            GetUserResourceUrlReq getUserResourceUrlReq = new GetUserResourceUrlReq()
            {
                openCorpId = "",
                clientUserId = "",
                resource = new UserResouce()
                {
                    resourceId = "",
                    action = "",
                    Params = ""
                }
            };


            #endregion

            #region 业务控件管理

            //创建业务控件
            AppFieldCreateReq appFieldCreateReq = new AppFieldCreateReq()
            {
            };

            //修改业务控件
            AppFieldModifyReq appFieldModifyReq = new AppFieldModifyReq()
            {
            };

            //设置业务控件状态
            AppFieldSetStatusReq appFieldSetStatusReq = new AppFieldSetStatusReq()
            {
            };

            //查询业务控件列表
            GetAppFieldListReq getAppFieldListReq = new GetAppFieldListReq()
            {
            };
            #endregion

            #region 审批管理

            //查询审批单据详情
            GetApprovalDetailReq getApprovalDetailReq = new GetApprovalDetailReq()
            { };
            PrintResponse("查询审批单据详情", client, getApprovalDetailReq, accessToken);

            //查询审批流程详情
            GetApprovalFlowDetailReq getApprovalFlowDetailReq = new GetApprovalFlowDetailReq()
            { };
            PrintResponse("查询审批流程详情", client, getApprovalFlowDetailReq, accessToken);

            //查询审批流程列表
            GetApprovalFlowListReq getApprovalFlowListReq = new GetApprovalFlowListReq()
            { };
            PrintResponse("查询审批流程列表", client, getApprovalFlowListReq, accessToken);

            //查询审批单据列表
            GetApprovalInfoListReq getApprovalInfoListReq = new GetApprovalInfoListReq()
            { };
            PrintResponse("查询审批单据列表", client, getApprovalInfoListReq, accessToken);

            //获取审批链接
            GetApprovalUrlReq getApprovalUrlReq = new GetApprovalUrlReq()
            { };
            PrintResponse("获取审批链接", client, getApprovalUrlReq, accessToken);

            #endregion

            #region 合同起草

            //合同协商文件定稿
            DocFinalizeReq docFinalizeReq = new DocFinalizeReq()
            { };
            PrintResponse("合同协商文件定稿", client, docFinalizeReq, accessToken);

            //发起合同协商
            DraftCreateReq draftCreateReq = new DraftCreateReq()
            { };
            PrintResponse("发起合同协商", client, draftCreateReq, accessToken);

            //获取合同协商编辑链接
            GetEditUrlReq getEditUrlReq = new GetEditUrlReq()
            { };
            PrintResponse("获取合同协商编辑链接", client, getEditUrlReq, accessToken);

            //查询已定稿的合同文件
            GetFinishedFileReq getFinishedFileReq = new GetFinishedFileReq()
            { };
            PrintResponse("查询已定稿的合同文件", client, getFinishedFileReq, accessToken);

            //获取合同协商邀请链接
            GetInviteUrlReq getInviteUrlReq = new GetInviteUrlReq()
            { };
            PrintResponse("获取合同协商邀请链接", client, getInviteUrlReq, accessToken);

            //获取合同起草管理链接
            GetManageUrlReq getManageUrlReq = new GetManageUrlReq()
            { };
            PrintResponse("获取合同起草管理链接", client, getManageUrlReq, accessToken);

            #endregion

            #region 合同归档

            //查询已归档合同列表
            ArchivedListReq archivedListReq = new ArchivedListReq()
            { };
            PrintResponse("查询已归档合同列表", client, archivedListReq, accessToken);

            //查询归档文件夹列表
            ArchivesCatalogListReq archivesCatalogListReq = new ArchivesCatalogListReq()
            { };
            PrintResponse("查询归档文件夹列表", client, archivesCatalogListReq, accessToken);

            //查询已归档合同详情
            ArchivesDetailReq archivesDetailReq = new ArchivesDetailReq()
            { };
            PrintResponse("查询已归档合同详情", client, archivesDetailReq, accessToken);

            //合同归档
            ContactArchivedReq contactArchivedReq = new ContactArchivedReq()
            { };
            PrintResponse("合同归档", client, contactArchivedReq, accessToken);

            //创建合同履约提醒
            CreateOrModifyPerformanceReq createOrModifyPerformanceReq = new CreateOrModifyPerformanceReq()
            { };
            PrintResponse("创建合同履约提醒", client, createOrModifyPerformanceReq, accessToken);

            //删除合同履约提醒
            DeletePerformanceReq deletePerformanceReq = new DeletePerformanceReq()
            { };
            PrintResponse("删除合同履约提醒", client, deletePerformanceReq, accessToken);

            //获取合同归档链接
            GetArchivesManageUrlReq getArchivesManageUrlReq = new GetArchivesManageUrlReq()
            { };
            PrintResponse("获取合同归档链接", client, getArchivesManageUrlReq, accessToken);

            //查询合同履约提醒列表
            GetPerformanceListReq getPerformanceListReq = new GetPerformanceListReq()
            { };
            PrintResponse("查询合同履约提醒列表", client, getPerformanceListReq, accessToken);

            #endregion

            #region 智能比对

            //查询历史比对数据
            GetCompareResultDataReq getCompareResultDataReq = new GetCompareResultDataReq()
            { };
            PrintResponse("查询历史比对数据", client, getCompareResultDataReq, accessToken);

            //获取历史文件比对页面链接
            GetCompareResultUrlReq getCompareResultUrlReq = new GetCompareResultUrlReq()
            { };
            PrintResponse("获取历史文件比对页面链接", client, getCompareResultUrlReq, accessToken);

            //获取文件比对页面链接
            GetCompareUrlReq getCompareUrlReq = new GetCompareUrlReq()
            { };
            PrintResponse("获取文件比对页面链接", client, getCompareUrlReq, accessToken);

            #endregion

            #region 工具能力服务

            //银行卡OCR
            BankCardOcrReq bankCardOcrReq = new BankCardOcrReq()
            { };
            PrintResponse("银行卡OCR", client, bankCardOcrReq, accessToken);

            //个人银行卡四要素校验
            BankFourElementVerifyReq bankFourElementVerifyReq = new BankFourElementVerifyReq()
            { };
            PrintResponse("个人银行卡四要素校验", client, bankFourElementVerifyReq, accessToken);

            //个人银行卡三要素校验
            BankThreeElementVerifyReq bankThreeElementVerifyReq = new BankThreeElementVerifyReq()
            { };
            PrintResponse("个人银行卡三要素校验", client, bankThreeElementVerifyReq, accessToken);

            //营业执照OCR
            BizLicenseOcrReq bizLicenseOcrReq = new BizLicenseOcrReq()
            { };
            PrintResponse("营业执照OCR", client, bizLicenseOcrReq, accessToken);

            //企业组织三要素校验
            BusinessThreeElementVerifyReq businessThreeElementVerifyReq = new BusinessThreeElementVerifyReq()
            { };
            PrintResponse("企业组织三要素校验", client, businessThreeElementVerifyReq, accessToken);

            //驾驶证OCR
            DrivingLicenseOcrReq drivingLicenseOcrReq = new DrivingLicenseOcrReq()
            { };
            PrintResponse("驾驶证OCR", client, drivingLicenseOcrReq, accessToken);

            //企业工商信息查询
            GetCorpBusinessInfoReq getCorpBusinessInfoReq = new GetCorpBusinessInfoReq()
            { };
            PrintResponse("企业工商信息查询", client, getCorpBusinessInfoReq, accessToken);

            //查询人脸核验结果
            GetFaceRecognitionStatusReq getFaceRecognitionStatusReq = new GetFaceRecognitionStatusReq()
            { };
            PrintResponse("查询人脸核验结果", client, getFaceRecognitionStatusReq, accessToken);

            //获取人脸核验链接
            GetFaceRecognitionUrlReq getFaceRecognitionUrlReq = new GetFaceRecognitionUrlReq()
            { };
            PrintResponse("获取人脸核验链接", client, getFaceRecognitionUrlReq, accessToken);

            //获取要素校验身份证图片下载链接
            GetIdCardImageDownloadUrlReq getIdCardImageDownloadUrlReq = new GetIdCardImageDownloadUrlReq()
            { };
            PrintResponse("获取要素校验身份证图片下载链接", client, getIdCardImageDownloadUrlReq, accessToken);

            //获取个人银行卡四要素校验链接
            GetUserFourElementVerifyUrlReq getUserFourElementVerifyUrlReq = new GetUserFourElementVerifyUrlReq()
            { };
            PrintResponse("获取个人银行卡四要素校验链接", client, getUserFourElementVerifyUrlReq, accessToken);

            //获取个人运营商三要素校验链接
            GetUserThreeElementVerifyUrlReq getUserThreeElementVerifyUrlReq = new GetUserThreeElementVerifyUrlReq()
            { };
            PrintResponse("获取个人运营商三要素校验链接", client, getUserThreeElementVerifyUrlReq, accessToken);

            //身份证OCR
            IdCardOcrReq idCardOcrReq = new IdCardOcrReq()
            { };
            PrintResponse("身份证OCR", client, idCardOcrReq, accessToken);

            //人脸图片比对校验
            IdCardThreeElementVerifyReq idCardThreeElementVerifyReq = new IdCardThreeElementVerifyReq()
            { };
            PrintResponse("人脸图片比对校验", client, idCardThreeElementVerifyReq, accessToken);

            //个人二要素校验
            IdentityTwoElementVerifyReq identityTwoElementVerifyReq = new IdentityTwoElementVerifyReq()
            { };
            PrintResponse("个人二要素校验", client, identityTwoElementVerifyReq, accessToken);

            //通行证OCR
            MainlandPermitOcrReq mainlandPermitOcrReq = new MainlandPermitOcrReq()
            { };
            PrintResponse("通行证OCR", client, mainlandPermitOcrReq, accessToken);

            //个人运营商三要素校验
            TelecomThreeElementVerifyReq telecomThreeElementVerifyReq = new TelecomThreeElementVerifyReq()
            { };
            PrintResponse("个人运营商三要素校验", client, telecomThreeElementVerifyReq, accessToken);

            //行驶证OCR
            VehicleLicenseOcrReq vehicleLicenseOcrReq = new VehicleLicenseOcrReq()
            { };
            PrintResponse("行驶证OCR", client, vehicleLicenseOcrReq, accessToken);

            #endregion

            #region 回调管理

            //查询回调列表
            GetCallBackListReq getCallBackListReq = new GetCallBackListReq()
            { };
            PrintResponse("查询回调列表", client,  getCallBackListReq, accessToken);

            #endregion

            #endregion

            Console.ReadKey();
        }

        static void PrintResponse<T>(string name, OpenApiClient client, BaseReq<T> req, string accessToken ) where T : class, new()
        {
            //接口调用
            var reslt = client.GetHttpResponse(req, accessToken);
            Console.WriteLine("【" + name + "】" + JsonConvert.SerializeObject(reslt));
            //Console.ReadKey();
        }
    }
}
