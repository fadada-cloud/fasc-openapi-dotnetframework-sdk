﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Dept
{
    /// <summary>
    /// 部门信息修改事件
    /// </summary>
    public class OrganizationDeptModifyDto
    {
        public string eventTime { get; set; }
        public string openCorpId { get; set; }
        public long deptId { get; set; }
        public string clientCorpId { get; set; }
    }
}
