﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Draft
{
    public class BaseDraftContractCallBackDto
    {
        public string eventTime { get; set; }
        public string contractConsultId { get; set; }
        public string contractStatus { get; set; }
        public string userName { get; set; }
        public string corpName { get; set; }
    }
}
