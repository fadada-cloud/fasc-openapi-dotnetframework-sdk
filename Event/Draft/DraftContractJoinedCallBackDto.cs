﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Draft
{
    /// <summary>
    /// 参与方加入协商事件
    /// </summary>
    public class DraftContractJoinedCallBackDto : BaseDraftContractCallBackDto
    {
    }
}
