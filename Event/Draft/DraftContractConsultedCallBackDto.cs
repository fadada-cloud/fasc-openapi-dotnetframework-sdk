﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Draft
{
    /// <summary>
    /// 协商流程完成事件
    /// </summary>
    public class DraftContractConsultedCallBackDto : BaseDraftContractCallBackDto
    {
    }
}
