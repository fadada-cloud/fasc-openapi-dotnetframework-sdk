﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Template
{
    /// <summary>
    /// 模板创建事件
    /// </summary>
    public class TemplateCreateCallbackDto
    {
        public string eventTime { get; set; }
        public string openCorpId { get; set; }
        public string templateId { get; set; }
        public string type { get; set; }
        public string createSerialNo { get; set; }
        public string clientCorpId { get; set; }
    }
}
