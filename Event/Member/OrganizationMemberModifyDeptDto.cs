﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Member
{
    /// <summary>
    /// 成员所属部门修改事件
    /// </summary>
    public class OrganizationMemberModifyDeptDto : BaseOrgnizationMemberDto
    {
    }
}
