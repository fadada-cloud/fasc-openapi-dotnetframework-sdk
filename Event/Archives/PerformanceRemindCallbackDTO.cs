﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Archives
{
    public class PerformanceRemindCallbackDTO
    {
        public string eventTime { get; set; }
        public string openCorpId { get; set; }
        public string archivesId { get; set; }
        public string performanceId { get; set; }
        public string performanceType { get; set; }
        public string[] reminder { get; set; }
    }
}
