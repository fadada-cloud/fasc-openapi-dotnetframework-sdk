﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Auth.User
{
    /// <summary>
    /// 个人用户授权事件
    /// </summary>
    public class UserAuthorizeDto
    {
        public string eventTime { get; set; }
        public string openUserId { get; set; }
        public string authResult { get; set; }
        public string authFailedReason { get; set; }
        public string[] authScope { get; set; }
        public string identProcessStatus { get; set; }
        public string identMethod { get; set; }
        public string identFailedReason { get; set; }
        public string clientUserId { get; set; }
    }
}
