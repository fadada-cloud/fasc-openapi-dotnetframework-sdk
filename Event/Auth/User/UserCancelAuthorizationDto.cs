﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Auth.User
{
    /// <summary>
    /// 个人用户解除授权事件
    /// </summary>
    public class UserCancelAuthorizationDto
    {
        public string eventTime { get; set; }
        public string clientUserId { get; set; }
        public string openUserId { get; set; }
    }
}
