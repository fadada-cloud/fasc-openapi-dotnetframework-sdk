﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Auth.User
{
    /// <summary>
    /// 个人三要素校验事件
    /// </summary>
    public class UserThreeElementVerifyDto
    {
        public string eventTime { get; set; }
        public string clientUserId { get; set; }
        public bool verifyResult { get; set; }
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string verifyId { get; set; }
        public string mobile { get; set; }
    }
}
