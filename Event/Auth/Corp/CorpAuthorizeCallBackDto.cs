﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Auth.Corp
{
    /// <summary>
    /// 企业用户授权事件
    /// </summary>
    public class CorpAuthorizeCallBackDto
    {
        public string eventTime { get; set; }
        public string[] clientUserIds { get; set; }
        public string clientCorpId { get; set; }
        public string openCorpId { get; set; }
        public string existClientCorpId { get; set; }
        public string existOpenCorpId { get; set; }
        public string authResult { get; set; }
        public string authFailedReason { get; set; }
        public string[] authScope { get; set; }
        public string corpIdentProcessStatus { get; set; }
        public string corpIdentFailedReason { get; set; }
        public string corpIdentMethod { get; set; }
    }
}
