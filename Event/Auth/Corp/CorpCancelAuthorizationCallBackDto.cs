﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Auth.Corp
{
    /// <summary>
    /// 企业解除授权
    /// </summary>
    public class CorpCancelAuthorizationCallBackDto
    {
        public string eventTime { get; set; }
        public string clientCorpId { get; set; }
        public string openCorpId { get; set; }
    }
}
