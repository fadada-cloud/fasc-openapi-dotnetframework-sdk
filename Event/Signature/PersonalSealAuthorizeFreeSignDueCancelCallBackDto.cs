﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signature
{
    /// <summary>
    /// 签名免验证签即将到期事件
    /// </summary>
    public class PersonalSealAuthorizeFreeSignDueCancelCallBackDto
    {
        public string eventTime { get; set; }
        public string openUserId { get; set; }
        public long sealId { get; set; }
        public string businessId { get; set; }
        public string clientUserId { get; set; }
        public string grantEndTime { get; set; }
    }
}
