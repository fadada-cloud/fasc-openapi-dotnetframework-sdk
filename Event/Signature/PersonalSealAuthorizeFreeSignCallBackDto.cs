﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signature
{
    /// <summary>
    /// 个人签名授权免验证签事件
    /// </summary>
    public class PersonalSealAuthorizeFreeSignCallBackDto
    {
        public string eventTime { get; set; }
        public string openUserId { get; set; }
        public long sealId { get; set; }
        public string businessId { get; set; }
        public string expiresTime { get; set; }
        public string clientUserId { get; set; }
    }
}
