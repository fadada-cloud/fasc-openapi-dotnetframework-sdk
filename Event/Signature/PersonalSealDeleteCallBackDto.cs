﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signature
{
    /// <summary>
    /// 签名删除事件
    /// </summary>
    public class PersonalSealDeleteCallBackDto
    {
        public string eventTime { get; set; }
        public long sealId { get; set; }
        public string openUserId { get; set; }
        public string clientUserId { get; set; }
    }
}
