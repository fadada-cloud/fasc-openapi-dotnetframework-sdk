﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 签署任务参与方填写事件
    /// </summary>
    public class SignTaskActorFilledCallbackDto : BaseSignTaskActorCallbackDto
    {
    }
}
