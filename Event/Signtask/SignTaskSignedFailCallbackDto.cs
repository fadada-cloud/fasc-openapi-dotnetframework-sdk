﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 签署任务签署失败事件
    /// </summary>
    public class SignTaskSignedFailCallbackDto : BaseSignTaskStatusCallbackDto
    {
        public string actorId { get; set; }
        public bool verifyFreeSign { get; set; }
        public string signFailedReason { get; set; }
    }
}
