﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 参与方企业成员移除事件
    /// </summary>
    public class SignTaskMemberRemoved : BaseSignTaskMemberCallbackDto
    {
    }
}
