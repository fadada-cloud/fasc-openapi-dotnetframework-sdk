﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 签署任务待处理事件（API3.0任务专属）
    /// </summary>
    public class SignTaskPendingCallbackDto
    {
        public string eventTime { get; set; }
        public string signTaskId { get; set; }
    }
}
