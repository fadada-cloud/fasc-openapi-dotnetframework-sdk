﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 签署任务参与方/抄送方阅读事件
    /// </summary>
    public class SignTaskReadCallBackDto
    {
        public string eventTime { get; set; }
        public string signTaskId { get; set; }
        public string actorId { get; set; }
        public string signTaskStatus { get; set; }
        public string transReferenceId { get; set; }
    }
}
