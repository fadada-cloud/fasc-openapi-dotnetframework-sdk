﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 签署任务作废事件
    /// </summary>
    public class SignTaskAbolishCallbackDto : BaseSignTaskStatusCallbackDto
    {
        public string abolishedSignTaskId { get; set; }
    }
}
