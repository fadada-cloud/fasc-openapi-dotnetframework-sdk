﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    public class BaseSignTaskActorCallbackDto
    {
        public string eventTime { get; set; }
        public string signTaskId { get; set; }
        public string signTaskStatus { get; set; }
        public string actorId { get; set; }
        public string userName { get; set; }
        public string transReferenceId { get; set; }
    }
}
