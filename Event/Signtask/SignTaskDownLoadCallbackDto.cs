﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 签署文档批量下载事件
    /// </summary>
    public class SignTaskDownLoadCallbackDto
    {
        public string eventTime { get; set; }
        public string downloadId { get; set; }
        public string downloadUrl { get; set; }
        public string status { get; set; }
    }
}
