﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 签署任务参与方签署成功事件
    /// </summary>
    public class SignTaskActorSignedCallbackDto : BaseSignTaskActorCallbackDto
    {
        public bool? verifyFreeSign { get; set; }
    }
}
