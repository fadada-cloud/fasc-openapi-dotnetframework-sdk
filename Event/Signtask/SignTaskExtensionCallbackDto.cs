﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 签署任务延期事件
    /// </summary>
    public class SignTaskExtensionCallbackDto : BaseSignTaskStatusCallbackDto
    {
        public string expiresTime { get; set; }
        public string userName { get; set; }
    }
}
