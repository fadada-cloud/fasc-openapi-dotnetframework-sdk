﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    public class BaseSignTaskMemberCallbackDto
    {
        public string eventTime { get; set; }
        public string signTaskId { get; set; }
        public string signTaskStatus { get; set; }
        public ActorIInfo[] actorIInfo { get; set; }
        public string transReferenceId { get; set; }
    }

    public class ActorIInfo
    {
        public string actorId { get; set; }
        public MemberInfo[] memberInfo { get; set; }
    }

    public class MemberInfo
    {
        public string memberId { get; set; }
        public string userName { get; set; }
        public string permission { get; set; }
    }
}
