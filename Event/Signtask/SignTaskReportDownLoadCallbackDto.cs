﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Signtask
{
    /// <summary>
    /// 签署任务证据报告生成事件
    /// </summary>
    public class SignTaskReportDownLoadCallbackDto
    {
        public string eventTime { get; set; }
        public long reportDownloadId { get; set; }
        public string reportType { get; set; }
        public string failReason { get; set; }
        public string status { get; set; }
    }
}
