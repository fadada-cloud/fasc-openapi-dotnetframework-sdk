﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.MemberCorp
{
    /// <summary>
    /// 成员企业相关事件
    /// </summary>
    public class MemberCorpEntityManageCallBackDto
    {
        public string eventTime { get; set; }
        public string openCorpId { get; set; }
        public string entityId { get; set; }
        public string clientCorpId { get; set; }
        public string manageType { get; set; }
    }
}
