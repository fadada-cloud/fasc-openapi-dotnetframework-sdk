﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Recognition
{
    /// <summary>
    /// （人脸核身）个人身份核验
    /// </summary>
    public class FaceRecognizeCallBackDto
    {
        public string eventTime { get; set; }
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string faceAuthMode { get; set; }
        public int? resultStatus { get; set; }
        public string resultTime { get; set; }
        public string failedReason { get; set; }
        public string urlStatus { get; set; }
        public string createSerialNo { get; set; }
    }
}
