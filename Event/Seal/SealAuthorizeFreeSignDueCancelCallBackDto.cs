﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章设置免验证签即将到期事件
    /// </summary>
    public class SealAuthorizeFreeSignDueCancelCallBackDto : EventCallBackDto
    {
        public long sealId { get; set; }
        public string businessId { get; set; }
        public string grantEndTime { get; set; }
    }
}
