﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章创建事件
    /// </summary>
    public class SealCreateCallBackDto : SealBasicCallBackDto
    {
        public long? verifyId { get; set; }
        public string createSerialNo { get; set; }
    }
}
