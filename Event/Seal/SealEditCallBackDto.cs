﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章基本信息修改事件
    /// </summary>
    public class SealEditCallBackDto : EventCallBackDto
    {
        public long sealId { get; set; }
    }
}
