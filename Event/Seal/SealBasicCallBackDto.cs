﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    public class SealBasicCallBackDto : EventCallBackDto
    {
        public long? sealId { get; set; }
    }
}
