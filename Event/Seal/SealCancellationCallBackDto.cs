﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章注销事件
    /// </summary>
    public class SealCancellationCallBackDto : EventCallBackDto
    {
        public long sealId { get; set; }
     }
}
