﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章审核不通过事件
    /// </summary>
    public class SealAuditRejectCallBackDto : EventCallBackDto
    {
        public long verifyId { get; set; }
        public string reason { get; set; }
    }
}
