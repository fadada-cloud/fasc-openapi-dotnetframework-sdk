﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    public class EventCallBackDto
    {
        public string eventTime { get; set; }
        public string openCorpId { get; set; }
        public string clientCorpId { get; set; }
    }
}
