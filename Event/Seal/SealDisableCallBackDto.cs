﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章停用事件
    /// </summary>
    public class SealDisableCallBackDto : EventCallBackDto
    {
        public long sealId { get; set; }
    }
}
