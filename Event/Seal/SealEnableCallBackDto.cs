﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章启用事件
    /// </summary>
    public class SealEnableCallBackDto : EventCallBackDto
    {
        public long sealId { get; set; }
    }
}
