﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章授权免验证签事件
    /// </summary>
    public class SealAuthorizeFreeSignCallBackDto : EventCallBackDto
    {
        public long sealId { get; set; }
        public string businessId { get; set; }
        public string expiresTime { get; set; }
    }
}
