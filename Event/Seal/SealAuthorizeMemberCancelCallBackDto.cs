﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章授权解除事件
    /// </summary>
    public class SealAuthorizeMemberCancelCallBackDto : EventCallBackDto 
    {
        public long[] sealIds { get; set; }
        public long[] memberIds { get; set; }
    }
}
