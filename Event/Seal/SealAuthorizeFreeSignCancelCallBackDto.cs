﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Seal
{
    /// <summary>
    /// 印章免验证签解除事件
    /// </summary>
    public class SealAuthorizeFreeSignCancelCallBackDto : EventCallBackDto
    {
        public long sealId { get; set; }
        public string businessId { get; set; }
    }
}
