﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Approval
{
    /// <summary>
    /// 审批发起事件
    /// </summary>
    public class ApprovalCreatCallBackDto
    {
        public string eventTime { get; set; }
        public string openCorpId { get; set; }
        public string approvalType { get; set; }
        public string templateId { get; set; }
        public string signTaskId { get; set; }
        public string approvalId { get; set; }
        public string approvalStatus { get; set; }
        public long? oprMemberId { get; set; }
        public string oprMemberName { get; set; }
        public NextApproveMember[] nextNodeMemberIds { get; set; }
        public string clientCorpId { get; set; }
    }
}
