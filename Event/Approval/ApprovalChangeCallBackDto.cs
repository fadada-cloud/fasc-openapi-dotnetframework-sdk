﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Event.Approval
{
    /// <summary>
    /// 审批变更事件
    /// </summary>
    public class ApprovalChangeCallBackDto
    {
        public string eventTime { get; set; }
        public string openCorpId { get; set; }
        public string approvalType { get; set; }
        public string templateId { get; set; }
        public string signTaskId { get; set; }
        public string approvalId { get; set; }
        public string approvalStatus { get; set; }
        public long? oprMemberId { get; set; }
        public string oprMemberName { get; set; }
        public string approverStatus { get; set; }
        public string note { get; set; }
        public NextApproveMember[] nextNodeMemberIds { get; set; }
        public string clientCorpId { get; set; }
    }

    public class NextApproveMember
    {
        public long? nextNodeMemberId { get; set; }
        public string nextNodeMemberName { get; set; }
    }
}
