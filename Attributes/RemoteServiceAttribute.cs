﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Attributes
{
    public  class RemoteServiceAttribute : Attribute
    {
        public string Url { get; set; }
        public string Method { get; set; }

        public RemoteServiceAttribute(string url,string method)
        {
            this.Url = url;
            this.Method = method;
        }
    }
}
