﻿using fasc_openapi_donet_sdk.Model.ResponseModel;

namespace fasc_openapi_donet_sdk.Model.RequestModel
{
    public class BaseReq<T> where T : class, new()
    {
        public BaseRes<T> GetResponse()
        {
            return new BaseRes<T>();
        }
    }
}
