﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCallBack;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCallBack
{
    // <summary>
    /// 查询回调列表
    /// </summary>
    [RemoteService("/callback/get-list", "POST")]
    public class GetCallBackListReq : BaseReq<GetCallBackListRes>
    {
        public string callbackType { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
    }
}
