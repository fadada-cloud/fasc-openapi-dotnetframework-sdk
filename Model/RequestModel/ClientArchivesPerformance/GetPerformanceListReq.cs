﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientArchivesPerformance
{
    // <summary>
    /// 查询合同履约提醒列表
    /// </summary>
    [RemoteService("/archives/performance/list", "POST")]
    public class GetPerformanceListReq : BaseReq<GetPerformanceListRes>
    {
        public string openCorpId { get; set; }
        public string mermberId { get; set; }
        public string archivesId { get; set; }
    }
}
