﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientArchivesPerformance
{
    // <summary>
    /// 删除合同履约提醒
    /// </summary>
    [RemoteService("/archives/performance/delete", "POST")]
    public class DeletePerformanceReq : BaseReq<VoidRes>
    {
        public string openCorpId { get; set; }
        public string archivesId { get; set; }
        public string performanceId { get; set; }
    }
}
