﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientArchivesPerformance
{
    // <summary>
    /// 创建合同履约提醒
    /// </summary>
    [RemoteService("/archives/performance/modify", "POST")]
    public class CreateOrModifyPerformanceReq : BaseReq<CreateOrModifyPerformanceRes>
    {
        public string openCorpId { get; set; }
        public string archivesId { get; set; }
        public string performanceId { get; set; }
        public string performanceType { get; set; }
        public string performanceName { get; set; }
        public string expireTime { get; set; }
        public string remindStartDate { get; set; }
        public string remindFrequency { get; set; }
        public int cycleDays { get; set; }
        public string remindTime { get; set; }
        public double amount { get; set; }
        public string[] reminder { get; set; }
        public bool? isRemind { get; set; }
    }
}
