﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientArchivesPerformance
{
    // <summary>
    /// 获取合同归档链接
    /// </summary>
    [RemoteService("/archives/get-archives-url", "POST")]
    public class GetArchivesManageUrlReq : BaseReq<GetArchivesManageUrlRes>
    {
        public string openCorpId { get; set; }
        public string clientUserId { get; set; }
        public string redirectUrl { get; set; }
    }
}
