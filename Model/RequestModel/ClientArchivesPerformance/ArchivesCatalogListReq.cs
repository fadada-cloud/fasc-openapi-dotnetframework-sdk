﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientArchivesPerformance
{
    // <summary>
    /// 查询归档文件夹列表
    /// </summary>
    [RemoteService("/archives/catalog-list", "POST")]
    public class ArchivesCatalogListReq : BaseReq<ArchivesCatalogListRes>
    {
        public string openCorpId { get; set; }
        public string catalogId { get; set; }
    }
}
