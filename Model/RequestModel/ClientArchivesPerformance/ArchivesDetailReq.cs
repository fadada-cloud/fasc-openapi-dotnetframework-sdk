﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientArchivesPerformance
{
    // <summary>
    /// 查询已归档合同详情
    /// </summary>
    [RemoteService("/archives/get-archived-detail", "POST")]
    public class ArchivesDetailReq : BaseReq<ArchivedDetailInfo>
    {
        public string openCorpId { get; set; }
        public string archivesId { get; set; }
    }
}
