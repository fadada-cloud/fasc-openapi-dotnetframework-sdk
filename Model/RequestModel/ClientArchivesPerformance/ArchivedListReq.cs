﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientArchivesPerformance
{
    // <summary>
    /// 查询已归档合同列表
    /// </summary>
    [RemoteService("/archives/get-archived-list", "POST")]
    public class ArchivedListReq : BaseReq<ArchivedListRes>
    {
        public string openCorpId { get; set; }
        public string contractType { get; set; }
        public string signTaskId { get; set; }
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
    }
}
