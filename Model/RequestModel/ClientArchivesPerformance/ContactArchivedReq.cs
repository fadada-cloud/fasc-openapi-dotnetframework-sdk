﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientArchivesPerformance
{
    // <summary>
    /// 合同归档
    /// </summary>
    [RemoteService("/archives/contact-archived", "POST")]
    public class ContactArchivedReq : BaseReq<ContactArchivedRes>
    {
        public string openCorpId { get; set; }
        public string contractType { get; set; }
        public string contractNo { get; set; }
        public string catalogId { get; set; }
        public string signTaskId { get; set; }
        public string fileId { get; set; }
        public string memberId { get; set; }
        public Attachment[] attachments { get; set; }
    }

    public class Attachment
    {
        public string attachmentId { get; set; }
        public string attachmentName { get; set; }
    }
}
