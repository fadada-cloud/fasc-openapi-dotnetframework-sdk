﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 创建成员
    /// </summary>
    [RemoteService("/corp/member/create", "POST")]
    public class CorpMemberCreateReq : BaseReq<CorpMemberCreateRes>
    {
        public string openCorpId { get; set; }
        public EmployeeInfo[] employeeInfos { get; set; }
        public bool? notifyActiveByEmail { get; set; }
        public string redirectUrl { get; set; }
    }

    public class EmployeeInfo
    {
        public string memberName { get; set; }
        public string internalIdentifier { get; set; }
        public string memberEmail { get; set; }
        public string memberMobile { get; set; }
        public long[] memberDeptIds { get; set; }
        public bool notifyActiveByEmail { get; set; }
    }
}
