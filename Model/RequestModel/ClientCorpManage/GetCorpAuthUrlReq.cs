﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientOrgManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 获取企业用户授权链接
    /// </summary>
    [RemoteService("/corp/get-auth-url", "POST")]
    public class GetCorpAuthUrlReq : BaseReq<GetCorpAuthUrlRes>
    {
        public string clientCorpId { get; set; }
        public string clientUserId { get; set; }
        public string accountName { get; set; }
        public corpIdentInfo corpIdentInfo { get; set; }
        public string[] corpNonEditableInfo { get; set; }
        public oprIdentInfo oprIdentInfo { get; set; }
        public bool corpIdentInfoMatch { get; set; }
        public string[] authScopes { get; set; }
        public string redirectUrl { get; set; }
        public string[] oprNonEditableInfo { get; set; }
        public string redirectMiniAppUrl { get; set; }
    }
    public class corpIdentInfo
    {
        public string corpName { get; set; }
        public string corpIdentType { get; set; }
        public string corpIdentNo { get; set; }
        public string legalRepName { get; set; }
        public string license { get; set; }
        public string[] corpIdentMethod { get; set; }
    }

    public class oprIdentInfo
    {
        public string userName { get; set; }
        public string userIdentType { get; set; }
        public string userIdentNo { get; set; }
        public string mobile { get; set; }
        public string bankAccountNo { get; set; }
        public string[] oprIdentMethod { get; set; }
    }
}
