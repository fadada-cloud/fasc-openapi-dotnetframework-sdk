﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 获取组织管理链接
    /// </summary>
    [RemoteService("/corp/organization/manage/get-url", "POST")]
    public class GetCorpOrganizationManageUrlReq : BaseReq<GetCorpOrganizationManageUrlRes>
    {
        public string openCorpId { get; set; }
        public string clientUserId { get; set; }
        public string[] modules { get; set; }
        public string redirectUrl { get; set; }
    }
}
