﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 查询部门详情
    /// </summary>
    [RemoteService("/corp/dept/get-detail", "POST")]
    public class GetCorpDeptDetailReq : BaseReq<GetCorpDeptDetailRes>
    {
        public string openCorpId { get; set; }
        public string deptId { get; set; }
    }
}
