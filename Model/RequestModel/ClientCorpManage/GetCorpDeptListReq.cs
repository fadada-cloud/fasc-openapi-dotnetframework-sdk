﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 查询部门列表
    /// </summary>
    [RemoteService("/corp/dept/get-list", "POST")]
    public class GetCorpDeptListReq : BaseReq<GetCorpDeptListRes>
    {
        public string openCorpId { get; set; }
        public long? parentDeptId { get; set; }
    }
}
