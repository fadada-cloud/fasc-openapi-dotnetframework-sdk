﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    // <summary>
    /// 查询相对方列表
    /// </summary>
    [RemoteService("/corp/member/get-active-url", "POST")]
    public class GetCounterpartListReq : BaseReq<GetCounterpartListRes>
    {
        public string openCorpId { get; set; }
    }
}
