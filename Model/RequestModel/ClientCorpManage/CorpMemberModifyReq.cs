﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 修改成员基本信息
    /// </summary>
    [RemoteService("/corp/member/modify", "POST")]
    public class CorpMemberModifyReq : BaseReq<CorpMemberModifyRes>
    {
        public string openCorpId { get; set; }
        public long memberId { get; set; }
        public string memberName { get; set; }
        public string internalIdentifier { get; set; }
        public string memberEmail { get; set; }
        public string memberMobile { get; set; }
        public long[] memberDeptIds { get; set; }
    }
}
