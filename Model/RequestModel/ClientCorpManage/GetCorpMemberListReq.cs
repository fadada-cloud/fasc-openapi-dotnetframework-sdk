﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 查询企业成员列表
    /// </summary>
    [RemoteService("/corp/member/get-list", "POST")]
    public class GetCorpMemberListReq : BaseReq<GetCorpMemberListRes>
    {
        public string ownerId { get; set; }
        public string openCorpId { get; set; }
        public CorpListFilter listFilter { get; set; }

        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
    }

    public class CorpListFilter
    {
        public string roleType { get; set; }
        public long? deptId { get; set; }
        public bool? fetchChild { get; set; }
    }
}
