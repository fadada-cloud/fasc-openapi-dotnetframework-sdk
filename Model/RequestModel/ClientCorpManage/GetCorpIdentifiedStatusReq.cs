﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 查询企业实名认证状态
    /// </summary>
    [RemoteService("/corp/get-identified-status", "POST")]
    public class GetCorpIdentifiedStatusReq : BaseReq<GetCorpIdentifiedStatusRes>
    {
        public string corpName { get; set; }
        public string corpIdentNo { get; set; }
    }
}
