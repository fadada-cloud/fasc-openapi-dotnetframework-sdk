﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 获取企业用户身份信息
    /// </summary>
    [RemoteService("/corp/get-identity-info", "POST")]
    public class GetCorpIdentityInfoReq : BaseReq<GetCorpIdentityInfoRes>
    {
        public string openCorpId { get; set; }
    }
}
