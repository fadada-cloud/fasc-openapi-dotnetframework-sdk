﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 删除部门
    /// </summary>
    [RemoteService("/corp/dept/delete", "POST")]
    public class GetCorpDeptDeleteReq : BaseReq<GGetCorpDeptDeleteRes>
    {
        public string openCorpId { get; set; }
        public string deptId { get; set; }
    }
}
