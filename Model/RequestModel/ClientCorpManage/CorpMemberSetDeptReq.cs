﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 获取成员激活链接
    /// </summary>
    [RemoteService("/corp/member/set-dept", "POST")]
    public class CorpMemberSetDeptReq : BaseReq<CorpMemberSetDeptRes>
    {
        public string openCorpId { get; set; }
        public long[] memberIds { get; set; }
        public long[] memberDeptIds { get; set; }
        public string model { get; set; }
    }
}
