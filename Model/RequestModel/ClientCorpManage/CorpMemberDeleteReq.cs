﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 删除成员
    /// </summary>
    [RemoteService("/corp/member/delete", "POST")]
    public class CorpMemberDeleteReq : BaseReq<CorpMemberDeleteRes>
    {
        public string openCorpId { get; set; }
        public long[] memberIds { get; set; }
    }
}
