﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 获取成员激活链接
    /// </summary>
    [RemoteService("/corp/member/get-active-url", "POST")]
    public class GetCorpMemberActiveUrlReq : BaseReq<GetCorpMemberActiveUrlRes>
    {
        public string openCorpId { get; set; }
        public long[] memberIds { get; set; }
        public string redirectUrl { get; set; }
    }
}
