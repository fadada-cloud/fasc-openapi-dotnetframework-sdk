﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 创建部门
    /// </summary>
    [RemoteService("/corp/dept/create", "POST")]
    public class GetCorpDeptCreateReq : BaseReq<GetCorpDeptCreateRes>
    {
        public string openCorpId { get; set; }
        public long? parentDeptId { get; set; }
        public string deptName { get; set; }
        public int? deptOrderNum { get; set; }
        public string identifier { get; set; }
    }
}
