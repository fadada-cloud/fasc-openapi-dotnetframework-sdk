﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 获取成员企业管理链接
    /// </summary>
    [RemoteService("/corp/entity/get-manage-url", "POST")]
    public class GetCorpEntityManageUrlReq : BaseReq<GetCorpEntityManageUrlRes>
    {
        public string openCorpId { get; set; }
        public string clientUserId { get; set; }
        public string redirectUrl { get; set; }
    }
}
