﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 获取企业用户基本信息
    /// </summary>
    [RemoteService("/corp/get", "POST")]
    public class GetCorpReq : BaseReq<GetCorpRes>
    {
        /// <summary>
        ///  企业在应用中的唯一标识。长度最大64个字符。    
        /// </summary>
        public string clientCorpId { get; set; }
        /// <summary>
        /// 法大大平台为该企业在该应用appId范围内分配的唯一标识。长度最大64个字符。
        /// </summary>
        public string openCorpId { get; set; }
        /// <summary>
        /// 企业统一社会信用代码或各种类型组织的唯一代码，长度最大50个字符。
        /// </summary>
        public string corpIdentNo { get; set; }
    }
}
