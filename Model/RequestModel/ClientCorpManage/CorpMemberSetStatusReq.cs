﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 设置成员状态
    /// </summary>
    [RemoteService("/corp/member/set-status", "POST")]
    public class CorpMemberSetStatusReq : BaseReq<CorpMemberSetStatusRes>
    {
        public string openCorpId { get; set; }
        public long[] memberIds { get; set; }
        public string memberStatus { get; set; }
    }
}
