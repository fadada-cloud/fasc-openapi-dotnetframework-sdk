﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 解绑企业用户
    /// </summary>
    [RemoteService("/corp/unbind", "POST")]
    public class CorpUnbindReq : BaseReq<CorpUnbindRes>
    {
        /// <summary>
        /// 法大大平台为该企业在该应用appId范围内分配的唯一标识
        /// </summary>
        public string openCorpId { get; set; }
    }
}
