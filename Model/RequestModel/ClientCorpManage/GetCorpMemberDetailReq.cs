﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 查询成员详情
    /// </summary>
    [RemoteService("/corp/member/get-detail", "POST")]
    public class GetCorpMemberDetailReq : BaseReq<GetCorpMemberDetailRes>
    {
        public string openCorpId { get; set; }
        public string memberId { get; set; }
        public string internalIdentifier { get; set; }
    }
}
