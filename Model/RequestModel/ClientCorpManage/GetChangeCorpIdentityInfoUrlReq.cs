﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    [RemoteService("/corp/change-identity-info", "POST")]
    public class GetChangeCorpIdentityInfoUrlReq : BaseReq<GetChangeCorpIdentityInfoUrlRes>
    {
        /// <summary>
        /// 法大大平台为该企业在该应用appId范围内分配的唯一标识。长度最大64个字符
        /// </summary>
        public string openCorpId { get; set; }
        /// <summary>
        /// 如需在企业变更工商信息后同时变更clientCorpId ，则在此处传入新的clientCorpId
        /// </summary>
        public string clientCorpId { get; set; }
        /// <summary>
        /// 应用系统中唯一确定登录用户身份的标识
        /// </summary>
        public string clientUserId { get; set; }
        /// <summary>
        /// 重定向地址。长度最大1000个字符
        /// </summary>
        public string redirectUrl { get; set; }
    }
}
