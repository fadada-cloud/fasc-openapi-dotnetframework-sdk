﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientCorpManage
{
    /// <summary>
    /// 查询企业主体列表
    /// </summary>
    [RemoteService("/corp/entity/get-list", "POST")]
    public class GetEntityListReq : BaseReq<GetEntityListRes>
    {
        /// <summary>
        /// 法大大平台为该企业帐号在该应用appId范围内分配的唯一标识。长度最大64个字符
        /// </summary>
        public string openCorpId { get; set; }
    }
}
