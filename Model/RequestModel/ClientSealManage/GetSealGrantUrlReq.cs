﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取印章创建链接
    /// </summary>
    [RemoteService("/seal/grant/get-url", "POST")]
    public class GetSealGrantUrlReq : BaseReq<GetSealGrantUrlRes>
    {
        public string openCorpId { get; set; }
        public string sealId { get; set; }
        public MemberInfo memberInfo { get; set; }
        public string clientUserId { get; set; }
        public string redirectUrl { get; set; }

    }
    public class MemberInfo
    {
        public long[] memberIds { get; set; }
        public string grantStartTime { get; set; }
        public string grantEndTime { get; set; }
    }
}
