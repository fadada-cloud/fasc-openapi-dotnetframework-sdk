﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 查询印章详情
    /// </summary>
    [RemoteService("/seal/get-detail", "POST")]
    public class GetSealDetailReq : BaseReq<GetSealDetailRes>
    {
        public string openCorpId { get; set; }
        public long sealId { get; set; }
    }
}
