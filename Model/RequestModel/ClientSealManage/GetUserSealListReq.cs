﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 查询成员被授权的印章列表
    /// </summary>
    [RemoteService("/seal/user/get-list", "POST")]
    public class GetUserSealListReq : BaseReq<GetUserSealListRes>
    {
        public string openCorpId { get; set; }
        public long memberId { get; set; }
    }
}
