﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 解除签名免验证签
    /// </summary>
    [RemoteService("/personal-seal/free-sign/cancel", "POST")]
    public class CancelPersonalSealFreeSignReq : BaseReq<CancelPersonalSealFreeSignRes>
    {
        public string openUserId { get; set; }
        public long sealId { get; set; }
        public string businessId { get; set; }
    }
}
