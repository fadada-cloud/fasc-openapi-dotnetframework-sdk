﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 设置印章状态
    /// </summary>
    [RemoteService("/seal/set-status", "POST")]
    public class SealSetStatusReq : BaseReq<SealSetStatusRes>
    {
        public string openCorpId { get; set; }
        public long sealId { get; set; }
        public string sealStatus { get; set; }
    }
}
