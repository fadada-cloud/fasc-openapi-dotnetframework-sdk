﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    // <summary>
    /// 创建法定代表人图片印章
    /// </summary>
    [RemoteService("/seal/create-legal-representative-by-template", "POST")]
    public class CreateLegalRepresentativeSealByImageReq : BaseReq<CreateLegalRepresentativeSealByImageRes>
    {
        public string openCorpId { get; set; }
        public string entityId { get; set; }
        public string openUserId { get; set; }
        public string sealImage { get; set; }
        public string sealName { get; set; }
        public string sealTag { get; set; }
        public int? sealWidth { get; set; }
        public int? sealHeight { get; set; }
        public double? sealOldStyle { get; set; }
        public string sealColor { get; set; }
        public string createSerialNo { get; set; }
    }
}
