﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取设置个人签名免验证签链接
    /// </summary>
    [RemoteService("/personal-seal/free-sign/get-url", "POST")]
    public class GetSealPersonalSealFreeSignUrlReq : BaseReq<GetSealPersonalSealFreeSignUrlRes>
    {
        public string openUserId { get; set; }
        public long[] sealIds { get; set; }
        public string businessId { get; set; }
        public string email { get; set; }
        public string redirectUrl { get; set; }
    }
}
