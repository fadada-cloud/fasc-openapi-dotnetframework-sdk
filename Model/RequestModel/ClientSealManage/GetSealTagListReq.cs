﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;
using System.Collections.Generic;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取企业用印员列表
    /// </summary>
    [RemoteService("/seal/tag/get-list", "POST")]
    public class GetSealTagListReq : BaseReq<List<string>>
    {
        public string openCorpId { get; set; }
    }
}
