﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取签名管理链接
    /// </summary>
    [RemoteService("/personal-seal/manage/get-url", "POST")]
    public class GetPersonalSealManageUrlReq : BaseReq<GetPersonalSealManageUrlRes>
    {
        public string clientUserId { get; set; }
        public string redirectUrl { get; set; }
    }
}
