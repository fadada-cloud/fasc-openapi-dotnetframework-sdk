﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取印章管理链接
    /// </summary>
    [RemoteService("/seal/manage/get-url", "POST")]
    public class SealManageUrlReq : BaseReq<SealManageUrlRes>
    {
        public string openCorpId { get; set; }
        public string clientUserId { get; set; }
        public string redirectUrl { get; set; }
    }
}
