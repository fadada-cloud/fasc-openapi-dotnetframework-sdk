﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取企业用印员列表
    /// </summary>
    [RemoteService("/seal/get-user-list", "POST")]
    public class GetSealUserListReq : BaseReq<GetSealUserListRes>
    {
        /// <summary>
        ///企业主体在应用上的openCorpId
        /// </summary>
        public string openCorpId { get; set; }
        /// <summary>
        /// 查询条件
        /// </summary>
        public ListFilter listFilter { get; set; }
    }
}
