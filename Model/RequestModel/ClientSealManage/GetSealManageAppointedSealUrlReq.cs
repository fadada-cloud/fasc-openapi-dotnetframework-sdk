﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取指定印章详情链接
    /// </summary>
    [RemoteService("/seal/manage/get-appointed-seal-url", "POST")]
    public class GetSealManageAppointedSealUrlReq : BaseReq<GetSealManageAppointedSealUrlRes>
    {
        public string openCorpId { get; set; }
        public long sealId { get; set; }
        public string clientUserId { get; set; }
        public string redirectUrl { get; set; }
    }
}
