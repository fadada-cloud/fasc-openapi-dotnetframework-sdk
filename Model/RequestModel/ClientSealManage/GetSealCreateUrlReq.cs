﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取印章创建链接
    /// </summary>
    [RemoteService("/seal/create/get-url", "POST")]
    public class GetSealCreateUrlReq : BaseReq<GetSealCreateUrlRes>
    {
        public string openCorpId { get; set; }
        public string entityId { get; set; }
        public string clientUserId { get; set; }
        public string sealName { get; set; }
        public string categoryType { get; set; }
        public string sealTag { get; set; }
        public string createSerialNo { get; set; }
        public string redirectUrl { get; set; }
    }
}
