﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    // <summary>
    /// 创建图片印章
    /// </summary>
    [RemoteService("/seal/create-by-image", "POST")]
    public class CreateSealByImageReq : BaseReq<CreateSealByImageRes>
    {
        public string openCorpId { get; set; }
        public string entityId { get; set; }
        public string sealName { get; set; }
        public string categoryType { get; set; }
        public string sealImage { get; set; }
        public string sealTag { get; set; }
        public int? sealWidth { get; set; }
        public int? sealHeight { get; set; }
        public double? sealOldStyle { get; set; }
        public string sealColor { get; set; }
        public string createSerialNo { get; set; }
    }
}
