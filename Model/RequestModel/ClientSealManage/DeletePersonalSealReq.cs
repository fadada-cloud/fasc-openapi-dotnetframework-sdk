﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 删除签名
    /// </summary>
    [RemoteService("/personal-seal/delete", "POST")]
    public class DeletePersonalSealReq : BaseReq<DeletePersonalSealRes>
    {
        public string openUserId { get; set; }
        public long sealId { get; set; }
    }
}
