﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取签名创建链接
    /// </summary>
    [RemoteService("/personal-seal/create/get-url", "POST")]
    public class GetPersonalSealCreateUrlReq : BaseReq<GetPersonalSealCreateUrlRes>
    {
        public string clientUserId { get; set; }
        public string createSerialNo { get; set; }
        public string[] createMethod { get; set; }
        public string redirectUrl { get; set; }
    }
}
