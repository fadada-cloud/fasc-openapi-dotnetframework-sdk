﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 解除印章免验证签
    /// </summary>
    [RemoteService("/seal/free-sign/cancel", "POST")]
    public class CancelSealFreeSignReq : BaseReq<CancelSealFreeSignRes>
    {
        public string openCorpId { get; set; }
        public long sealId { get; set; }
        public string businessId { get; set; }
    }
}
