﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 创建图片签名
    /// </summary>
    [RemoteService("/personal-seal/create-by-image", "POST")]
    public class CreatePersonalSealByImageReq : BaseReq<CreatePersonalSealByImageRes>
    {
        public string openUserId { get; set; }
        public string sealImage { get; set; }
        public string sealName { get; set; }
        public int? sealWidth { get; set; }
        public int? sealHeight { get; set; }
        public double? sealOldStyle { get; set; }
        public string sealColor { get; set; }
        public string createSerialNo { get; set; }
    }
}
