﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 创建模板签名
    /// </summary>
    [RemoteService("/personal-seal/create-by-template", "POST")]
    public class CreatePersonalSealByTemplateReq : BaseReq<CreatePersonalSealByTemplateRes>
    {
        public string openUserId { get; set; }
        public string sealName { get; set; }
        public string sealTemplateStyle { get; set; }
        public string sealSize { get; set; }
        public string sealSuffix { get; set; }
        public string sealColor { get; set; }
        public string createSerialNo { get; set; }
    }
}
