﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    // <summary>
    /// 创建法定代表人模板印章
    /// </summary>
    [RemoteService("/seal/create-legal-representative-by-template", "POST")]
    public class CreateLegalRepresentativeSealByTemplateReq : BaseReq<CreateLegalRepresentativeSealByTemplateRes>
    {
        public string openCorpId { get; set; }
        public string entityId { get; set; }
        public string openUserId { get; set; }
        public string sealName { get; set; }
        public string sealTag { get; set; }
        public string sealTemplateStyle { get; set; }
        public string sealSize { get; set; }
        public string sealSuffix { get; set; }
        public string securityCode { get; set; }
        public string sealColor { get; set; }
        public string createSerialNo { get; set; }
    }
}
