﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 查询审核中的印章列表
    /// </summary>
    [RemoteService("/seal/verify/get-list", "POST")]
    public class GetSealVerifyListReq : BaseReq<GetSealVerifyListRes>
    {
        public string openCorpId { get; set; }
    }
}
