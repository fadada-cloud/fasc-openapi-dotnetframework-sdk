﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 获取印章列表
    /// </summary>
    [RemoteService("/seal/get-list", "POST")]
    public class GetSealListReq : BaseReq<GetSealListRes>
    {
        /// <summary>
        ///企业主体在应用上的openCorpId
        /// </summary>
        public string openCorpId { get; set; }
        /// <summary>
        /// 查询条件
        /// </summary>
        public ListFilter listFilter { get; set; }
    }

    /// <summary>
    /// 查询条件
    /// </summary>
    public class ListFilter
    {
        /// <summary>
        /// 印章类型
        /// </summary>
        public string[] categoryType { get; set; }
        public string[] sealTags { get; set; }
    }
}
