﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 创建模板印章
    /// </summary>
    [RemoteService("/seal/create-by-template", "POST")]
    public class CreateSealByTemplateReq : BaseReq<CreateSealByTemplateRes>
    {
        public string openCorpId { get; set; }
        public string entityId { get; set; }
        public string sealName { get; set; }
        public string categoryType { get; set; }
        public string sealTag { get; set; }
        public string sealTemplateStyle { get; set; }
        public string sealSize { get; set; }
        public string sealHorizontalText { get; set; }
        public string sealBottomText { get; set; }
        public string sealColor { get; set; }
        public string createSerialNo { get; set; }
    }
}
