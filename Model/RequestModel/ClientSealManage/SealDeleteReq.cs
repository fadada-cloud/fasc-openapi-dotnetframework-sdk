﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 删除印章
    /// </summary>
    [RemoteService("/seal/delete", "POST")]
    public class SealDeleteReq : BaseReq<SealDeleteRes>
    {
        public string openCorpId { get; set; }
        public string sealId { get; set; }
    }
}
