﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 查询个人签名列表
    /// </summary>
    [RemoteService("/personal-seal/get-list", "POST")]
    public class GetSealPersonalSealListReq : BaseReq<GetSealPersonalSealListRes>
    {
        public string openUserId { get; set; }
    }
}
