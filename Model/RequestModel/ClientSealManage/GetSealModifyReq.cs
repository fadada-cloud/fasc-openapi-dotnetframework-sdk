﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 修改印章基本信息
    /// </summary>
    [RemoteService("/seal/modify", "POST")]
    public class GetSealModifyReq : BaseReq<GetSealModifyRes>
    {
        public string openCorpId { get; set; }
        public string sealId { get; set; }
        public string sealName { get; set; }
        public string categoryType { get; set; }
        public string sealTag { get; set; }
    }
}
