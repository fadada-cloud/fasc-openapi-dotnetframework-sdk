﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSealManage
{
    /// <summary>
    /// 解除印章授权
    /// </summary>
    [RemoteService("/seal/grant/cancel", "POST")]
    public class SealGrantCancelReq : BaseReq<SealGrantCancelRes>
    {
        public string openCorpId { get; set; }
        public long sealId { get; set; }
        public long memberId { get; set; }
    }
}
