﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDraftManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDraftManage
{
    // <summary>
    /// 查询已定稿的合同文件
    /// </summary>
    [RemoteService("/draft/get-finished-file", "POST")]
    public class GetFinishedFileReq : BaseReq<GetFinishedFileRes>
    {
        public string contractConsultId { get; set; }
    }
}
