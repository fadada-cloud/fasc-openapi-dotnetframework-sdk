﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDraftManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDraftManage
{
    // <summary>
    /// 获取合同起草管理链接
    /// </summary>
    [RemoteService("/draft/get-manage-url", "POST")]
    public class GetManageUrlReq : BaseReq<GetManageUrlRes>
    {
        public string openCorpId { get; set; }
        public string clientUserId { get; set; }
        public string redirectUrl { get; set; }
    }
}
