﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDraftManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDraftManage
{
    // <summary>
    /// 获取合同协商邀请链接
    /// </summary>
    [RemoteService("/draft/get-invite-url", "POST")]
    public class GetInviteUrlReq : BaseReq<GetInviteUrlRes>
    {
        public string contractConsultId { get; set; }
        public string expireTime { get; set; }
        public string innerPermission { get; set; }
        public string outerPermission { get; set; }
        public string touristView { get; set; }
        public string redirectUrl { get; set; }
    }
}
