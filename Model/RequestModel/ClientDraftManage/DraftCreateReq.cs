﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDraftManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDraftManage
{
    // <summary>
    /// 发起合同协商
    /// </summary>
    [RemoteService("/draft/create", "POST")]
    public class DraftCreateReq : BaseReq<DraftCreateRes>
    {
        public string openCorpId { get; set; }
        public string initiatorMemberId { get; set; }
        public string contractSubject { get; set; }
        public string fileId { get; set; }
    }
}
