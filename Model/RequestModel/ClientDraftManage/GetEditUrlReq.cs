﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDraftManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDraftManage
{
    // <summary>
    /// 获取合同协商编辑链接
    /// </summary>
    [RemoteService("/draft/get-edit-url", "POST")]
    public class GetEditUrlReq : BaseReq<GetEditUrlRes>
    {
        public string openCorpId { get; set; }
        public string clientUserId { get; set; }
        public string contractConsultId { get; set; }
        public string urlType { get; set; }
        public string redirectUrl { get; set; }
    }
}
