﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDraftManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDraftManage
{
    // <summary>
    /// 合同协商文件定稿
    /// </summary>
    [RemoteService("/draft/doc-finalize", "POST")]
    public class DocFinalizeReq : BaseReq<VoidRes>
    {
        public string contractConsultId { get; set; }
    }
}
