﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientBillManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientBillManage
{
    // <summary>
    /// 查询账户可用余量
    /// </summary>
    [RemoteService("/bill-account/get-usage-availablenum", "POST")]
    public class GetUsageAvailablenumReq : BaseReq<GetUsageAvailablenumRes>
    {
        public string openCorpId { get; set; }
        public string usageCode { get; set; }
    }
}
