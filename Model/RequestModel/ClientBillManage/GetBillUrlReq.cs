﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientBillManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientBillManage
{
    /// <summary>
    /// 获取计费链接
    /// </summary>
    [RemoteService("/billing/get-bill-url", "POST")]
    public class GetBillUrlReq : BaseReq<GetBillUrlRes>
    {
        /// <summary>
        /// 需要获取计费链接的应用系统上的个人或企业主体信息
        /// 若为个人用户，个人用户必须已实名。
        /// 若为企业用户，企业用户必须已实名，且企业经办人
        /// openUserId不能为空，必须为企业成员
        /// </summary>
        public OpenId openId { get; set; }
        /// <summary>order: 套餐订购默认为account。
        /// </summary>
        public string urlType { get; set; }
        /// <summary>
        /// 返回地址
        /// </summary>
        public string redirectUrl { get; set; }
        /// <summary>
        /// 应用系统中唯一确定登录用户身份的标识
        /// </summary>
        public string clientUserId { get; set; }
    }
}
