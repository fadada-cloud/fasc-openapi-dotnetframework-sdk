﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 添加签署任务控件
    /// </summary>
    [RemoteService("/sign-task/field/add", "POST")]
    public class AddSignTaskFieldReq : BaseReq<AddSignTaskFieldRes>
    {
        public string signTaskId { get; set; }
        public string actorId { get; set; }

        public AddFieldInfo[] fields;
    }

    public class AddFieldInfo
    {
        public int? docId;
        public Field[] docFields;
    }
}
