﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务公证处保全报告下载地址
    /// </summary>
    [RemoteService("/sign-task/download-evidence-report", "POST")]
    public class GetSignTaskDownloadEvidenceReportReq : BaseReq<GetSignTaskDownloadEvidenceReportRes>
    {
        public string signTaskId { get; set; }
    }
}
