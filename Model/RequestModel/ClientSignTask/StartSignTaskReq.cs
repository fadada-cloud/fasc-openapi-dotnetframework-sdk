﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 提交签署任务
    /// </summary>
    [RemoteService("/sign-task/start", "POST")]
    public class StartSignTaskReq : BaseReq<StartSignTaskRes>
    {
        public string signTaskId { get; set; }
    }
}
