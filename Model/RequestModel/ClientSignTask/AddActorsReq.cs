﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;
using SignTaskActor = fasc_openapi_donet_sdk.Model.CommonModel.SignTaskActor;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 添加签署任务参与方
    /// </summary>
    [RemoteService("/sign-task/actor/add", "POST")]
    public class AddActorsReq : BaseReq<AddActorsRes>
    {
        public string signTaskId { get; set; }
        public SignTaskActor[] actors { get; set; }
    }
}
