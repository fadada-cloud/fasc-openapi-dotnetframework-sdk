﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 催办签署任务
    /// </summary>
    [RemoteService("/sign-task/urge", "POST")]
    public class SignTaskUrgeReq : BaseReq<SignTaskUrgeRes>
    {
        public string signTaskId { get; set; }
    }
}
