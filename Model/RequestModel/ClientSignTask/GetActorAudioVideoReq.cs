﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取参与方签署音视频
    /// </summary>
    [RemoteService("/sign-task/actor/get-audio-video-download-url", "POST")]
    public class GetActorAudioVideoReq : BaseReq<GetActorAudioVideoRes>
    {
        public string signTaskId { get; set; }
        public string actorId { get; set; }
    }
}
