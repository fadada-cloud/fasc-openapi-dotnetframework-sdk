﻿using System.Collections.Generic;
using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取指定归属方的签署任务文档下载地址
    /// </summary>
    [RemoteService("/sign-task/owner/get-download-url", "POST")]
    public class GetOwnerSignTaskUrlReq : BaseReq<GetOwnerSignTaskUrlRes>
    {
        public OpenId ownerId { get; set; }
        public string signTaskId { get; set; }
        public string fileType { get; set; }
        public string id { get; set; }
        
        public string customName { get; set; }
        
        public bool? compression { get; set; }

        public bool? folderBySigntask { get; set; }
        
        public List<BatchDownloadInfo> batchDownloadInfo { get; set; }
    }
}
