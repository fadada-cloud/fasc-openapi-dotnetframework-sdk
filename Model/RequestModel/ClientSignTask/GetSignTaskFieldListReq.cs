﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 查询签署任务控件信息
    /// </summary>
    [RemoteService("/sign-task/field/list", "POST")]
    public class GetSignTaskFieldListReq : BaseReq<GetSignTaskFieldListRes>
    {
        public string signTaskId { get; set; }
    }
}
