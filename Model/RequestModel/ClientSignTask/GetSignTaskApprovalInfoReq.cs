﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 查询签署任务审批信息
    /// </summary>
    [RemoteService("/sign-task/get-approval-info", "POST")]
    public class GetSignTaskApprovalInfoReq : BaseReq<GetSignTaskApprovalInfoRes>
    {
        public string signTaskId { get; set; }
    }
}
