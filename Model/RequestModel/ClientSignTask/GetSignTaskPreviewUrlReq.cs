﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务预览链接
    /// </summary>
    [RemoteService("/sign-task/get-preview-url", "POST")]
    public class GetSignTaskPreviewUrlReq : BaseReq<GetSignTaskPreviewUrlRes>
    {
        public string signTaskId { get; set; }
        public string redirectUrl { get; set; }
    }
}
