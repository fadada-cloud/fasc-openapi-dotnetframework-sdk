﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;
namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取参与方签署链接（API3.0任务专属）
    /// </summary>
    [RemoteService("/sign-task/actor/v3/get-url", "POST")]
    public class GetV3ActorSignTaskUrlReq : BaseReq<GetV3ActorSignTaskUrlRes>
    {
        public string signTaskId { get; set; }
        public OpenId ownerId { get; set; }
        public string redirectUrl { get; set; }
    }
}
