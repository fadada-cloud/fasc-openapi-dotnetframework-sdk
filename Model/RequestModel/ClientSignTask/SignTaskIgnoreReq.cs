﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 驳回填写签署任务
    /// </summary>
    [RemoteService("/sign-task/ignore", "POST")]
    public class SignTaskIgnoreReq : BaseReq<VoidRes>
    {
        public Actor[] actors { get; set; }

        public class Actor
        {
            public string actorId { get; set; }
            public string reason { get; set; }
        }
    }
}
