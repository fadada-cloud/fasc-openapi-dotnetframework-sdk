﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 填写签署任务控件内容
    /// </summary>
    [RemoteService("/sign-task/field/fill-values", "POST")]
    public class FillFieldValuesReq : BaseReq<FillFieldValuesRes>
    {
        public string signTaskId { get; set; }
        public DocFieldValueInfo[] docFieldValues { get; set; }
    }

    public class DocFieldValueInfo
    {
        public string docId { get; set; }
        public string fieldId { get; set; }
        public string fieldKey { get; set; }
        public string fieldValue { get; set; }
        public string fieldName { get; set; }
    }
}
