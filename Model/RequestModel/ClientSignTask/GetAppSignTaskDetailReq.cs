﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务详情
    /// </summary>
    [RemoteService("/sign-task/app/get-detail", "POST")]
    public class GetAppSignTaskDetailReq : BaseReq<GetAppSignTaskDetailRes>
    {
        public string signTaskId { get; set; }
    }
}
