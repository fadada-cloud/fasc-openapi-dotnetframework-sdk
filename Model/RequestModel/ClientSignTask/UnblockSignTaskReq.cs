﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 解阻塞签署任务
    /// </summary>
    [RemoteService("/sign-task/unblock", "POST")]
    public class UnblockSignTaskReq : BaseReq<UnblockSignTaskRes>
    {
        public string signTaskId { get; set; }
        public string actorId { get; set; }
    }
}
