﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 删除签署任务
    /// </summary>
    [RemoteService("/sign-task/delete", "POST")]
    public class DeleteSignTaskReq : BaseReq<VoidRes>
    {
        public string signTaskId { get; set; }
    }
}
