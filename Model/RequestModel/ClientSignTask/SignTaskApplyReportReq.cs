﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 申请证据报告
    /// </summary>
    [RemoteService("/sign-task/apply-report", "POST")]
    public class SignTaskApplyReportReq : BaseReq<SignTaskApplyReportRes>
    {
        public string signTaskId { get; set; }
        public string fileId { get; set; }
        public OpenId ownerId { get; set; }
        public string reportType { get; set; }
    }
}
