﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 下载证据报告
    /// </summary>
    [RemoteService("/sign-task/download-report", "POST")]
    public class SignTaskDownloadReportReq : BaseReq<SignTaskDownloadReportRes>
    {
        public string reportDownloadId { get; set; }
    }
}
