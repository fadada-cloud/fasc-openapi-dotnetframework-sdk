﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 移除签署任务参与方
    /// </summary>
    [RemoteService("/sign-task/actor/delete", "POST")]
    public class DeleteActorReq : BaseReq<DeleteActorRes>
    {
        public string signTaskId { get; set; }
        public string[] actorIds { get; set; }
    }
}
