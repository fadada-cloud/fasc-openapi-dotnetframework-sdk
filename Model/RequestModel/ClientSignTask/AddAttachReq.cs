﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;
using Attach = fasc_openapi_donet_sdk.Model.CommonModel.Attach;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 添加签署任务附件
    /// </summary>
    [RemoteService("/sign-task/attach/add", "POST")]
    public class AddAttachReq : BaseReq<AddAttachRes>
    {
        public string signTaskId { get; set; }
        public Attach[] attachs { get; set; }
    }
}
