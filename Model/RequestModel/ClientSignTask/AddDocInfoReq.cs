﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;
using Doc = fasc_openapi_donet_sdk.Model.CommonModel.Doc;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 添加签署任务文档
    /// </summary>
    [RemoteService("/sign-task/doc/add", "POST")]
    public class AddDocInfoReq : BaseReq<AddDocInfoRes>
    {
        public string signTaskId { get; set; }
        public Doc[] docs { get; set; }
    }
}
