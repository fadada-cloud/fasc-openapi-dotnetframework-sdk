﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;
using Attach = fasc_openapi_donet_sdk.Model.CommonModel.Attach;
using Doc = fasc_openapi_donet_sdk.Model.CommonModel.Doc;
using SignTaskActor = fasc_openapi_donet_sdk.Model.CommonModel.SignTaskActor;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 创建签署任务(基于即时文档或文档模板)
    /// </summary>
    [RemoteService("/sign-task/create", "POST")]
    public class CreateSignTaskReq : BaseReq<CreateSignTaskRes>
    {
        public bool? signInOrder { get; set; }
        public Doc[] docs { get; set; }
        public Attach[] attachs { get; set; }
        public SignTaskActor[] actors { get; set; }
        public SignConfigInfo signConfigInfo { get; set; }
        public Watermark[] watermarks { get; set; }
        public bool? isAllowInsertFile { get; set; }
        public OpenId initiator { get; set; }
        public string initiatorMemberId { get; set; }
        public string initiatorEntityId { get; set; }
        public string signTaskSubject { get; set; }
        public string signDocType { get; set; }
        public string businessCode { get; set; }
        public long? businessTypeId { get; set; }
        public string startApprovalFlowId { get; set; }
        public string finalizeApprovalFlowId { get; set; }
        public string expiresTime { get; set; }
        public string dueDate { get; set; }
        public string catalogId { get; set; }
        public bool? autoStart { get; set; }
        public bool? autoFinish { get; set; }
        public bool? autoFillFinalize { get; set; }
        public string certCAOrg { get; set; }
        public string businessId { get; set; }
        public string transReferenceId { get; set; }
        public string fileFormat { get; set; }
    }
}

