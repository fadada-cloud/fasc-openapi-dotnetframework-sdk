﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 阻塞签署任务
    /// </summary>
    [RemoteService("/sign-task/block", "POST")]
    public class BlockSignTaskReq : BaseReq<BlockSignTaskRes>
    {
        public string signTaskId { get; set; }
        public string actorId { get; set; }
    }
}
