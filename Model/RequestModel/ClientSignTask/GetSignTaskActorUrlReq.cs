﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务参与方专属链接
    /// </summary>
    [RemoteService("/sign-task/actor/get-url", "POST")]
    public class GetSignTaskActorUrlReq : BaseReq<GetSignTaskActorUrlRes>
    {
        public string signTaskId { get; set; }
        public string actorId { get; set; }
        public string clientUserId { get; set; }
        public string redirectUrl { get; set; }
        public string redirectMiniAppUrl { get; set; }
    }
}
