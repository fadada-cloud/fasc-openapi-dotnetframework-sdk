﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;
using FillField = fasc_openapi_donet_sdk.Model.CommonModel.FillField;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 修改签署任务参与方
    /// </summary>
    [RemoteService("/sign-task/actor/modify", "POST")]
    public class ModifyActorReq : BaseReq<VoidRes>
    {
        public string signTaskId { get; set; }
        public string businessId { get; set; }
        public ModifySignTaskActorInfo[] actors { get; set; }
    }

    public class ModifySignTaskActorInfo
    {
        public string actorId { get; set; }
        public FillField[] fillFields { get; set; }
        public SignField[] signFields { get; set; }
        public SignConfigInfo signConfigInfo { get; set; }
    }
}
