﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;
using System.Collections.Generic;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 查询签署任务参与方信息
    /// </summary>
    [RemoteService("/sign-task/actor/list", "POST")]
    public class GetSignTaskActorListReq : BaseReq<List<GetSignTaskActorListRes>>
    {
        public string signTaskId { get; set; }
    }
}
