﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 查询签署业务类型列表
    /// </summary>
    [RemoteService("/sign-task/business-type/get-list", "POST")]
    public class GetSignTaskBusinessTypeListReq : BaseReq<GetSignTaskBusinessTypeListRes>
    {
        public string openCorpId { get; set; }
    }
}
