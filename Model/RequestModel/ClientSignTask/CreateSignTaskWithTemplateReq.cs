﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;
using SignTaskActor = fasc_openapi_donet_sdk.Model.CommonModel.SignTaskActor;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 创建签署任务(基于签署模板)
    /// </summary>
    [RemoteService("/sign-task/create-with-template", "POST")]
    public class CreateSignTaskWithTemplateReq : BaseReq<CreateSignTaskWithTemplateRes>
    {
        public string signTaskSubject { get; set; }
        public string signDocType { get; set; }
        public OpenId initiator { get; set; }
        public string expiresTime { get; set; }
        public bool autoStart { get; set; }
        public bool autoFillFinalize { get; set; }
        public BusinessSceneInfo businessScene { get; set; }
        public SignTaskActor[] actors { get; set; }
        public string signTemplateId { get; set; }
        public string initiatorEntityId { get; set; }
        public string initiatorMemberId { get; set; }
        public long? businessTypeId { get; set; }
        public string businessCode { get; set; }
        public string startApprovalFlowId { get; set; }
        public string finalizeApprovalFlowId { get; set; }
        public string dueDate { get; set; }
        public string catalogId { get; set; }
        public bool? autoFinish { get; set; }
        public string certCAOrg { get; set; }
        public string businessId { get; set; }
        public string transReferenceId { get; set; }
        public Watermark[] watermarks { get; set; }
    }
}
