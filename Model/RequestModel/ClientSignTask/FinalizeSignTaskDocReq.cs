﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 定稿签署任务文档
    /// </summary>
    [RemoteService("/sign-task/doc-finalize", "POST")]
    public class FinalizeSignTaskDocReq : BaseReq<FinalizeSignTaskDocRes>
    {
        public string signTaskId { get; set; }
    }
}
