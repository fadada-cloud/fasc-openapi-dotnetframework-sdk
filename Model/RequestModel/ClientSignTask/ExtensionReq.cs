﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 延期签署任务
    /// </summary>
    [RemoteService("/sign-task/extension", "POST")]
    public class ExtensionReq : BaseReq<VoidRes>
    {
        public string signTaskId { get; set; }
        public string extensionTime { get; set; }
    }
}
