﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务批量签署链接
    /// </summary>
    [RemoteService("/sign-task/get-batch-sign-url", "POST")]
    public class GetSignTaskBatchSignUrlReq : BaseReq<GetSignTaskBatchSignUrlRes>
    {
        public OpenId ownerId { get; set; }
        public int memberId { get; set; }
        public string clientUserId { get; set; }
        public string[] signTaskIds { get; set; }
        public string redirectUrl { get; set; }
        public string idType { get; set; }
        public string openId { get; set; }
        public string accountName { get; set; }
        public string redirectMiniAppUrl { get; set; }
    }
}
