﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取签署参与方刷脸底图
    /// </summary>
    [RemoteService("/sign-task/actor/get-face-picture", "POST")]
    public class GetSignTaskFacePictureReq : BaseReq<GetSignTaskFacePictureRes>
    {
        public string signTaskId { get; set; }
        public string actorId { get; set; }
    }
}
