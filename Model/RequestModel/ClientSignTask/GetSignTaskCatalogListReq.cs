﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 查询企业签署任务文件夹
    /// </summary>
    [RemoteService("/sign-task-catalog/list", "POST")]
    public class GetSignTaskCatalogListReq : BaseReq<GetSignTaskCatalogListRes>
    {
        public OpenId ownerId { get; set; }
    }
}
