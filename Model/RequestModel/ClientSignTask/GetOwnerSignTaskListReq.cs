﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取指定归属方的签署任务列表
    /// </summary>
    [RemoteService("/sign-task/owner/get-list", "POST")]
    public class GetOwnerSignTaskListReq : BaseReq<GetOwnerSignTaskListRes>
    {
        public OpenId ownerId { get; set; }
        public string ownerRole { get; set; }
        public SignListFilter listFilter { get; set; }
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
        
        public string memberId { get; set; }
    }

    public class SignListFilter
    {
        public string signTaskSubject { get; set; }
        public string[] signTaskStatus { get; set; }

        public string startTimeFrom { get; set; }

        public string startTimeTo { get; set; }

        public string finishTimeFrom { get; set; }

        public string finishTimeTo { get; set; }

        public string expiresTimeFrom { get; set; }

        public string expiresTimeTo { get; set; }
    }
}