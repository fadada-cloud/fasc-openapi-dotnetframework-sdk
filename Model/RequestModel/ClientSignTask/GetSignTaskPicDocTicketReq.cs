﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取图片版签署文档下载地址
    /// </summary>
    [RemoteService("/sign-task/owner/get-pic-download-url", "POST")]
    public class GetSignTaskPicDocTicketReq : BaseReq<GetSignTaskDocRes>
    {
        public string slicingTicketId { get; set; }
    }
}
