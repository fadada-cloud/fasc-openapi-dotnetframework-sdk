﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 移除签署任务附件
    /// </summary>
    [RemoteService("/sign-task/attach/delete", "POST")]
    public class DeleteAttachReq : BaseReq<DeleteAttachRes>
    {
        public string signTaskId { get; set; }
        public string[] attachIds { get; set; }
    }
}
