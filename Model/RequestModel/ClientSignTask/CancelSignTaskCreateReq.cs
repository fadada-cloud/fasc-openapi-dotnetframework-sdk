﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 作废签署任务
    /// </summary>
    [RemoteService("/sign-task/abolish", "POST")]
    public class CancelSignTaskCreateReq : BaseReq<CancelSignTaskCreateRes>
    {
        public string signTaskId { get; set; }
        public AbolishedInitiator abolishedInitiator { get; set; }
        public string initiatorMemberId { get; set; }
        public string docSource { get; set; }
        public string reason { get; set; }
        public CancelDoc docs { get; set; }
        public SignTaskActor[] actors { get; set; }
        public bool? autoStart { get; set; }
        public string signTaskSubject { get; set; }
        public string expiresTime { get; set; }
        public long? businessTypeId { get; set; }
        public string businessCode { get; set; }
        public string abolishApprovalFlowId { get; set; }
        public string catalogId { get; set; }
        public string businessId { get; set; }
        public string transReferenceId { get; set; }
        public string certCAOrg { get; set; }
        public string fileFormat { get; set; }
    }

    public class AbolishedInitiator
    {
        public string initiatorId { get; set; }
        public string actorId { get; set; }
    }

    public class CancelDoc
    {
        public string docId { get; set; }
        public string docName { get; set; }
        public string docFileId { get; set; }
        public Field[] docFields { get; set; }
    }
}
