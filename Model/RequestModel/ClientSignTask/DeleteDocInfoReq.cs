﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 移除签署任务文档
    /// </summary>
    [RemoteService("/sign-task/doc/delete", "POST")]
    public class DeleteDocInfoReq : BaseReq<DeleteDocInfoRes>
    {
        public string signTaskId { get; set; }
        public string[] docIds { get; set; }
    }
}
