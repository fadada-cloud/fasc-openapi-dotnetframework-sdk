﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 撤销签署任务
    /// </summary>
    [RemoteService("/sign-task/cancel", "POST")]
    public class CancelSignTaskReq : BaseReq<CancelSignTaskRes>
    {
        public string signTaskId { get; set; }
        public string terminationNote { get; set; }
    }
}
