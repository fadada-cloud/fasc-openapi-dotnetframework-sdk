﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    // <summary>
    /// 查询参与方证书文件
    /// </summary>
    [RemoteService("/sign-task/actor/get-cer-info", "POST")]
    public class SignTaskGetCerInfoReq : BaseReq<SignTaskGetCerInfoRes>
    {
        public string signTaskId { get; set; }
        public string actorId { get; set; }
    }
}
