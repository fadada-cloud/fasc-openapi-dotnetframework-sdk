﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 移除签署任务控件
    /// </summary>
    [RemoteService("/sign-task/field/delete", "POST")]
    public class DeleteSignTaskFieldReq : BaseReq<DeleteSignTaskFieldRes>
    {
        public string signTaskId { get; set; }
        public Fields[] fields { get; set; }
    }

    public class Fields
    {
        public int? docId { get; set; }
        public string[] fieldIds { get; set; }
    }
}
