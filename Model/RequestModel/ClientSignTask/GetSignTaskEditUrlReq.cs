﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务编辑链接
    /// </summary>
    [RemoteService("/sign-task/get-edit-url", "POST")]
    public class GetSignTaskEditUrlReq : BaseReq<GetSignTaskEditUrlRes>
    {
        public string signTaskId { get; set; }
        public string redirectUrl { get; set; }
        public OpenId initiator { get; set; }
        public string[] nonEditableInfo { get; set; }
    }
}
