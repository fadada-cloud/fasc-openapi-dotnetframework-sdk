﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    // <summary>
    /// 查询签署完成的文件
    /// </summary>
    [RemoteService("/sign-task/owner/get-file", "POST")]
    public class SignTaskGetFileReq : BaseReq<SignTaskGetFileRes>
    {
        public string signTaskId { get; set; }
        public string docId { get; set; }
    }
}
