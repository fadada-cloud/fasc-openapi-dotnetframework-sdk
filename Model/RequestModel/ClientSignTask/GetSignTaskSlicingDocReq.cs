﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask
{
    /// <summary>
    /// 获取签署文档切图
    /// </summary>
    [RemoteService("/sign-task/owner/get-slicing-ticket-id", "POST")]
    public class GetSignTaskSlicingDocReq : BaseReq<GetSignTaskPicDocTicketRes>
    {
        public OpenId ownerId { get; set; }
        public string signTaskId { get; set; }
    }
}
