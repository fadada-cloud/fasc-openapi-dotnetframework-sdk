﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 文档验签
    /// </summary>
    [RemoteService("/file/verify-sign", "POST")]
    public class FileVerifySignReq : BaseReq<FileVerifySignRes>
    {
        public string fileId { get; set; }
        public string fileHash { get; set; }
    }
}
