﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取历史合同智审页面链接
    /// </summary>
    [RemoteService("/ocr/edit/examineResultUrl", "POST")]
    public class GetOcrEditExamineResultUrlReq : BaseReq<GetOcrEditExamineResultUrlRes>
    {
        public OpenId initiator { get; set; }
        public string examineId { get; set; }
    }
}
