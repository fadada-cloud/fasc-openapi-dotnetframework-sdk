﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取文件模板列表
    /// </summary>
    [RemoteService("/doc-template/get-list", "POST")]
    public class GetDocTemplateListReq : BaseReq<GetDocTemplateListRes>
    {
        public OpenId ownerId { get; set; }
        public DocTemplateListFilterInfo listFilter { get; set; }
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
    }
    public class DocTemplateListFilterInfo
    {
        public string docTemplateName { get; set; }
    }

}
