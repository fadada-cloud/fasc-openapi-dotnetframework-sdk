﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 启用/停用签署任务模板
    /// </summary>
    [RemoteService("/sign-template/set-status", "POST")]
    public class SetSignTemplateStatusReq : BaseReq<SetSignTemplateStatusRes>
    {
        public string openCorpId { get; set; }
        public string signTemplateId { get; set; }
        public string signTemplateStatus { get; set; }
    }
}
