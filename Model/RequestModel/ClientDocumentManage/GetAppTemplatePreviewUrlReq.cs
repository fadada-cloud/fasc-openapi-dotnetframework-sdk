﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用模板预览链接
    /// </summary>
    [RemoteService("/app-template/preview/get-url", "POST")]
    public class GetAppTemplatePreviewUrlReq : BaseReq<GetAppTemplatePreviewUrlRes>
    {
        public string appTemplateId { get; set; }
        public string redirectUrl { get; set; }
    }
}
