﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 文件处理
    /// </summary>
    [RemoteService("/file/process", "POST")]
    public class FileProcessReq : BaseReq<FileProcessRes>
    {
        public FddUploadFiles[] fddFileUrlList { get; set; }
    }

    public class FddUploadFiles
    {
        public string fileType { get; set; }
        public string fddFileUrl { get; set; }
        public string fileName { get; set; }
        public string fileFormat { get; set; }
    }
}
