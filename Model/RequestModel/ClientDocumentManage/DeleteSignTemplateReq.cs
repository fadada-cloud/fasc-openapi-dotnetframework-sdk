﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 签署模板删除
    /// </summary>
    [RemoteService("/sign-template/delete", "POST")]
    public class DeleteSignTemplateReq : BaseReq<VoidRes>
    {
        public string openCorpId { get; set; }
        public string signTemplateId { get; set; }
    }
}
