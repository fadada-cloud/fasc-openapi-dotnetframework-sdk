﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 删除应用签署任务模板
    /// </summary>
    [RemoteService("/app-sign-template/delete", "POST")]
    public class DeleteSignAppTemplateReq : BaseReq<VoidRes>
    {
        public string appSignTemplateId { get; set; }
    }
}
