﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 创建文档模板
    /// </summary>
    [RemoteService("/doc-template/create", "POST")]
    public class CreateDocTemplateReq : BaseReq<CreateDocTemplateRes>
    {
        public string openCorpId { get; set; }
        public string docTemplateName { get; set; }
        public string createSerialNo { get; set; }
        public string fileId { get; set; }
    }
}
