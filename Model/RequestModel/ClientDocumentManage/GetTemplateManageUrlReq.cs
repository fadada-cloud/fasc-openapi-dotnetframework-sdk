﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取模板管理链接
    /// </summary>
    [RemoteService("/template/manage/get-url", "POST")]
    public class GetTemplateManageUrlReq : BaseReq<GetTemplateManageUrlRes>
    {
        public string openCorpId { get; set; }
        public string redirectUrl { get; set; }
    }
}
