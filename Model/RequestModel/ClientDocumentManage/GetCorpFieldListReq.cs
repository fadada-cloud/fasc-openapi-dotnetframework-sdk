﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;
using System.Collections.Generic;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 查询自定义控件列表
    /// </summary>
    [RemoteService("/corp-field/get-list", "POST")]
    public class GetCorpFieldListReq : BaseReq<List<GetCorpFieldListRes>>
    {
        public string openCorpId { get; set; }
    }
}
