﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 通过网络文件地址上传
    /// </summary>
    [RemoteService("/file/upload-by-url", "POST")]
    public class UploadFileByUrlReq : BaseReq<UploadFileByUrlRes>
    {
        /// <summary>
        /// 文件的用途类型：
        /// </summary>
        public string fileType { get; set; }
        /// <summary>
        /// 网络文件URL，法大大平台将从该地址拉取文档，
        /// </summary>
        public string fileUrl { get; set; }
        /// <summary>
        /// 文件上传的目的存储类型
        /// </summary>
        public string storageType { get; set; }
    }
}
