﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Common;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 创建自定义控件
    /// </summary>
    [RemoteService("/corp-field/create", "POST")]
    public class CreateCorpFieldReq : BaseReq<CreateCorpFieldRes>
    {
        public string openCorpId { get; set; }
        public CreateCorpFieldInfo[] fields { get; set; }
    }
}
