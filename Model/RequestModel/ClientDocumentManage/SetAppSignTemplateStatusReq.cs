﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 启用/停用应用签署任务模板
    /// </summary>
    [RemoteService("/app-sign-template/set-status", "POST")]
    public class SetAppSignTemplateStatusReq : BaseReq<VoidRes>
    {
        public string appSignTemplateId { get; set; }
        public string appSignTemplateStatus { get; set; }
    }
}
