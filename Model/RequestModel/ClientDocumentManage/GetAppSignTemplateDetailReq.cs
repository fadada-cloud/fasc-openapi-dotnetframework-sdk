﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用签署任务模板详情
    /// </summary>
    [RemoteService("/app-sign-template/get-detail", "POST")]
    public class GetAppSignTemplateDetailReq : BaseReq<GetAppSignTemplateDetailRes>
    {
        public string appSignTemplateId { get; set; }
    }
}
