﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取文件对比页面链接
    /// </summary>
    [RemoteService("/ocr/edit/get-compareUrl", "POST")]
    public class GetOcrEditCompareUrlReq : BaseReq<GetOcrEditCompareUrlRes>
    {
        public OpenId initiator { get; set; }
        public string originFileId { get; set; }
        public string targetFileId { get; set; }
    }
}
