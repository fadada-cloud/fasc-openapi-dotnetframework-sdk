﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用模板新增链接
    /// </summary>
    [RemoteService("/app-template/create/get-url", "POST")]
    public class GetAppTemplateCreateUrlReq : BaseReq<GetAppTemplateCreateUrlRes>
    {
        public string type { get; set; }
        public string createSerialNo { get; set; }
        public string redirectUrl { get; set; }
    }
}
