﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 查询应用文档模板列表
    /// </summary>
    [RemoteService("/app-doc-template/get-list", "POST")]
    public class GetAppDocTemplateListReq : BaseReq<GetAppDocTemplateListRes>
    {
        public listFilter listFilter { get; set; }
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
    }
    public class listFilter
    {
        public string appDocTemplateName { get; set; }
    }
}
