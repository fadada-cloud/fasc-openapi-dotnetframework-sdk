﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用模板编辑链接
    /// </summary>
    [RemoteService("/app-template/edit/get-url", "POST")]
    public class GetAppTemplateEditUrlReq : BaseReq<GetAppTemplateEditUrlRes>
    {
        public string appTemplateId { get; set; }
        public string redirectUrl { get; set; }
    }
}
