﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;
using fasc_openapi_donet_sdk.Model.ResponseModel;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 删除文档模板
    /// </summary>
    [RemoteService("/doc-template/delete", "POST")]
    public class DeleteDocTemplateReq : BaseReq<VoidRes>
    {
        public string openCorpId { get; set; }
        public string docTemplateId { get; set; }
    }
}
