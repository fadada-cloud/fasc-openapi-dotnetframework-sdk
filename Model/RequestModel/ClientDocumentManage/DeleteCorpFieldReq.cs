﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 删除自定义控件
    /// </summary>
    [RemoteService("/corp-field/delete", "POST")]
    public class DeleteCorpFieldReq : BaseReq<VoidRes>
    {
        public string openCorpId { get; set; }
        public DeleteCorpFileInfo[] fields { get; set; }
    }
}
