﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 启用/停用文档模板
    /// </summary>
    [RemoteService("/doc-template/set-status", "POST")]
    public class SetDocTemplateStatusReq : BaseReq<VoidRes>
    {
        public string openCorpId { get; set; }
        public string docTemplateId { get; set; }
        public string docTemplateStatus { get; set; }
    }
}
