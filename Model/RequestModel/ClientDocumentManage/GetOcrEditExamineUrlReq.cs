﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取合同智审页面链接
    /// </summary>
    [RemoteService("/ocr/edit/get-examineUrl", "POST")]
    public class GetOcrEditExamineUrlReq : BaseReq<GetOcrEditExamineUrlRes>
    {
        public OpenId initiator { get; set; }
        public string fileId { get; set; }
    }
}
