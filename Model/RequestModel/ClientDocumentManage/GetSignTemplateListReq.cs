﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取签署模板列表
    /// </summary>
    [RemoteService("/sign-template/get-list", "POST")]
    public class GetSignTemplateListReq : BaseReq<GetSignTemplateListRes>
    {
        public OpenId ownerId;
        public SignTemplateListFilterInfo listFilter;
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
    }
    public class SignTemplateListFilterInfo
    {
        public string signTemplateName;
    }
}
