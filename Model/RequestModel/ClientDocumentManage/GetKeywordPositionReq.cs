﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 查询文档关键字坐标
    /// </summary>
    [RemoteService("/file/get-keyword-positions", "POST")]
    public class GetKeywordPositionReq : BaseReq<List<GetKeywordPositionRes>>
    {
        public string fileId { get; set; }
        public string[] keywords { get; set; }
    }
}
