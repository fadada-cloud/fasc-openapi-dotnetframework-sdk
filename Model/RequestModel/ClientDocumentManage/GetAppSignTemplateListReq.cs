﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 查询应用签署任务模板列表
    /// </summary>
    [RemoteService("/app-sign-template/get-list", "POST")]
    public class GetAppSignTemplateListReq : BaseReq<GetAppSignTemplateListRes>
    {
        public AppListFilter listFilter { get; set; }
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
    }
    public class AppListFilter
    {
        public string appSignTemplateName { get; set; }
    }
}
