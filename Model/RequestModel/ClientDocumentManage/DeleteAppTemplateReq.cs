﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 删除应用文档模板
    /// </summary>
    [RemoteService("/app-doc-template/delete", "POST")]
    public class DeleteAppTemplateReq : BaseReq<VoidRes>
    {
        public string appDocTemplateId { get; set; }
    }
}
