﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// OFD文件追加
    /// </summary>
    [RemoteService("/file/ofd-file-merge", "POST")]
    public class OfdFileMergeReq : BaseReq<OfdFileMergeRes>
    {
        public string fileId { get; set; }
        public string insertFileId { get; set; }
        public string mergeFileName { get; set; }
    }
}
