﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取签署模板详情
    /// </summary>
    [RemoteService("/sign-template/get-detail", "POST")]
    public class GetSignTemplateDetailReq : BaseReq<GetSignTemplateDetailRes>
    {
        public OpenId ownerId { get; set; }
        public string signTemplateId { get; set; }
    }

}
