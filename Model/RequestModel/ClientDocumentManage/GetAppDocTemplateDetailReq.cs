﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用文档模板详情
    /// </summary>
    [RemoteService("/app-doc-template/get-detail", "POST")]
    public class GetAppDocTemplateDetailReq : BaseReq<GetAppDocTemplateDetailRes>
    {
        public string appDocTemplateId { get; set; }
    }
}
