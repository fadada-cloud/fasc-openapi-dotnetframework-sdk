﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取本地上传文件地址
    /// </summary>
    [RemoteService("/file/get-upload-url", "POST")]
    public class GetLocalUploadFileUrlReq : BaseReq<GetLocalUploadFileUrlRes>
    {
        public string fileType { get; set; }
        public string storageType { get; set; }
    }
}
