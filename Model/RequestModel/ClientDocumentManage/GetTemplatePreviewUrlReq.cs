﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取模板预览链接
    /// </summary>
    [RemoteService("/template/preview/get-url", "POST")]
    public class GetTemplatePreviewUrlReq : BaseReq<GetTemplatePreviewUrlRes>
    {
        public string openCorpId { get; set; }
        public string templateId { get; set; }
        public string redirectUrl { get; set; }
    }
}
