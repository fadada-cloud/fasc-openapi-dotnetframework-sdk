﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;
using DocFieldValueInfo = fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask.DocFieldValueInfo;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    // <summary>
    /// 填写文档模板生成文件
    /// </summary>
    [RemoteService("/doc-template/fill-values", "POST")]
    public class DocTemplateFillValuesReq : BaseReq<DocTemplateFillValuesRes>
    {
        public string openCorpId { get; set; }
        public string docTemplateId { get; set; }
        public string fileName { get; set; }
        public DocFieldValueInfo[] docFieldValues { get; set; }
    }
}
