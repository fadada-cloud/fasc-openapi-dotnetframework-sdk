﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 复制新增文档模板
    /// </summary>
    [RemoteService("/doc-template/copy-create", "POST")]
    public class CopyCreateDocTemplateReq : BaseReq<CopyCreateDocTemplateRes>
    {
        public string openCorpId { get; set; }
        public string docTemplateId { get; set; }
        public string docTemplateName { get; set; }
        public string createSerialNo { get; set; }
    }
}
