﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 启用/停用应用文档模板
    /// </summary>
    [RemoteService("/app-doc-template/set-status", "POST")]
    public class SetTemplateStatusReq : BaseReq<VoidRes>
    {
        public string appDocTemplateId { get; set; }
        public string appDocTemplateStatus { get; set; }
    }
}
