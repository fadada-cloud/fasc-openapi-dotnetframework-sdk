﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取模板新增链接
    /// </summary>
    [RemoteService("/template/create/get-url", "POST")]
    public class GetTemplateCreateUrlReq : BaseReq<GetTemplateCreateUrlRes>
    {
        public string openCorpId { get; set; }
        public string type { get; set; }
        public string createSerialNo { get; set; }
        public string redirectUrl { get; set; }
    }
}
