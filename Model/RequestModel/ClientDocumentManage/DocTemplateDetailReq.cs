﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取文件模板详情
    /// </summary>
    [RemoteService("/doc-template/get-detail", "POST")]
    public class DocTemplateDetailReq : BaseReq<DocTemplateDetailRes>
    {
        public OpenId ownerId { get; set; }
        public string docTemplateId { get; set; }
    }
}
