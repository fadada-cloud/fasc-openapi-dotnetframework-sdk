﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientDocumentManage
{
    /// <summary>
    /// 获取历史文件对比页面链接
    /// </summary>
    [RemoteService("/ocr/edit/compareResultUrl", "POST")]
    public class GetOcrEditCompareResultUrlReq : BaseReq<GetOcrEditCompareResultUrlRes>
    {
        public OpenId initiator { get; set; }
        public string compareId { get; set; }
    }
}
