﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientApproveManage
{
    /// <summary>
    /// 获取审批链接
    /// </summary>
    [RemoteService("/approval/get-url", "POST")]
    public class GetApprovalUrlReq : BaseReq<GetApprovalUrlRes>
    {
        public string openCorpId { get; set; }
        public string clientUserId { get; set; }
        public string approvalId { get; set; }
        public string redirectUrl { get; set; }
    }
}
