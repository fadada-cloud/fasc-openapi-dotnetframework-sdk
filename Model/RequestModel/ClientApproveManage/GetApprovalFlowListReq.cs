﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage;
using System.Collections.Generic;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientApproveManage
{
    /// <summary>
    /// 查询审批流程列表
    /// </summary>
    [RemoteService("/approval-flow/get-list", "POST")]
    public class GetApprovalFlowListReq : BaseReq<List<GetApprovalFlowListRes>>
    {
        public string openCorpId { get; set; }
        public string approvalType { get; set; }
    }
}
