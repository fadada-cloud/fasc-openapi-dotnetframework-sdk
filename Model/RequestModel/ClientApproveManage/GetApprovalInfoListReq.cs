﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientApproveManage
{
    /// <summary>
    /// 查询审批单据列表
    /// </summary>
    [RemoteService("/approval/get-list", "POST")]
    public class GetApprovalInfoListReq : BaseReq<GetApprovalInfoListRes>
    {
        public string openCorpId { get; set; }
        public string approvalType { get; set; }
        public string[] approvalStatus { get; set; }
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
    }
}
