﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientApproveManage
{
    /// <summary>
    /// 查询审批流程详情
    /// </summary>
    [RemoteService("/approval-flow/get-detail", "POST")]
    public class GetApprovalFlowDetailReq : BaseReq<GetApprovalFlowDetailRes>
    {
        public string openCorpId { get; set; }
        public string approvalFlowId { get; set; }
        public string approvalType { get; set; }
    }
}
