﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientApproveManage
{
    /// <summary>
    /// 查询审批单据详情
    /// </summary>
    [RemoteService("/approval/get-detail", "POST")]
    public class GetApprovalDetailReq : BaseReq<GetApprovalDetailRes>
    {
        public string openCorpId { get; set; }
        public string approvalType { get; set; }
        public string approvalId { get; set; }
    }
}
