﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 通行证OCR
    /// </summary>
    [RemoteService("/user/ocr/mainland-permit", "POST")]
    public class MainlandPermitOcrReq : BaseReq<MainlandPermitOcrRes>
    {
        public string imageBase64 { get; set; }
    }
}
