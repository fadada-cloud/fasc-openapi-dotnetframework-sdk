﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 查询人脸核验结果
    /// </summary>
    [RemoteService("/user/verify/face-status-query", "POST")]
    public class GetFaceRecognitionStatusReq : BaseReq<GetFaceRecognitionStatusRes>
    {
        public string serialNo { get; set; }
        public int? getFile { get; set; }
    }
}
