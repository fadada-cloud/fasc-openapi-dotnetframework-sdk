﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 个人二要素校验
    /// </summary>
    [RemoteService("/user/identity/two-element-verify", "POST")]
    public class IdentityTwoElementVerifyReq : BaseReq<IdentityTwoElementVerifyRes>
    {
        public string userName { get; set; }
        public string userIdentNo { get; set; }
    }
}
