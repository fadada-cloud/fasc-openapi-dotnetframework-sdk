﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 获取人脸核验链接
    /// </summary>
    [RemoteService("/user/verify/face-recognition", "POST")]
    public class GetFaceRecognitionUrlReq : BaseReq<GetFaceRecognitionUrlRes>
    {
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string faceauthMode { get; set; }
        public string redirectUrl { get; set; }
        public string createSerialNo { get; set; }
    }
}
