﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 获取要素校验身份证图片下载链接
    /// </summary>
    [RemoteService("/user/element-verify/get-idcard-image-download-url", "POST")]
    public class GetIdCardImageDownloadUrlReq : BaseReq<GetIdCardImageDownloadUrlRes>
    {
        public string verifyId { get; set; }
    }
}
