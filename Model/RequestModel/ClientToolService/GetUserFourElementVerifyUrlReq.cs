﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 获取个人银行卡四要素校验链接
    /// </summary>
    [RemoteService("/user/four-element-verify/get-url", "POST")]
    public class GetUserFourElementVerifyUrlReq : BaseReq<GetFourElementsVerifyUrlRes>
    {
        public string clientUserId { get; set; }
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string bankAccountNo { get; set; }
        public string mobile { get; set; }
        public bool? idCardImage { get; set; }
        public string faceSide { get; set; }
        public string nationalEmblemSide { get; set; }
        public string redirectUrl { get; set; }
    }
}
