﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 驾驶证OCR
    /// </summary>
    [RemoteService("/user/ocr/drivinglicense", "POST")]
    public class DrivingLicenseOcrReq : BaseReq<DrivingLicenseOcrRes>
    {
        public string imageBase64 { get; set; }
        public string backImageBase64 { get; set; }
    }
}
