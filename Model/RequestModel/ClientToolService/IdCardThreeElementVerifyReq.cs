﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 人脸图片比对校验
    /// </summary>
    [RemoteService("/user/identity/idcard-three-element-verify", "POST")]
    public class IdCardThreeElementVerifyReq : BaseReq<IdCardThreeElementVerifyRes>
    {
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string imgBase64 { get; set; }
    }
}
