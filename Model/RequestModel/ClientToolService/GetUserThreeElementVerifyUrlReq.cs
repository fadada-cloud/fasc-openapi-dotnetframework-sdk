﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 获取个人运营商三要素校验链接
    /// </summary>
    [RemoteService("/user/three-element-verify/get-url", "POST")]
    public class GetUserThreeElementVerifyUrlReq : BaseReq<GetThreeElementsVerifyUrlRes>
    {
        public string clientUserId { get; set; }
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string mobile { get; set; }
        public bool? idCardImage { get; set; }
        public string faceSide { get; set; }
        public string nationalEmblemSide { get; set; }
        public string redirectUrl { get; set; }
    }
}
