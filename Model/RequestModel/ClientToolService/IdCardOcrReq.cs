﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 身份证OCR
    /// </summary>
    [RemoteService("/user/ocr/id-card", "POST")]
    public class IdCardOcrReq : BaseReq<IdCardOcrRes>
    {
        public string faceSide { get; set; }
        public string nationalEmblemSide { get; set; }
    }
}
