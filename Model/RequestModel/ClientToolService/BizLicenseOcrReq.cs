﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 营业执照OCR
    /// </summary>
    [RemoteService("/user/ocr/license", "POST")]
    public class BizLicenseOcrReq : BaseReq<BizLicenseOcrRes>
    {
        public string imageBase64 { get; set; }
    }
}
