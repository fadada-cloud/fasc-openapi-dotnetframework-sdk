﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 银行卡OCR
    /// </summary>
    [RemoteService("/user/ocr/bankcard", "POST")]
    public class BankCardOcrReq : BaseReq<BankCardOcrRes>
    {
        public string imageBase64 { get; set; }
    }
}
