﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 个人银行卡三要素校验
    /// </summary>
    [RemoteService("/user/identity/bank-three-element-verify", "POST")]
    public class BankThreeElementVerifyReq : BaseReq<BankThreeElementVerifyRes>
    {
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string bankAccountNo { get; set; }
    }
}
