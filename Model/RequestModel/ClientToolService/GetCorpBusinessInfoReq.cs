﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 企业工商信息查询
    /// </summary>
    [RemoteService("/corp/identity/business-info-query", "POST")]
    public class GetCorpBusinessInfoReq : BaseReq<GetCorpBusinessInfoRes>
    {
        public string corpName { get; set; }
        public string corpidentNo { get; set; }
    }
}
