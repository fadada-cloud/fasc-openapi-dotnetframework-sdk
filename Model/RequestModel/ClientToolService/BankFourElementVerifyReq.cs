﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 个人银行卡四要素校验
    /// </summary>
    [RemoteService("/user/bank/four-element-verify", "POST")]
    public class BankFourElementVerifyReq : BaseReq<BankFourElementVerifyRes>
    {
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string bankAccountNo { get; set; }
        public string mobile { get; set; }
    }
}
