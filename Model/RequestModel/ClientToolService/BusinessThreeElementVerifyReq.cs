﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 企业组织三要素校验
    /// </summary>
    [RemoteService("/corp/identity/business-three-element-verify", "POST")]
    public class BusinessThreeElementVerifyReq : BaseReq<BusinessThreeElementVerifyRes>
    {
        public string corpName { get; set; }
        public string corpidentNo { get; set; }
        public string legalRepName { get; set; }
    }
}
