﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 个人运营商三要素校验
    /// </summary>
    [RemoteService("/user/telecom/three-element-verify", "POST")]
    public class TelecomThreeElementVerifyReq : BaseReq<TelecomThreeElementVerifyRes>
    {
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string mobile { get; set; }
    }
}
