﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientToolService
{
    // <summary>
    /// 行驶证OCR
    /// </summary>
    [RemoteService("/user/ocr/vehiclelicense", "POST")]
    public class VehicleLicenseOcrReq : BaseReq<VehicleLicenseOcrRes>
    {
        public string imageBase64 { get; set; }
        public string backImageBase64 { get; set; }
    }
}
