﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientOCR;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientOCR
{
    // <summary>
    /// 查询历史比对数据
    /// </summary>
    [RemoteService("/ocr/edit/compare-result-data", "POST")]
    public class GetCompareResultDataReq : BaseReq<GetCompareResultDataRes>
    {
        public OpenId initiator { get; set; }
        public string compareId { get; set; }
    }
}
