﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientOCR;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientOCR
{
    // <summary>
    /// 获取历史文件比对页面链接
    /// </summary>
    [RemoteService("/ocr/edit/compare-result-url", "POST")]
    public class GetCompareResultUrlReq : BaseReq<GetCompareResultUrlRes>
    {
        public OpenId initiator { get; set; }
        public string compareId { get; set; }
    }
}
