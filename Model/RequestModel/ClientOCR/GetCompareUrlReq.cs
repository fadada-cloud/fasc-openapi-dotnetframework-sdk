﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientOCR;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientOCR
{
    // <summary>
    /// 获取文件比对页面链接
    /// </summary>
    [RemoteService("/ocr/edit/get-compare-url", "POST")]
    public class GetCompareUrlReq : BaseReq<GetCompareUrlRes>
    {
        public OpenId initiator { get; set; }
        public string originFileId { get; set; }
        public string targetFileId { get; set; }
    }
}
