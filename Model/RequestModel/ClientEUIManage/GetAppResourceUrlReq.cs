﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientEUIManage;
using Newtonsoft.Json;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientEUIManage
{
    /// <summary>
    /// 获取应用级资源访问链接
    /// </summary>
    [RemoteService("/app-page-resource/get-url", "POST")]
    public class GetAppResourceUrlReq : BaseReq<GetAppResourceUrlRes>
    {
        public OpenId ownerId { get; set; }
        public AppResouce resource { get; set; }
    }

    public class AppResouce
    {
        public string resourceId { get; set; }
        public string action { get; set; }

        [JsonProperty(PropertyName = "params")]
        public string Params { get; set; }
    }
}
