﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientEUIManage;
using Newtonsoft.Json;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientEUIManage
{
    /// <summary>
    /// 获取用户级资源访问链接
    /// </summary>
    [RemoteService("/user-page-resource/get-url", "POST")]
    public class GetUserResourceUrlReq : BaseReq<GetUserResourceUrlRes>
    {
        public string openCorpId { get; set; }
        public string clientUserId { get; set; }
        public UserResouce resource { get; set; }
    }

    public class UserResouce
    {
        public string resourceId { get; set; }
        public string action { get; set; }

        [JsonProperty(PropertyName = "params")]
        public string Params { get; set; }
    }
}
