﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientAppField;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientAppField
{
    /// <summary>
    /// 创建业务控件
    /// </summary>
    [RemoteService("/app-field/create", "POST")]
    public class AppFieldCreateReq : BaseReq<AppFieldCreateRes>
    {
        public string fieldKey { get; set; }
        public string fieldName { get; set; }
        public string fieldType { get; set; }
    }
}
