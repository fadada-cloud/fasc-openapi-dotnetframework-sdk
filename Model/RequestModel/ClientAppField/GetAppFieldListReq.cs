﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientAppField;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientAppField
{
    /// <summary>
    /// 设置业务控件状态
    /// </summary>
    [RemoteService("/app-field/get-list", "POST")]
    public class GetAppFieldListReq : BaseReq<GetAppFieldListRes>
    {
    }
}
