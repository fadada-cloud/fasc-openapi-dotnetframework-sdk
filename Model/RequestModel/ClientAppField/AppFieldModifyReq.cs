﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientAppField;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientAppField
{
    /// <summary>
    /// 修改业务控件
    /// </summary>
    [RemoteService("/app-field/modify", "POST")]
    public class AppFieldModifyReq : BaseReq<AppFieldModifyRes>
    {
        public string fieldKey { get; set; }
        public string fieldName { get; set; }
    }
}
