﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientUserManage
{
    /// <summary>
    /// 禁用个人用户类
    /// </summary>
    [RemoteService("/user/disable", "POST")]
    public class UserForbidenReq : BaseReq<UserForbidenRes>
    {
        /// <summary>
        /// 法大大平台为该用户在该应用appId范围内分配的唯一标识
        /// </summary>
        public string openUserId { get; set; }
    }
}
