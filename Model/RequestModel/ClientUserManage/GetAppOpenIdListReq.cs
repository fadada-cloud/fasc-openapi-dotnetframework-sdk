﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientUserManage
{
    /// <summary>
    /// 查询授权用户列表
    /// </summary>
    [RemoteService("/app/get-openId-list", "POST")]
    public class GetAppOpenIdListReq : BaseReq<GetAppOpenIdListRes>
    {
        public string idType { get; set; }
        public bool? owner { get; set; }
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }
    }
}
