﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientUserManage
{
    /// <summary>
    /// 获取个人用户的基本信息、认证状态、授权状态和范围等
    /// </summary>
    [RemoteService("/user/get", "POST")]
    public class GetUserInfoReq : BaseReq<GetUserInfoRes>
    {
        /// <summary>
        /// 法大大平台为该用户在该应用appId范围内分配的唯一标识
        /// </summary>
        /// **注意：clientUserId与openUserId传值只能二选一，不能同时为空。** |
        public string openUserId { get; set; }
        /// <summary>
        /// 个人用户在应用中的唯一标识，
        /// </summary>
        public string clientUserId { get; set; }
    }
}
