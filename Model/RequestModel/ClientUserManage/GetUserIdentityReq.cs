﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientUserManage
{
    /// <summary>
    /// 获取个人用户身份信息
    /// </summary>
    [RemoteService("/user/get-identity-info", "POST")]
    public class GetUserIdentityReq : BaseReq<GetUserIdentityRes>
    {
        /// <summary>
        /// 法大大平台为该用户在该应用appId范围内分配的唯一标识
        /// </summary>
        public string openUserId { get; set; }
    }
}
