﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientUserManage
{
    /// <summary>
    /// 恢复个人用户
    /// </summary>
    [RemoteService("/user/account-name/get-change-url", "POST")]
    public class GetChangeAccountUrlReq : BaseReq<GetChangeAccountRes>
    {
        /// <summary>
        /// 需要更换帐号类型
        /// </summary>
        public string changeType { get; set; }
        /// <summary>
        /// 重定向地址。长度最大1000个字符
        /// </summary>
        public string redirectUrl { get; set; }
    }
}
