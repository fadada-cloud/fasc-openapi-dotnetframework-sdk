﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientUserManage
{
    /// <summary>
    /// 恢复个人用户
    /// </summary>
    [RemoteService("/user/enable", "POST")]
    public class UserEnableReq : BaseReq<UserEnableRes>
    {
        /// <summary>
        /// 法大大平台为该用户在该应用appId范围内分配的唯一标识
        /// </summary>
        public string openUserId { get; set; }
    }
}
