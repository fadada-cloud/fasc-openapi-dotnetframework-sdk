﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientUserManage
{
    /// <summary>
    /// 解绑个人用户
    /// </summary>
    [RemoteService("/user/unbind", "POST")]
    public class UserUnbindReq : BaseReq<UserUnbindRes>
    {
        /// <summary>
        /// 个人建立免登时对应的clientUserId。长度最大64个字符。传此字段表示解除该clientUserId的免登关系
        /// </summary>
        public string clientUserId { get; set; }
        /// <summary>
        /// 法大大平台为该用户在该应用appId范围内分配的唯一标识
        /// </summary>
        public string openUserId { get; set; }
    }
}
