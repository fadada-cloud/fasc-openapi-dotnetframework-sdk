﻿using fasc_openapi_donet_sdk.Attributes;
using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage;

namespace fasc_openapi_donet_sdk.Model.RequestModel.ClientUserManage
{
    /// <summary>
    ///获取个人用户授权链接
    /// </summary>
    [RemoteService("/user/get-auth-url", "POST")]
    public class GetUserAuthUrlReq : BaseReq<GetUserAuthUrlRes>
    {
        /// <summary>
        /// 个人用户在应用中的唯一标识，由集成方自定义，长度最大64个字符。
        /// </summary>
        public string clientUserId { get; set; }
        /// <summary>
        /// 个人用户的法大大帐号，仅限手机号或邮箱，长度最大30个字符。
        /// 如该手机号或邮箱未注册法大大，则用户会以此作为注册账号。
        /// 当用户accountName与mobile传相同手机号，同时设置认证方式为手机号认证，用户
        /// 打开链接后只需输入一次验证码即可完成认证和授权。
        /// </summary>
        public string accountName { get; set; }
        /// <summary>
        /// 如clientUserId已经绑定了法大大帐号，或绑定的实名信息与本次认证实名信息不一
        /// 致，用户打开链接后是否默认解绑之前的帐号并以本次登录的帐号更新实名信息
        /// 默认为false
        /// false：不解绑
        /// true：解绑
        /// </summary>
        public bool? unbindAccount { get; set; }
        /// <summary>
        /// 个人认证信息
        /// </summary>
        public UserIdentInfo userIdentInfo { get; set; }
        /// <summary>
        /// 页面中不可编辑的个人信息，不传默认都可编辑
        /// </summary>
        public string[] nonEditableInfo { get; set; }
        /// <summary>
        /// 是否需要匹配个人身份信息一致false: 不需要匹配一致true: 需要匹配一致缺省为false
        /// </summary>
        public bool identInfoMatch { get; set; }
        /// <summary>
        /// 个人用户授权范围
        /// </summary>
        public string[] requestAuthScope { get; set; }
        public string[] authScopes { get; set; }
        public FreeSignInfoReq freeSignInfo { get; set; }
        /// <summary>
        /// 重定向地址
        /// </summary>
        public string redirectUrl { get; set; }
        /// <summary>
        /// 小程序的重定向地址（微信和支付宝），长度最大1000个字符。
        /// </summary>
        public string redirectMiniAppUrl { get; set; }
    }
}
