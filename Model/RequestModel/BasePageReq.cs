﻿namespace fasc_openapi_donet_sdk.Model.RequestModel
{
    public class BasePageReq
    {
        public int? listPageNo { get; set; }
        public int? listPageSize { get; set; }

    }
}
