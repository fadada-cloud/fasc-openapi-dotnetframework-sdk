﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientBillManage
{
    /// <summary>
    /// 获取计费链接
    /// </summary>
    public class GetBillUrlRes
    {
        /// <summary>
        /// 订购下单链接的Embedded URL形式
        /// </summary>
        public string eUrl { get; set; }
    }
}
