﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientBillManage
{
    public class GetUsageAvailablenumRes
    {
        public string usageCode { get; set; }
        public string usageName { get; set; }
        public string usageUnit { get; set; }
        public string availableNum { get; set; }
        public UsageShareDetail[] usageShareDetail { get; set; }
    }

    public class UsageShareDetail
    {
        public string amountUsable { get; set; }
        public string billAccountName { get; set; }
    }
}
