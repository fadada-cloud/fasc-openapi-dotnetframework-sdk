﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientEUIManage
{
    /// <summary>
    /// 获取用户级资源访问链接
    /// </summary>
    public class GetUserResourceUrlRes
    {
        public string resourceUrl { get; set; }
    }
}
