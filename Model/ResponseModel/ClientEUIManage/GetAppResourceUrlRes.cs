﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientEUIManage
{
    /// <summary>
    /// 获取应用级资源访问链接
    /// </summary>
    public class GetAppResourceUrlRes
    {
        public string resourceUrl { get; set; }
    }
}
