﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientAppField
{
    public class GetAppFieldListRes
    {
        public FieldLibraryInfo[] data { get; set; }
    }
    public class FieldLibraryInfo
    {
        public string fieldKey { get; set; }
        public string fieldName { get; set; }
        public string fieldType { get; set; }
        public string fieldStatus { get; set; }
    }
}
