﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService
{
    public class TelecomThreeElementVerifyRes
    {
        public bool? verifyResult { get; set; }
    }
}
