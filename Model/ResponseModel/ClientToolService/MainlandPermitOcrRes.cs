﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService
{
    public class MainlandPermitOcrRes
    {
        public string serialNo { get; set; }
        public string name { get; set; }
        public string englishName { get; set; }
        public string sex { get; set; }
        public string birthday { get; set; }
        public string issueAuthority { get; set; }
        public string validDate { get; set; }
        public string number { get; set; }
        public string issueAddress { get; set; }
        public string issueNumber { get; set; }
        public string type { get; set; }
    }
}
