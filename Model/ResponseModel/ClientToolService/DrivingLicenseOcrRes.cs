﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService
{
    public class DrivingLicenseOcrRes
    {
        public string serialNo { get; set; }
        public string name { get; set; }
        public string nationality { get; set; }
        public string sex { get; set; }
        public string address { get; set; }
        public string birthday { get; set; }
        public string firstIssueDate { get; set; }
        public string number { get; set; }
        public string quasiDrivingType { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string archivesCode { get; set; }
        public string record { get; set; }
        public string[] warningCode { get; set; }
    }
}
