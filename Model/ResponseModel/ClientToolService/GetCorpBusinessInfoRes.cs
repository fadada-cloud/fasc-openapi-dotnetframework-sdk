﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService
{
    public class GetCorpBusinessInfoRes
    {
        public string serialNo { get; set; }
        public string orgName { get; set; }
        public string organizationNo { get; set; }
        public string companyCode { get; set; }
        public string legalRepName { get; set; }
        public string orgType { get; set; }
        public string orgStatus { get; set; }
        public string authority { get; set; }
        public string capital { get; set; }
        public string address { get; set; }
        public string businessScope { get; set; }
        public string establishDate { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string issueDate { get; set; }
    }
}
