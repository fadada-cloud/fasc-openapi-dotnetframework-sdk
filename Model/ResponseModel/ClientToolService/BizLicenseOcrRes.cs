﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService
{
    public class BizLicenseOcrRes
    {
        public string serialNo { get; set; }
        public string creditNo { get; set; }
        public string companyName { get; set; }
        public string companyType { get; set; }
        public string address { get; set; }
        public string registerNo { get; set; }
        public string serialNumber { get; set; }
        public string artificialName { get; set; }
        public string regCapital { get; set; }
        public string startTime { get; set; }
        public string operatingPeriod { get; set; }
        public string scope { get; set; }
        public string[] warningCode { get; set; }
    }
}
