﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService
{
    public class IdCardOcrRes
    {
        public string serialNo { get; set; }
        public string name { get; set; }
        public string identNo { get; set; }
        public string gender { get; set; }
        public string birthday { get; set; }
        public string nation { get; set; }
        public string address { get; set; }
        public string issueAuthority { get; set; }
        public string validPeriod { get; set; }
    }
}
