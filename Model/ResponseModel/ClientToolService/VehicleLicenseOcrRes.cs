﻿using fasc_openapi_donet_sdk.Model.CommonModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService
{
    public class VehicleLicenseOcrRes
    {
        public string serialNo { get; set; }
        public VehicleFrontInfo vehicleFrontInfo { get; set; }
        public VehicleBackInfo vehicleBackInfo { get; set; }
        public string[] warningCode { get; set; }
    }
}
