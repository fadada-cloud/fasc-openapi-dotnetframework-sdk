﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService
{
    public class GetFaceRecognitionStatusRes
    {
        public string userName { get; set; }
        public string userIdentNo { get; set; }
        public string faceauthMode { get; set; }
        public int? resultStatus { get; set; }
        public string resultTime { get; set; }
        public string failedReason { get; set; }
        public int? urlStatus { get; set; }
        public string faceImg { get; set; }
        public string[] faceImgs { get; set; }
        public string similarity { get; set; }
        public string liveRate { get; set; }
    }
}
