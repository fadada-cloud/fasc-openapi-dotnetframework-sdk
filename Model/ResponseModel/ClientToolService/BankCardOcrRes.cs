﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientToolService
{
    public class BankCardOcrRes
    {
        public string serialNo { get; set; }
        public string bankName { get; set; }
        public string bankCardType { get; set; }
        public string bankCardNo { get; set; }
        public string validDate { get; set; }
        public string[] warningCode { get; set; }
    }
}
