﻿using System;

namespace fasc_openapi_donet_sdk.Model.ResponseModel
{
    public class AccessTokenRes
    {
        public string accessToken { get; set; }
        public string expiresIn { get; set; }

        /// <summary>
        /// 有效截⽌时间(yyyy-MM-dd HH:mm:ss.sss)
        /// </summary>
        public DateTime expiresTime { get; set; }
    }
}
