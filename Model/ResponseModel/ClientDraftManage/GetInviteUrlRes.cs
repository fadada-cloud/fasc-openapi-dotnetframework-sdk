﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDraftManage
{
    public class GetInviteUrlRes
    {
        public string contractName { get; set; }
        public string createdByOwner { get; set; }
        public string createdByName { get; set; }
        public string inviteLinkUrl { get; set; }
    }
}
