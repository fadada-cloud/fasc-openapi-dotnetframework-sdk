﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDraftManage
{
    public class GetFinishedFileRes
    {
        public string fileId { get; set; }
        public string contractSubject { get; set; }
        public string fileUrl { get; set; }
    }
}
