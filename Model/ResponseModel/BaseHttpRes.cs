﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel
{
    public class BaseHttpRes
    {
        private int httpStatusCode;

        public int getHttpStatusCode()
        {
            return httpStatusCode;
        }

        public void setHttpStatusCode(int code)
        {
            this.httpStatusCode = code;
        }
    }
}
