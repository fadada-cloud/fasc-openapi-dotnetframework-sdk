﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取文件对比页面链接
    /// </summary>
    public class GetOcrEditCompareUrlRes
    {
        public string compareId { get; set; }
        public string compareUrl { get; set; }
    }
}
