﻿using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用签署任务模板详情
    /// </summary>
    public class GetAppSignTemplateDetailRes
    {
        public string appSignTemplateId { get; set; }
        public string appSignTemplateName { get; set; }
        public string appSignTemplateStatus { get; set; }
        public string certCAOrg { get; set; }
        public bool signInOrder { get; set; }
        public Doc[] docs { get; set; }
        public Attach[] attachs { get; set; }
        public SignTaskActor[] actors { get; set; }
    }
    public class SignTaskActor
    {
        public ActorInfo actorInfo { get; set; }

        public FillField[] fillFields { get; set; }
        public SignField[] signFields { get; set; }

        public SignConfigInfo signConfigInfo { get; set; }
    }
}
