﻿using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用文档模板详情
    /// </summary>
    public class GetAppDocTemplateDetailRes
    {
        public string appDocTemplateId { get; set; }
        public string appDocTemplateName { get; set; }
        public string appDocTemplateStatus { get; set; }
        public Field[] docFields { get; set; }
    }
}
