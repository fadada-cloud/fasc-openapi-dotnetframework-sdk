﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取合同智审页面链接
    /// </summary>
    public class GetOcrEditExamineUrlRes
    {
        public string examineid { get; set; }
        public string examineUrl { get; set; }
    }
}
