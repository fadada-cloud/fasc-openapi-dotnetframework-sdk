﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取模板管理链接
    /// </summary>
    public class GetTemplateManageUrlRes
    {
        public string templateManageUrl { get; set; }
    }
}
