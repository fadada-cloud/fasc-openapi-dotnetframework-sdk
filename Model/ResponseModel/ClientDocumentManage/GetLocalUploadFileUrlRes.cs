﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    public class GetLocalUploadFileUrlRes
    {
        public string uploadUrl { get; set; }
        public string fddFileUrl { get; set; }
    }
}
