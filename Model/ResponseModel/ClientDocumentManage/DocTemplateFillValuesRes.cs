﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    public class DocTemplateFillValuesRes
    {
        public string fileId { get; set; }
        public string fileDownloadUrl { get; set; }
    }
}
