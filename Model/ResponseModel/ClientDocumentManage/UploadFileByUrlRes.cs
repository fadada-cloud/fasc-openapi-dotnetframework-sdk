﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    public class UploadFileByUrlRes
    {
        /// <summary>
        /// 法大大云存储中的源文件地址
        /// </summary>
        public string fddFileUrl { get; set; }
    }
}
