﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 文件处理
    /// </summary>
    public class FileProcessRes
    {
        public FileProcessArr[] fileIdList { get; set; }
    }

    public class FileProcessArr
    {
        public string fileId { get; set; }
        public string fileType { get; set; }
        public string fddFileUrl { get; set; }
        public string fileName { get; set; }
        public int? fileTotalPages { get; set; }
    }
}
