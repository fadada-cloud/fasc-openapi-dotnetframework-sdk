﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    public class CopyCreateDocTemplateRes
    {
        public string docTemplateId { get; set; }
        public string docTemplateName { get; set; }
        public string createSerialNo { get; set; }
    }
}
