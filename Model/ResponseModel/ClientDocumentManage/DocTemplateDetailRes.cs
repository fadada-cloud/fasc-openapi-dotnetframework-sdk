﻿using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取文档模板详情
    /// </summary>
    public class DocTemplateDetailRes
    {
        public string docTemplateId { get; set; }
        public string docTemplateName { get; set; }
        public string createSerialNo { get; set; }
        public string catalogName { get; set; }
        public string docTemplateStatus { get; set; }
        public string storageType { get; set; }
        public Field[] docFields { get; set; }
    }
}
