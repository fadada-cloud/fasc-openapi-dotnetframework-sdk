﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用模板预览链接
    /// </summary>
    public class GetAppTemplatePreviewUrlRes
    {
        public string appTemplatePreviewUrl { get; set; }
    }
}
