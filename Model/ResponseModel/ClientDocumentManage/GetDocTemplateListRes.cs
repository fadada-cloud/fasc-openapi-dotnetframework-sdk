﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取文件模板列表
    /// </summary>
    public class GetDocTemplateListRes
    {
        public DocTemplate[] docTemplates { get; set; }
        public int listPageNo { get; set; }
        public int countInPage { get; set; }
        public int listPageCount { get; set; }
        public int totalCount { get; set; }
    }

    public class DocTemplate
    {
        public string docTemplateId { get; set; }
        public string docTemplateName { get; set; }
        public string docTemplateStatus { get; set; }
        public string createTime { get; set; }
        public string updateTime { get; set; }
        public string templateVersion { get; set; }
        public string catalogName { get; set; }
        public string createSerialNo { get; set; }
        public string storageType { get; set; }
    }
}
