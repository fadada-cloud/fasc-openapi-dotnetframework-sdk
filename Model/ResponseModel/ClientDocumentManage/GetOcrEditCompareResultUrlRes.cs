﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取历史文件对比页面链接
    /// </summary>
    public class GetOcrEditCompareResultUrlRes
    {
        public string compareUrl { get; set; }
    }
}
