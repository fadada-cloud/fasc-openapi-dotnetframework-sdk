﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    public class GetKeywordPositionRes
    {
        public string keyword { get; set; }
        public Position positions { get; set; }
    }

    public class Position
    {
        public int? positionPageNo { get; set; }
        public Coordinate coordinate { get; set; }
    }

    public class Coordinate
    {
        public string positionX { get; set; }
        public string positionY { get; set; }
    }
}
