﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取模板新增链接
    /// </summary>
    public class GetTemplateCreateUrlRes
    {
        public string templateCreateUrl { get; set; }
    }
}
