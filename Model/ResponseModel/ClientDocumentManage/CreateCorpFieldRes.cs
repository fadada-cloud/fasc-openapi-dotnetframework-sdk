﻿using fasc_openapi_donet_sdk.Model.CommonModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    public class CreateCorpFieldRes
    {
        public CreateCorpFieldFailInfo[] failField { get; set; }
    }
}
