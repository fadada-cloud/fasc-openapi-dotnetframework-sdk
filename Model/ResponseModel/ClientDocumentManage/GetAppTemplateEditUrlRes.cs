﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用模板编辑链接
    /// </summary>
    public class GetAppTemplateEditUrlRes
    {
        public string appTemplateEditUrl { get; set; }
    }
}
