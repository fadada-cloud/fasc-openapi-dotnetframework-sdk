﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取历史合同智审页面链接
    /// </summary>
    public class GetOcrEditExamineResultUrlRes
    {
        public string examineUrl { get; set; }
    }
}
