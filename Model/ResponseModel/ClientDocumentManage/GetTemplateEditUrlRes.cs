﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    public class GetTemplateEditUrlRes
    {
        public string templateEditUrl { get; set; }
    }
}
