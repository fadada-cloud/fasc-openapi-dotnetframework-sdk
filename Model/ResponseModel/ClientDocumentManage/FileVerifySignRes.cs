﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 文档验签
    /// </summary>
    public class FileVerifySignRes
    {
        public bool verifyResult { get; set; }
        public string reason { get; set; }
        public SignatureInfo[] signatureInfos { get; set; }
    }
    public class SignatureInfo
    {
        public string signer { get; set; }
        public string signedonTime { get; set; }
        public string authority { get; set; }
        public bool timestampFlag { get; set; }
        public string timestampTime { get; set; }
        public bool timestampVerifyFlag { get; set; }
        public bool integrityFlag { get; set; }
    }
}
