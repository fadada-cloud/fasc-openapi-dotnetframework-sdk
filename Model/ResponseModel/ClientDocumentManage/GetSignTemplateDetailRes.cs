﻿using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取签署模板详情
    /// </summary>
    public class GetSignTemplateDetailRes
    {
        public string signTemplateId { get; set; }
        public string signTemplateName { get; set; }
        public string createSerialNo { get; set; }
        public string catalogName { get; set; }
        public string businessTypeName { get; set; }
        public string signTemplateStatus { get; set; }
        public string storageType { get; set; }
        public string certCAOrg { get; set; }
        public bool signInOrder { get; set; }
        public Doc[] docs { get; set; }
        public Attach[] attachs { get; set; }
        public SignTaskTemActor[] actors { get; set; }
        public Watermark[] watermarks { get; set; }
        public ApprovalInfo[] approvalInfos { get; set; }
    }

    public class SignTaskTemActor
    {
        public ActorInfo actorInfo { get; set; }
        public FillField[] fillFields { get; set; }
        public SignField[] signFields { get; set; }
        public SignConfigInfo signConfigInfo { get; set; }
        public Notification notification { get; set; }
    }

    public class ActorInfo
    {
        public string actorId { get; set; }
        public string identNameForMatch { get; set; }
        public string certType { get; set; }
        public string certNoForMatch { get; set; }
        public bool isInitiator { get; set; }
        public string actorType { get; set; }
        public string[] permissions { get; set; }
        public long[] memberIds { get; set; }
    }
}
