﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取签署模板列表
    /// </summary>
    public class GetSignTemplateListRes
    {
        public SignTemplateListInfo[] signTemplates { get; set; }
        public int listPageNo { get; set; }
        public int countInPage { get; set; }
        public int listPageCount { get; set; }
        public int totalCount { get; set; }
    }

    public class SignTemplateListInfo
    {
        public string templateVersion { get; set; }
        public string signTemplateId { get; set; }
        public string createSerialNo { get; set; }
        public string signTemplateName { get; set; }
        public string catalogName { get; set; }
        public string businessTypeName { get; set; }
        public string signTemplateStatus { get; set; }
        public string storageType { get; set; }
        public string createTime { get; set; }
        public string updateTime { get; set; }
    }
}
