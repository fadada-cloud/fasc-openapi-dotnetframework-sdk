﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 查询应用文档模板列表
    /// </summary>
    public class GetAppDocTemplateListRes
    {
        public AppDocTemplate[] appDocTemplates { get; set; }
        public string listPageNo { get; set; }
        public int? countInPage { get; set; }
        public int? listPageCount { get; set; }
        public int? totalCount { get; set; }
    }
    public class AppDocTemplate
    {
        public string appDocTemplateId { get; set; }
        public string appDocTemplateName { get; set; }
        public string appDocTemplateStatus { get; set; }
        public string createTime { get; set; }
        public string updateTime { get; set; }
    }
}
