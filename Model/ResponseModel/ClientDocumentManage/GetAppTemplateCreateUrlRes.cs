﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 获取应用模板新增链接
    /// </summary>
    public class GetAppTemplateCreateUrlRes
    {
        public string appTemplateCreateUrl { get; set; }
    }
}
