﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    public class GetTemplatePreviewUrlRes
    {
        public string templatePreviewUrl { get; set; }
    }
}
