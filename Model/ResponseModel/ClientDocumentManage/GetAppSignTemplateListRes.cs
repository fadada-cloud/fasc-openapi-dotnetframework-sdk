﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientDocumentManage
{
    /// <summary>
    /// 查询应用签署任务模板列表
    /// </summary>
    public class GetAppSignTemplateListRes
    {
        public SignTemplate[] appSignTemplates { get; set; }
        public int? listPageNo { get; set; }
        public int? countInPage { get; set; }
        public int? listPageCount { get; set; }
        public int? totalCount { get; set; }
    }
    public class SignTemplate
    {
        public string appSignTemplateId { get; set; }
        public string appSignTemplateName { get; set; }
        public string appSignTemplateStatus { get; set; }
        public string createTime { get; set; }
        public string updateTime { get; set; }
    }
}
