﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCallBack
{
    public class GetCallBackListRes
    {
        public int listPageNo { get; set; }
        public int countInPage { get; set; }
        public int listPageCount { get; set; }
        public int totalCount { get; set; }
        public CallBackInfo[] callbackInfos { get; set; }
    }

    public class CallBackInfo
    {
        public string eventId { get; set; }
        public string eventInfo { get; set; }
        public bool success { get; set; }
    }
}
