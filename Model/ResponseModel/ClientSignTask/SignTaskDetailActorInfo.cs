namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class SignTaskDetailActorInfo
    {
		public string actorId { get; set; }
		public string actorType { get; set; }
		public string actorName { get; set; }
		public string[] permissions { get; set; }
		public bool? isInitiator { get; set; }
		public string actorOpenId { get; set; }
    }
}
