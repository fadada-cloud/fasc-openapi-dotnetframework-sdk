﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class SignTaskGetFileRes
    {
        public DocFileInfo[] docs { get; set; }
    }

    public class DocFileInfo
    {
        public string docId { get; set; }
        public string docName { get; set; }
        public string fileId { get; set; }
        public string fddFileUrl { get; set; }
    }
}
