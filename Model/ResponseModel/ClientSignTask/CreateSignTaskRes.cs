﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 创建签署任务(基于即时文档或文档模板)
    /// </summary>
    public class CreateSignTaskRes
    {
        public string signTaskId { get; set; }
    }
}
