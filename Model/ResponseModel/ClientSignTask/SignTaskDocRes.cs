namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class SignTaskDocRes
    {
		public string docId { get; set; }
		public string docName { get; set; }
		public string docFileId { get; set; }
    }
}
