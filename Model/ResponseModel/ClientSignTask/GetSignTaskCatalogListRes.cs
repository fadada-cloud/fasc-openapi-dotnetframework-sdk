﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 查询企业签署任务文件夹
    /// </summary>
    public class GetSignTaskCatalogListRes
    {
        public string catalogId { get; set; }
        public string catalogName { get; set; }
        public string parentCatalogId { get; set; }
    }
}
