﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务编辑链接
    /// </summary>
    public class GetSignTaskEditUrlRes
    {
        public string signTaskEditUrl { get; set; }
    }
}
