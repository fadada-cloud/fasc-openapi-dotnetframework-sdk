﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务参与方专属链接
    /// </summary>
    public class GetSignTaskActorUrlRes
    {
        public string actorSignTaskUrl { get; set; }
        public string actorSignTaskEmbedUrl { get; set; }
        public MiniAppInfo actorSignTaskMiniAppInfo { get; set; }
    }

    public class MiniAppInfo
    {
        public string wxOriginalId { get; set; }
        public string path { get; set; }
    }
}
