﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 获取指定归属方的签署任务文档下载地址
    /// </summary>
    public class GetOwnerSignTaskUrlRes
    {
        public string downloadUrl { get; set; }
        public string downloadId { get; set; }
    }
}
