﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 创建签署任务基于签署模板
    /// </summary>
    public class CreateSignTaskWithTemplateRes
    {
        public string signTaskId { get; set; }
    }
}
