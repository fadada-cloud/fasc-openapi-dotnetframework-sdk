﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务公证处保全报告下载地址
    /// </summary>
    public class GetSignTaskDownloadEvidenceReportRes
    {
        public string downloadUrl { get; set; }
    }
}
