﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class GetSignTaskBusinessTypeListRes
    {
        public long? businessTypeId { get; set; }
        public string businessTypeName { get; set; }
        public ApprovalFlowInfo[] approvalFlowInfo { get; set; }
    }

    public class ApprovalFlowInfo
    {
        public string approvalType { get; set; }
        public string approvalFlowId { get; set; }
        public string approvalFlowName { get; set; }
    }
}
