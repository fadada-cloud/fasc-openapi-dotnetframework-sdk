﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class SignTaskGetCerInfoRes
    {
        public CerInfoActor[] actors { get; set; }
    }

    public class CerInfoActor
    {
        public string actorId { get; set; }
        public string actorType { get; set; }
        public string actorName { get; set; }
        public string identName { get; set; }
        public string[] permissions { get; set; }
        public string[] cerFile { get; set; }
    }
}
