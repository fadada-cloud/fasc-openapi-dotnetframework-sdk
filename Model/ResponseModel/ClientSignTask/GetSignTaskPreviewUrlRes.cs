﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务预览链接
    /// </summary>
    public class GetSignTaskPreviewUrlRes
    {
        public string signTaskPreviewUrl { get; set; }
    }
}
