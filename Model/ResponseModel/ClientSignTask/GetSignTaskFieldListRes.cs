﻿using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// >查询签署任务控件信息
    /// </summary>
    public class GetSignTaskFieldListRes
    {
        public string signTaskId { get; set; }
        public string signTaskSubject { get; set; }
        public FillField[] fillFields { get; set; }

    }

    public class FillField
    {
        public string fieldId { get; set; }
        public string fieldName { get; set; }
        public string fieldType { get; set; }
        public string fieldValue { get; set; }
        public string actorId { get; set; }
        public string actorName { get; set; }
        public string docId { get; set; }
        public string docName { get; set; }
        public Position position { get; set; }
    }
}
