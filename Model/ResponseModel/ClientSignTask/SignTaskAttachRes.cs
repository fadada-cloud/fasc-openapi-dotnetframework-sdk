namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class SignTaskAttachRes
    {
		public string attachId { get; set; }
		public string attachName { get; set; }
    }
}
