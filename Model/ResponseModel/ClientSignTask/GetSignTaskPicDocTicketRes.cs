﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class GetSignTaskPicDocTicketRes
    {
        public string slicingTicketId { get; set; }
    }
}
