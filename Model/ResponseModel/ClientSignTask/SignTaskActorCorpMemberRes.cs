namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class SignTaskActorCorpMemberRes
    {
        public string memberName { get; set; }
        public string mobile { get; set; }
        public string memberId { get; set; }
    }
}
