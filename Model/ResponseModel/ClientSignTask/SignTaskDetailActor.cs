namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class SignTaskDetailActor
    {
		public SignTaskDetailActorInfo actorInfo { get; set; }
		public SignFieldRes[] signFields { get; set; }
		public string joinStatus { get; set; }
		public string joinTime { get; set; }
		public string fillStatus { get; set; }
		public string fillTime { get; set; }
		public string signStatus { get; set; }
		public string signTime { get; set; }
		public int? signOrderNo { get; set; }
		public string blockStatus { get; set; }
		public string actorNote { get; set; }
		public string actorSignTaskUrl { get; set; }
		public string actorSignTaskEmbedUrl { get; set; }
		public string readStatus { get; set; }
		public string readTime { get; set; }
    }
}
