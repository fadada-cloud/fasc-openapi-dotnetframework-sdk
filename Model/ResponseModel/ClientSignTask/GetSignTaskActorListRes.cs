using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class GetSignTaskActorListRes
    {
        public string actorId { get; set; }
		public string actorType { get; set; }
		public string actorName { get; set; }
		public string[] permissions { get; set; }
		public bool? isInitiator { get; set; }
		public int? signOrderNo { get; set; }
		public string identNameForMatch { get; set; }
		public string certNoForMatch { get; set; }
		public string joinStatus { get; set; }
		public string joinTime { get; set; }
		public string joinName { get; set; }
		public string joinIdentNo { get; set; }
		public string fillStatus { get; set; }
		public string fillTime { get; set; }
		public string signStatus { get; set; }
		public string actorNote { get; set; }
		public string signTime { get; set; }
		public string blockStatus { get; set; }
		public Notification notification { get; set; }
		public string readStatus { get; set; }
		public string readTime { get; set; }
		public string verifyMethod { get; set; }
		public string confirmationTime { get; set; }
		public SignTaskActorCorpMemberRes memberInfo { get; set; }
		public string corpName { get; set; }
		public string corpIdentNo { get; set; }
    }
}
