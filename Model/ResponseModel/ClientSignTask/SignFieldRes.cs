using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class SignFieldRes
    {
		public string signFieldStatus { get; set; }
		public string fieldDocId { get; set; }
		public string fieldId { get; set; }
		public string fieldName { get; set; }
		public bool? moveable { get; set; }
		public Position position { get; set; }
		public string signRemark { get; set; }
		public long? sealId { get; set; }
		public string categoryType { get; set; }
    }
}
