﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 获取签署任务批量签署链接
    /// </summary>
    public class GetSignTaskBatchSignUrlRes
    {
        public string batchSignUrl { get; set; }
    }
}
