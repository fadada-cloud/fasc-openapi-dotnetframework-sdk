using fasc_openapi_donet_sdk.Model.CommonModel;
using fasc_openapi_donet_sdk.Model.RequestModel;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class GetAppSignTaskDetailRes
    {
		public OpenId initiator { get; set; }
		public string initiatorMemberId { get; set; }
		public string initiatorMemberName { get; set; }
		public string signTaskId { get; set; }
		public string signTaskSubject { get; set; }
		public string storageType { get; set; }
		public string signDocType { get; set; }
		public string signTaskStatus { get; set; }
		public string createTime { get; set; }
		public string signTaskSource { get; set; }
		public string approvalStatus { get; set; }
		public string rejectNote { get; set; }
		public string businessTypeName { get; set; }
		public string businessCode { get; set; }
		public string templateId { get; set; }
		public string startTime { get; set; }
		public string finishTime { get; set; }
		public string deadlineTime { get; set; }
		public string terminationNote { get; set; }
		public string certCAOrg { get; set; }
		public long? businessTypeId { get; set; }
		public string revokeReason { get; set; }
		public bool? autoFillFinalize { get; set; }
		public bool? autoFinish { get; set; }
		public bool? signInOrder { get; set; }
		public string abolishedSignTaskId { get; set; }
		public string originalSignTaskId { get; set; }
		public string dueDate { get; set; }
		public SignTaskDocRes[] docs { get; set; }
		public SignTaskAttachRes[] attachs { get; set; }
		public SignTaskDetailActor[] actors { get; set; }
		public Watermark[] watermarks { get; set; }
		public ApprovalInfo[] approvalInfos { get; set; }
		public string fileFormat { get; set; }
		public string transReferenceId { get; set; }
		public string initiatorEntityId { get; set; }
    }
}
