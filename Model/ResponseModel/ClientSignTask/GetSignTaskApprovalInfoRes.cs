﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// >查询签署任务审批信息
    /// </summary>
    public class GetSignTaskApprovalInfoRes
    {
        public string signTaskId { get; set; }
        public string signTaskSubject { get; set; }
        public string approvalId { get; set; }
        public string approvalSubject { get; set; }
        public string applicantName { get; set; }
        public string applicationTime { get; set; }
        public string approvalStatus { get; set; }
        public approvalNode[] approvalNode { get; set; }

    }

    public class approvalNode
    {
        public int nodeId { get; set; }
        public string approvalType { get; set; }
        public string nodeStatus { get; set; }
        public approversInfo[] approversInfo { get; set; }
    }

    public class approversInfo
    {
        public string approverName { get; set; }
        public string approverStatus { get; set; }
        public string operateTime { get; set; }
        public string rejectNote { get; set; }
    }
}
