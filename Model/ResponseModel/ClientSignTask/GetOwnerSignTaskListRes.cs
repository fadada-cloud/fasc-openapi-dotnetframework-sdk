﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    /// <summary>
    /// 获取指定归属方的签署任务列表
    /// </summary>
    public class GetOwnerSignTaskListRes
    {
        public SignTask[] signTasks { get; set; }
        public int listPageNo { get; set; }
        public int countInPage { get; set; }
        public int listPageCount { get; set; }
        public int totalCount { get; set; }
    }

    public class SignTask
    {
        public string signTaskId { get; set; }
        public string transReferenceId { get; set; }
        public string signTaskSource { get; set; }
        public string signTaskSubject { get; set; }
        public string approvalStatus { get; set; }
        public string signTaskStatus { get; set; }
        public string rejectNote { get; set; }
        public string catalogId { get; set; }
        public string catalogName { get; set; }
        public string businessTypeName { get; set; }
        public string businessCode { get; set; }
        public string templateId { get; set; }
        public string terminationNote { get; set; }
        public string initiatorName { get; set; }
        public string actorName { get; set; }
        public string startTime { get; set; }
        public string createTime { get; set; }
        public string finishTime { get; set; }
        public string deadlineTime { get; set; }
        public long? businessTypeId { get; set; }
        public string abolishedSignTaskId { get; set; }
        public string originalSignTaskId { get; set; }
        public string initiatorMemberId { get; set; }
        public string dueDate { get; set; }
    }
}
