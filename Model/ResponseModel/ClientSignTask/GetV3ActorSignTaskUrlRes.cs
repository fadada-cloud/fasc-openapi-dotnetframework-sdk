﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSignTask
{
    public class GetV3ActorSignTaskUrlRes
    {
        public SignUrlRes[] signUrls { get; set; }
        public SignUrlDetail[] signDetails { get; set; }
    }

    public class SignUrlRes
    {
        public string signUrl { get; set; }
    }

    public class SignUrlDetail
    {
        public string signUrl { get; set; }
        public string signOrder { get; set; }
        public string signStatus { get; set; }
        public SignerRes signer { get; set; }
    }

    public class SignerRes
    {
        public SignatoryRes signatory { get; set; }
        public CorpRes corp { get; set; }
    }

    public class SignatoryRes
    {
        public string signerId { get; set; }
    }

    public class CorpRes
    {
        public string corpId { get; set; }
    }
}
