﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel
{
    public class HttpInfoRes
    {
        private string body;

        public string getBody()
        {
            return body;
        }

        public void setBody(string body)
        {
            this.body = body;
        }

        public static HttpInfoRes getInstance()
        {
            return new HttpInfoRes();
        }
    }
}
