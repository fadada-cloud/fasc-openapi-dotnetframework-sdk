﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel
{
    public class BaseRes<T> where T : class, new()
    {
        public string requestId;
        public string code;
        public string msg;
        public T data;

        public BaseRes()
        {

        }

        public BaseRes(string requestId, string code, string msg)
        {
            this.requestId = requestId;
            this.code = code;
            this.msg = msg;
        }

        public string getRequestId()
        {
            return requestId;
        }

        public void setRequestId(string requestId)
        {
            this.requestId = requestId;
        }

        public string getCode()
        {
            return code;
        }

        public string getMsg()
        {
            return msg;
        }

        public void setCode(string code)
        {
            this.code = code;
        }
        public void setMsg(string msg)
        {
            this.msg = msg;
        }

        public T getData()
        {
            if (data != null)
            {
                return data;
            }
            return null;
        }

        public void setData(T data)
        {
            this.data = data;
        }
    }
}
