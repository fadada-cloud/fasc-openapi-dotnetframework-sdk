﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance
{
    public class ContactArchivedRes
    {
        public ArchiveInfo[] archives { get; set; }
    }

    public class ArchiveInfo
    {
        public string archivesId { get; set; }
    }
}
