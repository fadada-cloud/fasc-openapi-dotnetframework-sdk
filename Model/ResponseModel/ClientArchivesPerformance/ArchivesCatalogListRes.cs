﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance
{
    public class ArchivesCatalogListRes
    {
        public string catalogId { get; set; }
        public string catalogName { get; set; }
        public string parentCatalogId { get; set; }
    }
}
