﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance
{
    public class ArchivedListRes
    {
        public ArchivedDetailInfo[] archives { get; set; }
        public int listPageNo { get; set; }
        public int listPageSize { get; set; }
        public int listPageCount { get; set; }
        public int total { get; set; }
    }

    public class ArchivedDetailInfo
    {
        public string archivesId { get; set; }
        public string contractName { get; set; }
        public string contractNo { get; set; }
        public string contractType { get; set; }
        public string catalogId { get; set; }
        public string signTaskId { get; set; }
        public string archiveTime { get; set; }
        public string memberId { get; set; }
        public AssociatedContractsArchivesInfo[] associatedContracts { get; set; }
        public AttachmentsArchivesInfo[] attachments { get; set; }
    }

    public class AssociatedContractsArchivesInfo
    {
        public string archivesId { get; set; }
        public string contractName { get; set; }
    }

    public class AttachmentsArchivesInfo
    {
        public string attachmentName { get; set; }
    }
}
