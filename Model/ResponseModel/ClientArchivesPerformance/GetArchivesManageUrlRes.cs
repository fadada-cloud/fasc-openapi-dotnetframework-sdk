﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance
{
    public class GetArchivesManageUrlRes
    {
        public string archivesUrl { get; set; }
    }
}
