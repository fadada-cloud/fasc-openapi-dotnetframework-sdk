﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientArchivesPerformance
{
    public class GetPerformanceListRes
    {
        public string performanceId { get; set; }
        public string archivesId { get; set; }
        public string performanceType { get; set; }
        public string performanceName { get; set; }
        public string expireTime { get; set; }
        public string remindStartDate { get; set; }
        public string remindFrequency { get; set; }
        public int? cycleDays { get; set; }
        public string remindTime { get; set; }
        public double? amount { get; set; }
        public string[] reminder { get; set; }
    }
}
