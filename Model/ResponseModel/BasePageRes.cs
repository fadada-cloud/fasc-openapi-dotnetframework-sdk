﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel
{
    public class BasePageRes
    {
        private int listPageNo;
        private int countInPage;
        private int listPageCount;
        private int totalCount;

        public int getTotalCount()
        {
            return totalCount;
        }

        public void setTotalCount(int totalCount)
        {
            this.totalCount = totalCount;
        }

        public int getListPageCount()
        {
            return listPageCount;
        }

        public void setListPageCount(int listPageCount)
        {
            this.listPageCount = listPageCount;
        }

        public int getListPageNo()
        {
            return listPageNo;
        }

        public void setListPageNo(int listPageNo)
        {
            this.listPageNo = listPageNo;
        }

        public int getCountInPage()
        {
            return countInPage;
        }

        public void setCountInPage(int countInPage)
        {
            this.countInPage = countInPage;
        }
    }
}
