﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    public class GetUserSealListRes
    {
        public long sealId { get; set; }
        public string entityId { get; set; }
        public string sealName { get; set; }
        public string createMethod { get; set; }
        public string categoryType { get; set; }
        public bool defaultSeal { get; set; }
        public string picFileUrl { get; set; }
        public string sealStatus { get; set; }
        public string certCAOrg { get; set; }
        public string certEncryptType { get; set; }
        public string grantTime { get; set; }
        public string grantStartTime { get; set; }
        public string grantEndTime { get; set; }
        public string grantStatus { get; set; }
    }
}
