﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 查询印章详情
    /// </summary>
    public class GetSealDetailRes
    {
        public long sealId { get; set; }
        public string sealName { get; set; }
        public string sealTag { get; set; }
        public string categoryType { get; set; }
        public string picFileUrl { get; set; }
        public string sealStatus { get; set; }
        public string createTime { get; set; }
        public string certCAOrg { get; set; }
        public string certEncryptType { get; set; }
        public SealUsers[] sealUsers { get; set; }
        public FreeSignInfo freeSignInfo { get; set; }
        public SealInfo sealInfo { get; set; }
    }
    public class SealInfo
    {
        public long sealId { get; set; }
        public string entityId { get; set; }
        public string sealName { get; set; }
        public string createMethod { get; set; }
        public string sealTag { get; set; }
        public string categoryType { get; set; }
        public string picFileUrl { get; set; }
        public int sealWidth { get; set; }
        public int sealHeight { get; set; }
        public string sealStatus { get; set; }
        public string createTime { get; set; }
        public string certCAOrg { get; set; }
        public string certEncryptType { get; set; }
        public SealUsers[] sealUsers { get; set; }
        public FreeSignInfo[] freeSignInfos { get; set; }
    }
    public class SealUsers
    {
        public long? memberId { get; set; }
        public string memberName { get; set; }
        public string internalIdentifier { get; set; }
        public string memberEmail { get; set; }
        public string grantTime { get; set; }
        public string grantStartTime { get; set; }
        public string grantEndTime { get; set; }
        public string grantStatus { get; set; }
    }
    public class FreeSignInfo
    {
        public string businessId { get; set; }
        public string businessScene { get; set; }
        public string businessSceneExp { get; set; }
        public string expiresTime { get; set; }
    }
}
