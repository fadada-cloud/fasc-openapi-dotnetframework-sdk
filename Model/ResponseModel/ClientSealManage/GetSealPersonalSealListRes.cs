﻿using System.Collections.Generic;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 查询个人签名列表
    /// </summary>
    public class GetSealPersonalSealListRes
    {
        public long sealId { get; set; }
        public string sealName { get; set; }
        public string picFileUrl { get; set; }
        public string createTime { get; set; }
        public string certCAOrg { get; set; }
        public string certEncryptType { get; set; }
        public FreeSignInfo[] freeSignInfos { get; set; }
        public List<SealInfo> sealInfos { get; set; }
    }
}
