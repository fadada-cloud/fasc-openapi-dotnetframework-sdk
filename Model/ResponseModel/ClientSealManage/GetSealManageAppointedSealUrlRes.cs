﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 获取指定印章详情链接
    /// </summary>
    public class GetSealManageAppointedSealUrlRes
    {
        public string appointedSealUrl { get; set; }
    }
}
