﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 获取设置个人签名免验证签链接
    /// </summary>
    public class GetSealPersonalSealFreeSignUrlRes
    {
        public string freeSignUrl { get; set; }
        public string freeSignShortUrl { get; set; }
    }
}
