﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 获取企业用印员列表
    /// </summary>
    public class GetSealUserListRes
    {
        public SealUserInfo[] sealUsers { get; set; }
    }

    /// <summary>
    /// 企业用印员列表
    /// </summary>
    public class SealUserInfo
    {
        public string memberId { get; set; }
        public string memberName { get; set; }
        public string internalIdentifier { get; set; }
        public string memberEmail { get; set; }
        public SealArrInfo[] sealInfos { get; set; }
    }
    /// <summary>
    ///  用印员持有的印章列表，数组类
    /// </summary>
    public class SealArrInfo
    {
        public string sealId { get; set; }
        public string entityId { get; set; }
        public string sealName { get; set; }
        public string createMethod { get; set; }
        public string sealStatus { get; set; }
        public string categoryType { get; set; }
        public string picFileUrl { get; set; }
        public string createTime { get; set; }
        public string certCAOrg { get; set; }
        public string certEncryptType { get; set; }
        public string grantTime { get; set; }
        public string grantStartTime { get; set; }
        public string grantEndTime { get; set; }
        public string grantStatus { get; set; }
    }
}
