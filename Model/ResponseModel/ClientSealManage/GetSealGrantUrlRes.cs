﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 获取印章授权链接
    /// </summary>
    public class GetSealGrantUrlRes
    {
        public string sealGrantUrl { get; set; }
    }
}
