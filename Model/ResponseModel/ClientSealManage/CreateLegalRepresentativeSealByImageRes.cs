﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    public class CreateLegalRepresentativeSealByImageRes
    {
        public long? sealId { get; set; }
        public long? verifyId { get; set; }
    }
}
