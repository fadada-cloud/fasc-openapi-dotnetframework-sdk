﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 获取印章列表
    /// </summary>
    public class GetSealListRes
    {
        /// <summary>
        /// 印章列表
        /// </summary>
        public SealInfo[] sealInfos { get; set; }
    }
}
