﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 获取设置企业印章免验证签链接
    /// </summary>
    public class GetSealFreeSignUrlRes
    {
        public string freeSignUrl { get; set; }
        public string freeSignShortUrl { get; set; }
    }
}
