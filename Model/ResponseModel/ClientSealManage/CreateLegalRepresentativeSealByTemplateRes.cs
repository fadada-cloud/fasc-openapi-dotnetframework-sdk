﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    public class CreateLegalRepresentativeSealByTemplateRes
    {
        public long sealId { get; set; }
    }
}
