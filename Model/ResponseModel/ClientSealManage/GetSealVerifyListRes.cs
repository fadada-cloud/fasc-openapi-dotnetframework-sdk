﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 查询审核中的印章列表
    /// </summary>
    public class GetSealVerifyListRes
    {
        public VerifyInfo[] verifyInfos { get; set; }
    }
    public class VerifyInfo
    {
        public long verifyId { get; set; }
        public string sealName { get; set; }
        public string categoryType { get; set; }
        public string picFileUrl { get; set; }
        public string createTime { get; set; }
        public string verifyStatus { get; set; }
    }
}
