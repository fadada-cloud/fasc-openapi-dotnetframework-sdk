﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientSealManage
{
    /// <summary>
    /// 获取印章管理链接
    /// </summary>
    public class SealManageUrlRes
    {
        public string resourceUrl { get; set; }
    }
}
