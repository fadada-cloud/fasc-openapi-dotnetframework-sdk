﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    /// <summary>
    /// 查询成员详情
    /// </summary>
    public class GetCorpMemberDetailRes
    {
        public long memberId { get; set; }
        public string memberName { get; set; }
        public string internalIdentifier { get; set; }
        public string memberEmail { get; set; }
        public string memberMobile { get; set; }
        public string fddId { get; set; }
        public string memberStatus { get; set; }
        public long[] memberDeptIds { get; set; }
        public string[] roleType { get; set; }
    }
}
