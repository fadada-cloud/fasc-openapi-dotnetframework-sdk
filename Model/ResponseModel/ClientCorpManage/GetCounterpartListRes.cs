﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    public class GetCounterpartListRes
    {
        public CounterpartInfo[] counterpartInfos { get; set; }
    }

    public class CounterpartInfo
    {
        public string corpName { get; set; }
        public string corpIdentNo { get; set; }
        public string status { get; set; }
    }
}
