﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    public class GetCorpEntityManageUrlRes
    {
        /// <summary>
        /// 组织管理页面链接，有效期10分钟，仅提供PC端
        /// </summary>
        public string entityManageUrl { get; set; }
    }
}
