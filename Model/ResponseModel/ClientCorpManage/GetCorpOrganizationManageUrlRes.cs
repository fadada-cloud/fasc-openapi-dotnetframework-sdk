﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    /// <summary>
    /// 获取组织管理链接
    /// </summary>
    public class GetCorpOrganizationManageUrlRes
    {
        public string resourceUrl { get; set; }
    }
}
