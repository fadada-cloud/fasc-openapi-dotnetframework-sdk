﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    public class GetEntityListRes
    {
        /// <summary>
        /// 企业主体Id
        /// </summary>
        public string entityId { get; set; }
        /// <summary>
        /// 该主体在企业帐号下的类型
        /// </summary>
        public string entityType { get; set; }
        /// <summary>
        /// 企业名称
        /// </summary>
        public string corpName { get; set; }
        /// <summary>
        /// 企业统信码或工商执照注册号
        /// </summary>
        public string corpIdentNo { get; set; }
        /// <summary>
        /// 企业法定代表人名称
        /// </summary>
        public string legalRepName { get; set; }
        /// <summary>
        /// 企业认证状态
        /// </summary>
        public string identStatus { get; set; }
    }
}
