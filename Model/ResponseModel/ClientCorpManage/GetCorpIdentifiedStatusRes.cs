﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    /// <summary>
    /// 查询企业实名认证状态
    /// </summary>
    public class GetCorpIdentifiedStatusRes
    {
        public bool identStatus { get; set; }
    }
}
