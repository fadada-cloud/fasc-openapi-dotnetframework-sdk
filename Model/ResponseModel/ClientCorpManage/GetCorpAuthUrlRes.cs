﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientOrgManage
{
    /// <summary>
    /// 获取企业用户授权链接
    /// </summary>
    public class GetCorpAuthUrlRes
    {
        public string authUrl { get; set; }
    }
}
