﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    /// <summary>
    /// 查询部门列表
    /// </summary>
    public class GetCorpDeptListRes
    {
        public long? deptId { get; set; }
        public string deptName { get; set; }
        public int deptOrderNum { get; set; }
        public long? parentDeptId { get; set; }
        public string identifier { get; set; }
    }
}
