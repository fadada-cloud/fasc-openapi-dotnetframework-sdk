﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    /// <summary>
    /// 获取成员激活链接
    /// </summary>
    public class CorpMemberCreateRes
    {
        public long memberId { get; set; }
        public string internalIdentifier { get; set; }
        public string memberActiveUrl { get; set; }
        public string memberActiveEmbedUrl { get; set; }
        public string memberStatus { get; set; }
    }
}
