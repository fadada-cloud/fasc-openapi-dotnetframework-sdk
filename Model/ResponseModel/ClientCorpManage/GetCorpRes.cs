﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    /// <summary>
    /// 获取企业用户基本信息
    /// </summary>
    public class GetCorpRes
    {
        public string clientCorpId { get; set; }
        public string openCorpId { get; set; }
        public string bindingStatus { get; set; }
        public string[] authScope { get; set; }
        public string identStatus { get; set; }
        public string availableStatus { get; set; }
    }
}
