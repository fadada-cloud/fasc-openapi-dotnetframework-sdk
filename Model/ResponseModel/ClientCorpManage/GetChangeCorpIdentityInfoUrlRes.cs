﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    public class GetChangeCorpIdentityInfoUrlRes
    {
        /// <summary>
        /// 变更工商信息页面链接，有效期10分钟，1次有效，仅提供PC端
        /// </summary>
        public string changeIdentityInfoUrl { get; set; }
    }
}
