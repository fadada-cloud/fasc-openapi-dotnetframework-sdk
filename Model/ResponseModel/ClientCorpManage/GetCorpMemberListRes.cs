﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    /// <summary>
    /// 查询企业成员列表
    /// </summary>
    public class GetCorpMemberListRes
    {
        public EmployeeInfo[] employeeInfos { get; set; }
        public int listPageNo { get; set; }
        public int countInPage { get; set; }
        public int listPageCount { get; set; }
        public int totalCount { get; set; }
    }

    public class EmployeeInfo
    {
        public string memberId { get; set; }
        public string memberName { get; set; }
        public string internalIdentifier { get; set; }
        public string memberEmail { get; set; }
        public string memberMobile { get; set; }
        public string memberStatus { get; set; }
        public long[] memberDeptIds { get; set; }
        public string[] roleType { get; set; }
    }
}
