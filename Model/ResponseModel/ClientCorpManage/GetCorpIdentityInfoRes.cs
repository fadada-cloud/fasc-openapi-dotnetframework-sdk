﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientCorpManage
{
    /// <summary>
    /// 获取企业用户身份信息
    /// </summary>
    public class GetCorpIdentityInfoRes
    {
        public string openCorpId { get; set; }
        public string corpIdentStatus { get; set; }
        public CorpIdentInfoExtend corpIdentInfoExtend { get; set; }
        public CorpInfoExtend corpInfoExtend { get; set; }
        public string corpIdentMethod { get; set; }
        public string identSubmitTime { get; set; }
        public string identSuccessTime { get; set; }
        public string fddId { get; set; }
        public bool? licenseCorpName { get; set; }
    }
    public class CorpIdentInfoExtend
    {
        public string corpName { get; set; }
        public string corpIdentType { get; set; }
        public string corpIdentNo { get; set; }
        public string legalRepName { get; set; }
    }
    public class CorpInfoExtend
    {
        public string bankName { get; set; }
        public string bankBranchName { get; set; }
        public string bankAccountNo { get; set; }
        public string bankProvinceName { get; set; }
        public string bankCityName { get; set; }
    }
}
