﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage
{
    /// <summary>
    /// 查询个人用户基本信息
    /// </summary>
    public class GetUserInfoRes
    {
        /// <summary>
        /// 用户在应用系统中的唯一标识
        /// </summary>
        public string clientUserId { get; set; }
        /// <summary>
        /// 法大大平台为该用户在该应用appId范围内分配的唯一标识
        /// </summary>
        public string openUserId { get; set; }
        /// <summary>
        /// 授权状态
        /// </summary>
        public string bindingStatus { get; set; }
        /// <summary>
        /// 用户授权范围列表
        /// </summary>
        public string[] authScope { get; set; }
        /// <summary>
        /// 实名认证状态
        /// </summary>
        public string identStatus { get; set; }
        /// <summary>
        /// 个人用户有效状态
        /// </summary>
        public string availableStatus { get; set; }
    }
}
