﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage
{
    public class GetAppOpenIdListRes
    {
        public OpenIdInfo[] openIdInfos { get; set; }
        public int listPageNo { get; set; }
        public int countInPage { get; set; }
        public int listPageCount { get; set; }
        public int totalCount { get; set; }
    }

    public class OpenIdInfo
    {
        public string name { get; set; }
        public string openId { get; set; }
        public string clientId { get; set; }
    }

}
