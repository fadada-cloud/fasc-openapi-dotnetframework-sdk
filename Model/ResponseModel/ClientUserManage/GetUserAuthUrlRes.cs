﻿namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage
{
    /// <summary>
    ///获取个人用户授权链接
    /// </summary>
    public class GetUserAuthUrlRes
    {
        /// <summary>
        /// 授权链接的Embedded URL形式
        /// </summary>
        public string authUrl { get; set; }
        /// <summary>
        /// 个人授权短连接，有效期7天
        /// </summary>
        public string authShortUrl { get; set; }
    }
}
