﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage
{
    public class GetChangeAccountRes
    {
        /// <summary>
        /// 更换帐号页面链接，有效期10分钟
        /// </summary>
        public string accountNameChangeUrl { get; set; }
    }
}
