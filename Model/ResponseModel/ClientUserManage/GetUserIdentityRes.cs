﻿using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientUserManage
{
    /// <summary>
    /// 获取个人用户身份信息
    /// </summary>
    public class GetUserIdentityRes
    {
        /// <summary>
        /// 法大大平台为该用户在该应用appId范围内分配的唯一标识
        /// </summary>
        public string openUserId { get; set; }
        /// <summary>
        /// 该用户的法大大帐号，为邮箱或手机号
        /// </summary>
        public string accountName { get; set; }
        /// <summary>
        /// 实名认证状态
        /// </summary>
        public string identStatus { get; set; }
        /// <summary>
        /// 用户的实名身份信息
        /// </summary>
        public UserIdentInfoExtend userIdentInfoExtend { get; set; }
        /// <summary>
        /// 用户的补充信息
        /// </summary>
        public UserInfoExtend userInfoExtend { get; set; }
        /// <summary>
        /// 用户实名认证时所选择的认证方式
        /// </summary>
        public string identMethod { get; set; }
        /// <summary>
        /// 认证提交时间。格式为：Unix标准时间戳，精确到毫秒
        /// </summary>
        public string identSubmitTime { get; set; }
        /// <summary>
        /// 认证通过时间
        /// </summary>
        public string identSuccessTime { get; set; }
        /// <summary>
        /// 该用户的法大大号
        /// </summary>
        public string fddId { get; set; }
        /// <summary>
        /// 该用户认证时的刷脸底图base64，仅当用户使用刷脸认证时会返回
        /// </summary>
        public string facePicture { get; set; }

    }
}
