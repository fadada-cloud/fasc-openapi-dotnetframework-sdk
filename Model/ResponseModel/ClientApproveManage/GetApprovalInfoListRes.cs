﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage
{
    public class GetApprovalInfoListRes
    {
        public int listPageNo { get; set; }
        public int countInPage { get; set; }
        public int listPageCount { get; set; }
        public int totalCount { get; set; }
        public ApprovalInfoRes[] approvalInfos { get; set; }
    }

    public class ApprovalInfoRes
    {
        public string approvalType { get; set; }
        public string templateId { get; set; }
        public string signTaskId { get; set; }
        public string approvalId { get; set; }
        public string approvalName { get; set; }
        public string approvalStatus { get; set; }
    }
}
