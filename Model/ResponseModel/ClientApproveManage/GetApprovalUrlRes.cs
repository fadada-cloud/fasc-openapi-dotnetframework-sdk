﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage
{
    public class GetApprovalUrlRes
    {
        public string approvalUrl { get; set; }
    }
}
