﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage
{
    public class GetApprovalDetailRes
    {
        public string approvalType { get; set; }
        public string templateId { get; set; }
        public string signTaskId { get; set; }
        public string approvalId { get; set; }
        public string approvalName { get; set; }
        public string applicantName { get; set; }
        public string applicantMemberId { get; set; }
        public string approvalStatus { get; set; }
        public ApprovalNode[] approvalNode { get; set; }
    }

    public class ApprovalNode
    {
        public long nodeId { get; set; }
        public string approvalType { get; set; }
        public string nodeStatus { get; set; }
        public ApproversInfo[] approversInfos { get; set; }
    }

    public class ApproversInfo
    {
        public string approverMemberName { get; set; }
        public long approverMemberId { get; set; }
        public string approverStatus { get; set; }
        public string operateTime { get; set; }
        public string note { get; set; }
    }
}
