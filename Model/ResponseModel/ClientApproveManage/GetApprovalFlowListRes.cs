﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage
{
    public class GetApprovalFlowListRes
    {
        public string approvalType { get; set; }
        public string approvalFlowId { get; set; }
        public string approvalFlowName { get; set; }
        public string description { get; set; }
        public string status { get; set; }
    }
}
