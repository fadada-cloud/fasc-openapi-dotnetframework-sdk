﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.ResponseModel.ClientApproveManage
{
    public class GetApprovalFlowDetailRes
    {
        public string approvalType { get; set; }
        public string approvalFlowId { get; set; }
        public string approvalFlowName { get; set; }
        public string description { get; set; }
        public string lastOprMemberId { get; set; }
        public string lastOprMemberName { get; set; }
        public string updateTime { get; set; }
        public string status { get; set; }
        public ApprovalFlowNode[] approvalNode { get; set; }
    }

    public class ApprovalFlowNode
    {
        public int sortNum { get; set; }
        public string approvalNodeType { get; set; }
        public ApproversFlowInfo[] approversInfos { get; set; }
    }

    public class ApproversFlowInfo
    {
        public string approverMemberName { get; set; }
        public string approverMemberId { get; set; }
    }
}
