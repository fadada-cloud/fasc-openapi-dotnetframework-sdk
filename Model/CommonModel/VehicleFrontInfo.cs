﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class VehicleFrontInfo
    {
        public string plateNo { get; set; }
        public string vehicleType { get; set; }
        public string owner { get; set; }
        public string address { get; set; }
        public string useCharacter { get; set; }
        public string model { get; set; }
        public string vin { get; set; }
        public string engineNo { get; set; }
        public string registerDate { get; set; }
        public string issueDate { get; set; }
        public string issuingAuthority { get; set; }
    }
}
