﻿using System.Collections.Generic;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class BatchDownloadInfo
    {
        public string batchSigntaskId { get; set; }
        
        public List<BatchDownloadInfoDoc> docInfos { get; set; }

        public List<BatchDownloadInfoAttach> attaches { get; set; }
    }
}