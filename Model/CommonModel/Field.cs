﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class Field
    {
        public string fieldId { get; set; }
        public string fieldName { get; set; }
        public string fieldKey { get; set; }
        public FieldPosition position { get; set; }
        public string fieldType { get; set; }
        public FieldTextSingleLine fieldTextSingleLine { get; set; }
        public FieldTextMultiLine fieldTextMultiLine { get; set; }
        public FieldCheckBox fieldCheckBox { get; set; }
        public FieldMultiCheckbox fieldMultiCheckbox { get; set; }
        public FieldRemarkSign fieldRemarkSign { get; set; }
        public bool? moveable { get; set; }
        public FieldNumber fieldNumber { get; set; }
        public FieldIdCard fieldIdCard { get; set; }
        public FieldFillDate fieldFillDate { get; set; }
        public FieldMultiRadio fieldMultiRadio { get; set; }
        public FieldPicture fieldPicture { get; set; }
        public FieldSelectBox fieldSelectBox { get; set; }
        public FieldTable fieldTable { get; set; }
        public FieldPersonSign fieldPersonSign { get; set; }
        public FieldCorpSeal fieldCorpSeal { get; set; }
        public FieldDateSign fieldDateSign { get; set; }
    }
}
