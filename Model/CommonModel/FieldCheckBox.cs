﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    /// <summary>
    /// 复选框控件。
    /// </summary>
    public class FieldCheckBox
    {
        public bool required { get; set; }
        public bool defaultValue { get; set; }
    }
}
