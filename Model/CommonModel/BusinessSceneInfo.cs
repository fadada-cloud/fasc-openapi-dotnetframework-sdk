﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class BusinessSceneInfo
    {
        public string businessId { get; set; }
        public string transReferenceId { get; set; }
    }
}
