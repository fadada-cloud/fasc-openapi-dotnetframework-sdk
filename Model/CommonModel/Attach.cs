﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class Attach
    {
        public string attachId { get; set; }
        public string attachName { get; set; }
        public string attachFileId { get; set; }
    }
}
