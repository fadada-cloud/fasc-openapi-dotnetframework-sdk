﻿namespace fasc_openapi_donet_sdk.Model.RequestModel
{
    /// <summary>
    /// 统一标识应用系统上的用户(个人或企业)
    /// </summary>
    public class OpenId
    {
        /// <summary>
        /// 主体类型：<br />corp: 企业<br />person: 个人     
        /// </summary>
        public string idType { get; set; }
        /// <summary>
        /// 主体标识，长度最大64个字符。<br />如果idType为corp：代表应用系统上的企业用户，主体方是openCorpId所指定的企业；<br />如果idType为person：代表应用系统上的个人用户，主体方是openUserId所指定的个人
        /// </summary>
        public string openId { get; set; }
    }
}
