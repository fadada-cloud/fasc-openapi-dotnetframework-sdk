﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class Notification
    {
        public bool? sendNotification { get; set; }
        public string notifyWay { get; set; }
        public string notifyAddress { get; set; }
        public bool? appointAccount { get; set; }
        public CustomNotifyContent customNotifyContent { get; set; }
    }
}
