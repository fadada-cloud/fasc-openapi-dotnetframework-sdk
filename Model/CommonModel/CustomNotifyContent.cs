namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class CustomNotifyContent
    {
		public string serviceCenterName { get; set; }
		public string customUrl { get; set; }
    }
}
