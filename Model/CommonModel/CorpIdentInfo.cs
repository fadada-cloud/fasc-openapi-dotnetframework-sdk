﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class CorpIdentInfo
    {
        public string corpName { get; set; }
        public string corpIdentType { get; set; }
        public string corpIdentNo { get; set; }
        public string legalRepName { get; set; }
    }
}
