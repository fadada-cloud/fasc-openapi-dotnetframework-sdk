﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class CreateCorpFieldFailInfo
    {
        public string fieldName { get; set; }
        public string fieldType { get; set; }
        public string failReason { get; set; }
    }
}
