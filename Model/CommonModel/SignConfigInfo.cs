﻿using fasc_openapi_donet_sdk.Model.RequestModel.ClientSignTask;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class SignConfigInfo
    {
        public int? orderNo { get; set; }
        public bool? blockHere { get; set; }
        public bool? requestVerifyFree { get; set; }
        public bool? requestMemberSign { get; set; }
        public string[] verifyMethods { get; set; }
        public bool? joinByLink { get; set; }
        public string signerSignMethod { get; set; }
        public bool? readingToEnd { get; set; }
        public string readingTime { get; set; }
        public bool? identifiedView { get; set; }
        public bool? resizeSeal { get; set; }
        public string[] audioVideoInfo { get; set; }
        public ActorAttachInfo[] actorAttachInfos { get; set; }
        public bool? freeLogin { get; set; }
        public bool? viewCompletedTask { get; set; }
        public long? freeDragSealId { get; set; }
        public bool? signAllDoc { get; set; }
        public bool? authorizeFreeSign { get; set; }
    }
}
