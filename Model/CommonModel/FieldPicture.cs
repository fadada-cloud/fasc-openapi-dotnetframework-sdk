using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class FieldPicture
    {
		public bool? required { get; set; }
		public string defaultValue { get; set; }
		public int? width { get; set; }
		public int? height { get; set; }
    }
}
