﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    /// <summary>
    /// 身份证控件。
    /// </summary>
    public class FieldIdCard
    {
        public bool required { get; set; }
        public string defaultValue { get; set; }
        public string tips { get; set; }
        public int? width { get; set; }
        public int? height { get; set; }
        public string fontType { get; set; }
        public int? fontSize { get; set; }
        public string alignment { get; set; }
    }
}
