﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class VehicleBackInfo
    {
        public string plateNo { get; set; }
        public string fileNo { get; set; }
        public string allowNum { get; set; }
        public string totalMass { get; set; }
        public string curbWeight { get; set; }
        public string loadQuality { get; set; }
        public string externalSize { get; set; }
        public string totalQuasiMass { get; set; }
        public string marks { get; set; }
        public string record { get; set; }
    }
}
