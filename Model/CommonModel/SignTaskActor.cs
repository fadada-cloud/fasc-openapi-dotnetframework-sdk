﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class SignTaskActor
    {
        public Actor actor { get; set; }

        public FillField[] fillFields { get; set; }
        public SignField[] signFields { get; set; }

        public SignConfigInfo signConfigInfo { get; set; }
    }
}
