﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    /// <summary>
    /// 个人用户补充信息
    /// </summary>
    public class UserInfoExtend
    {
        /// <summary>
        /// 个人银行账户号。长度最大30个字符
        /// </summary>
        public string bankAccountNo { get; set; }
        /// <summary>
        /// 个人实名手机号 。长度最大30个字符。
        /// </summary>
        public string mobile { get; set; }
    }
}
