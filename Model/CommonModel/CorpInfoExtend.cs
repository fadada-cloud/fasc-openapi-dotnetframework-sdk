﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{

    public class CorpInfoExtend
    {
        public string bankName { get; set; }
        public string bankBranchName { get; set; }
        public string bankAccountNo { get; set; }
        public string bankProvinceName { get; set; }
        public string bankCityName { get; set; }
    }
}
