﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class Doc
    {
        public string docId { get; set; }
        public string docName { get; set; }
        public string docFileId { get; set; }
        public string docTemplateId { get; set; }
        public Field[] docFields { get; set; }
    }
}
