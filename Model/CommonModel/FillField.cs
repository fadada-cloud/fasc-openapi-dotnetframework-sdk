﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class FillField
    {
        public string fieldDocId { get; set; }
        public string fieldId { get; set; }
        public string fieldName { get; set; }
        public string fieldValue { get; set; }
    }
}
