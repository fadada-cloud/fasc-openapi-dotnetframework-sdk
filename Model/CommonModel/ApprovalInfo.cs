namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class ApprovalInfo
    {
		public string approvalFlowId { get; set; }
		public string approvalId { get; set; }
		public string approvalType { get; set; }
		public string approvalStatus { get; set; }
		public string rejectNote { get; set; }
    }
}
