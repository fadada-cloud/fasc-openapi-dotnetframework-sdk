using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class FieldDateSign
    {
		public string dateFormat { get; set; }
		public int? fontSize { get; set; }
    }
}
