namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class Watermark
    {
		public string type { get; set; }
		public string content { get; set; }
		public string picFileId { get; set; }
		public string picBase64 { get; set; }
		public string picWidth { get; set; }
		public string picHeight { get; set; }
		public int? fontSize { get; set; }
		public string fontColor { get; set; }
		public int? rotation { get; set; }
		public int? transparency { get; set; }
		public string position { get; set; }
		public string density { get; set; }
    }
}
