﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class BatchDownloadInfoAttach
    {
        public string attachId { get; set; }

        public string customDocName { get; set; }
    }
}