﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    /// <summary>
    /// 个人身份信息
    /// </summary>
    public class UserIdentInfoExtend
    {
        /// <summary>
        /// 个人用户真实姓名。长度最大50个字符
        /// </summary>
        public string userName { get; set; }
        /// <summary>
        /// 证件类型
        /// </summary>
        public string identType { get; set; }
        /// <summary>
        /// 证件号
        /// </summary>
        public string identNo { get; set; }
    }
}
