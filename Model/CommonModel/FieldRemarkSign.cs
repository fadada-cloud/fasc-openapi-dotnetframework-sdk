﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    /// <summary>
    /// 备注区属性参数
    /// </summary>
    public class FieldRemarkSign
    {
        /// <summary>
        /// 备注区内的提示语
        /// </summary>
        public string defaultValue { get; set; }
        public string tips { get; set; }
        public bool? editable { get; set; }
        public int? width { get; set; }
        public int? height { get; set; }
        public string fontType { get; set; }
        public int? fontSize { get; set; }
    }
}
