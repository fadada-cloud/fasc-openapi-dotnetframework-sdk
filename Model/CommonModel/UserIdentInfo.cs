﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    /// <summary>
    /// 个人身份信息
    /// </summary>
    public class UserIdentInfo
    {
        /// <summary>
        /// 个人用户真实姓名。长度最大50个字符
        /// </summary>
        public string userName { get; set; }
        /// <summary>
        /// 个人手机号 ，长度最大30个字符
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// 个人银行账户号。长度最大30个字符
        /// </summary>
        public string bankAccountNo { get; set; }
        /// <summary>
        /// 用户实名认证方式：传多项按字段顺序为优先级展示
        /// </summary>
        public string[] identMethod { get; set; }
        /// <summary>
        /// 证件类型
        /// </summary>
        public string identType { get; set; }
        /// <summary>
        /// 证件号
        /// </summary>
        public string identNo { get; set; }
    }
}
