﻿using fasc_openapi_donet_sdk.Model.CommonModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Common
{
    public class CreateCorpFieldInfo
    {
        public string fieldName { get; set; }
        public string fieldType { get; set; }
        public FieldTextSingleLine fieldTextSingleLine { get; set; }
        public FieldTextMultiLine fieldTextMultiLine { get; set; }
        public FieldMultiCheckbox fieldMultiCheckbox { get; set; }
    }
}
