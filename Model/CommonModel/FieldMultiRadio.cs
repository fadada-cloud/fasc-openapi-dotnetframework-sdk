﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    /// <summary>
    /// 单选框-多项控件。
    /// </summary>
    public class FieldMultiRadio
    {
        public bool required { get; set; }
        public bool option { get; set; }
        public bool[] defaultValue { get; set; }
    }
}
