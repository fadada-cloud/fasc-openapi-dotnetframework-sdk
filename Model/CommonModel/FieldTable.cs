using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class FieldTable
    {
		public bool? required { get; set; }
		public string[] header { get; set; }
		public int? requiredCount { get; set; }
		public string fontType { get; set; }
		public int? fontSize { get; set; }
		public string alignment { get; set; }
		public string headerPosition { get; set; }
		public int? rows { get; set; }
		public int? cols { get; set; }
		public int? rowHeight { get; set; }
		public int?[] widths { get; set; }
		public bool? dynamicFilling { get; set; }
		public string[][] defaultValue { get; set; }
		public bool? hideHeader { get; set; }
    }
}
