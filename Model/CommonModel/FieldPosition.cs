﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class FieldPosition
    {
        public string positionMode { get; set; }
        public int? positionPageNo { get; set; }
        public string positionX { get; set; }
        public string positionY { get; set; }
        public string positionKeyword { get; set; }
        public string keywordOffsetX { get; set; }
        public string keywordOffsetY { get; set; }
    }
}
