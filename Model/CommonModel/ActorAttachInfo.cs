using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class ActorAttachInfo
    {
		public string actorAttachName { get; set; }
		public bool? required { get; set; }
    }
}
