﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class Actor
    {
        public string actorId { get; set; }
        public string actorType { get; set; }
        public string actorName { get; set; }
        public string[] permissions { get; set; }
        public string actorOpenId { get; set; }
        public string actorFDDId { get; set; }
        public string actorEntityId { get; set; }
        public ActorCorpMember[] actorCorpMembers { get; set; }
        public string identNameForMatch { get; set; }
        public string certType { get; set; }
        public string certNoForMatch { get; set; }
        public string accountName { get; set; }
        public Notification notification { get; set; }
        public bool? sendNotification { get; set; }
        public string[] notifyType { get; set; }
        public string notifyAddress { get; set; }
        public string clientUserId { get; set; }
        public string[] authScopes { get; set; }
    }
}
