﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    /// <summary>
    /// 复选框-多项控件。
    /// </summary>
    public class FieldMultiCheckbox
    {
        public bool required { get; set; }
        public string option { get; set; }
        public bool[] defaultValue { get; set; }
    }
}
