using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class FieldCorpSeal
    {
		public int? width { get; set; }
		public int? height { get; set; }
		public string categoryType { get; set; }
    }
}
