﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class SignField
    {
        public string fieldDocId { get; set; }
        public string fieldId { get; set; }
        public string fieldName { get; set; }
        public long? sealId { get; set; }
        public bool moveable { get; set; }
    }
}
