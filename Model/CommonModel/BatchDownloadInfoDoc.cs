﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class BatchDownloadInfoDoc
    {
        public string docId { get; set; }

        public string customDocName { get; set; }
    }
}