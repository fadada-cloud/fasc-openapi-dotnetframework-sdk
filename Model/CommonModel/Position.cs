namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class Position
    {
		public int? positionPageNo { get; set; }
		public string positionX { get; set; }
		public string positionY { get; set; }
    }
}
