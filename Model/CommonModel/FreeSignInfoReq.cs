﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class FreeSignInfoReq
    {
        /// <summary>
        /// 需要绑定默认签名的免验证签场景码
        /// </summary>
        public string businessId { get; set; }
    }
}
