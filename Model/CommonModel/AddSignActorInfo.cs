﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class AddSignActorInfo
    {
        public Actor signActor { get; set; }
        public int? orderNo { get; set; }
        public bool blockHere { get; set; }
        public bool requestVerifyFree { get; set; }
        public string[] verifyMethods { get; set; }
        public bool corpOperatorSign { get; set; }
        public string signerSignMethod { get; set; }
        public AddSignActorFieldInfo[] actorFields { get; set; }
    }
}
