using fasc_openapi_donet_sdk.Model.CommonModel;

namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class FieldSelectBox
    {
		public bool? required { get; set; }
		public string[] option { get; set; }
		public string defaultValue { get; set; }
		public string tips { get; set; }
		public int? width { get; set; }
		public int? height { get; set; }
		public string fontType { get; set; }
		public int? fontSize { get; set; }
		public string alignment { get; set; }
    }
}
