﻿namespace fasc_openapi_donet_sdk.Model.CommonModel
{
    public class AddFillActorInfo
    {
        public Actor fillActor { get; set; }
        public int? orderNo { get; set; }
        public AddFillActorFieldInfo[] actorFields { get; set; }
    }
}
